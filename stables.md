# Stables

## Description

When referring to stable/unstable, I talk about major bugs. In other words, if a version has a major bug(not a missing feature, but something that went wrong) then I call it unstable. A major bug would be a bug that is so big, that it is hard to understand where the problem is, generally these take more than a day to fix.

## Total`(30)` (Including the one in progress)

## In Progress

[![](https://img.shields.io/badge/Orion OS-test--1.104-red.svg?style=flat-square)]()

## Stable`(18)`

[![](https://img.shields.io/badge/Orion OS-test--s--1.000-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.010-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.022-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.032-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.040-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.045-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.061-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.062-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.063-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.082-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.083-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.085-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.090-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.092-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.093-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.100-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.102-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--s--1.103-red.svg?style=flat-square)]()

## Unstable`(8)`

[![](https://img.shields.io/badge/Orion OS-test--u--1.020-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--u--1.030-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--u--1.060-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--u--1.080-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--u--1.081-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--u--1.084-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--u--1.091-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--u--1.098-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--u--1.101-red.svg?style=flat-square)]()

## Fail`(3)`

[![](https://img.shields.io/badge/Orion OS-test--f--1.050-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--f--1.070-red.svg?style=flat-square)]()
[![](https://img.shields.io/badge/Orion OS-test--f--1.08,10-red.svg?style=flat-square)]()
