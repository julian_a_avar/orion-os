# Orion OS

[![Orion OS demo](screenshots/test-1.102.PNG)](http://spotnight.io/projects/github/orion-os/test/test-1/test-1.1/test-1.10/test-1.102)
Demo might be found\* [here](http://spotnight.io/projects/github/orion-os/test/test-1/test-1.0/test-1.09/test-1.093/index.html).  
\*__Please notice that this demo might not be fully updated as it is a demo__

[![](https://img.shields.io/badge/Technologies used- HTML5 | CSS3 | JS | JSON-383838.svg?style=flat-square)]()

[![](https://img.shields.io/badge/Licence-GNU_v2.0-blue.svg?style=flat-square)](LICENCE.txt)
[![](https://img.shields.io/badge/Orion OS-test--s--1.103-red.svg?style=flat-square)](test/test-1/test-1.1/test-1.10/test-1.103)
[![](https://img.shields.io/badge/Status-fully oprational-green.svg?style=flat-square)](stables.md)
[//]: # (Options include fully operational: green, stable: yellow, unstable: orange, failed: red)
[//]: # (fully operational: less than 20 bugs, and application is "usable"; stable: less than 25 bugs; unstable: less than 40 bugs, and/or application is unusable; failed: nothing was accomplished)


## Versions

Recommended Version is [`test-s-1.103`](test/test-1/test-1.1/test-1.10/test-1.103).

Latest **completed** release is [`test-s-1.103`](test/test-1/test-1.1/test-1.10/test-1.103).

Most recent **fully operational** release is [`test-s-1.103`](test/test-1/test-1.1/test-1.10/test-1.103).

Most recent **stable** release is [`test-s-1.103`](test/test-1/test-1.1/test-1.10/test-1.103)  
Most recent **unstable** release is [`test-u-1.101`](test/test-1/test-1.1/test-1.10/test-1.101).  
Most recent **failed** release is [`test-f-1.08,10`](test/test-1/test-1.0/test-1.08/test-1.08,10).

Currently working on [`test 1.104`](test/test-1/test-1.1/test-1.10/test-1.104)  
It's project plan may be found [`here`](test/test-1/test-1.1/test-1.10/test-1.104/plan.md). This contains bugs and features which need to be resolved as well as a description and summary of the whole application until now.

To see a more detailed documentation of stability from all the versions, you may find them [here](stables.md).

## Resources

Project logs may be found [here](logs.md). This includes a log containing the accomplishments of Orion OS, as well as quick memos for what I'll be doing.

If you don't understand how I've written `plan.md`, you can find an explanation [here](Howto-Readplan.md).

And if you wonder how to read file versions 'cause you don't understand how I numbered them, you can find that [here](Howto-Readfileversion.md).

Finally, if you would like to see some of my brainstorming of this project, you may find that [here](notes).

## License

The GNU General Public License V2.0(GNU GPL V2.0),  
&copy; 2016, Julian Antonio Avar Campopiano.  
See [full license here](license.txt)
