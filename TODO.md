# TODO

This is a list of documents and things I should do next time I continue this project. This is important if project is left unattended for long periods of time.

Open

1. [ test 1.03 ] [ index.html ]              [ line ~20 ]
2. [ test 1.03 ] [ js / consts.js ]          [ line ~12 ]
3. [ test 1.03 ] [ js / apps / sys_apps.js ] [ line ~21 ]
4. [ test 1.03 ] [ js / oos-engine.js ]      [ line ~80 ]

Todo

1. leave [index.html] alone
2. work on adding on to uasoi() in [consts.js], a function that can append multiple nested object levels
3. use [sys_apps.js] to debug uasoi() in todo-2
4. [oos-engine.js] should be left alone, although it could come in handy later on
