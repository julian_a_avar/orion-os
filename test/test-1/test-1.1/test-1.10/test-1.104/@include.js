including = {
	// JQuery & JQuery UI
	"JQuery 2.1.4"                 : "ext-libraries/js/jquery.js",
	"JQuery UI 1.11.4"             : "ext-libraries/js/jquery-ui.js",

	// Circular JSON support
	"Circular JSON"                : "ext-libraries/js/circular-json.js",

	// Orion OS
	"Constants"                    : "js/consts.js",
	"Orion OS Engine (oos engine)" : "js/oos-engine.js",
	"System Applications"          : "js/apps/sys_apps.js",
	"User Applications"            : "js/apps/usr_apps.js",
	"Orion OS Start (oos start)"   : "js/oos-start.js"
};

include();
