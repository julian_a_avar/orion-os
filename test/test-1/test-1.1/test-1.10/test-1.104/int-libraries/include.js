// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano

/*

HOW TO USE:

including = { // This is the list of packets and files
	// Description of packet 1

	"Description of file 1.1"        : "file/path/1-1.html",
	"Description of file 1.2"        : "file/path/1-2.css",

	// Description of packet 2

	"Description of file 2.1"        : "file/path/2-1.js",
	"Description of file 2.2"        : "file/path/2-2.js",
	"Description of file 2.3"        : "file/path/2-3.js",
	"Description of file 2.4 A5"     : "file/path/2-4-A5.js",
	"Description of file 2.5 QAH6.8" : "file/path/2-5-QAH6-8.css"
};

include(); // This line is necessary

INCLUDE this file in a script tag in index.html
INCLUDE above script under the name "@include.js" (or whatever is prefered) and add it after this file.

IF WANTED you could include the @include information below the folllowing script.
*/

var including = {};

function include() {
	for(var file in including) {
		var head = document.getElementsByTagName("head")[0];

		var script = document.createElement("script");

		script.src = including[file];
		script.type = "text/javascript";

		head.appendChild(script)
	}
}
