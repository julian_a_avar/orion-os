# Version plan

## Description

The plan for every version will be found inside it's respective folder, and will be written previous, during, and sometimes after it's development.

Although the Logs file contains information about what the goals are, I've found that there is the need for more technical procedure explanation.

The Logs file will contain the language information. The Logs file will be much like a log. Inside it, you will find what I think of what I am doing, what I can do to progress, comments on my previous failures, as well as successes. But it will not contain as much technical information, for that is not the purpose, the purpose is to have a place where I can find a Logs book and see what the time and place was.

The plan file will contain the plan for the entire app until now. It will define and explain every function and file. This way, I can understand what I was planning after I am lost in many lines of code.

And as I said, the plan file will contain ALL of the PROGRESS that WAS achieved and WILL be achieved. The plan file, as well as the logs file, is not definite. If I decide to change what I am doing, I must report that to the plan file during progress, and to the logs file in EDevde, once finished.

In some cases, I might find features that are both "GUI and behavior" & "Technical", if that is the case, first add it to "GUI and behavior". Everything else goes to "Technical".

## Index

1. [GUI and behavior](#gui)
	- [Past goals - Already accomplished](#gui-past)
	- [Current goals - Goals for this version](#gui-current)
	- [Short-term goals](#gui-short)
	- [Long-term goals](#gui-long)
2. [Technical](#tech)
	- [Past goals - Already accomplished](#tech-past)
		1. [The load](#tech-past-first)
		2. [Summary of files](#tech-past-second)
		3. [Detailed explanation of files](#tech-past-third)
	- [Current goals - Goals for this version](#tech-current)
	- [Short-term goals](#tech-short)
	- [Long-term goals](#tech-long)
3. [Bugs](#bugs)

## Plan

### GUI and behavior <a id="gui"></a>

#### Past goals - Already accomplished <a id="gui-past"></a>

Orion OS is be an independent app powered by `Javascript`, `HTML5`, `CSS3`, `JSON`. Orion will be able to run both online and offline.

Orion OS has a taskbar, which contains all possible apps, in the form of tiny icons called appcons. When an appcon is clicked, it's respected `div` element, illustrated as a window, is appended into the `body`, or it's `display` is set back to `block` as explained in the following paragraph. A window may be created only once.

All windows(or apps), have a head and a body. The head displays it's title and the close, minimize, and maximize/unmaximze buttons. It's body contains it's contents. The close button removes the `div` from the `body`. The maximize button sets the window to fullscreen, and once it is on fullscreen, it serves as unmaximize, which reverts it's size to what it was, as well as it's position. The minimize button set's the `display` to `none`(so like invisible), and the only way to revert this effect is by clicking it's respective appcon.

#### Current goals - Goals for this version <a id="gui-current"></a>
When unchecked, it means that this feature is in progress.

 - [ ] Get rid of the extra padding in all windows.
 - [x] Show date and time in the taskbar.
 - [x] Instead of having your appcons lined up in the taskbar taking up space, add a menu on the left, that when clicked bring up a box with all the possible apps. The taskbar will only contain the open apps. Apps can be opened as many times as specified. Add app shortcuts in the desktop.

#### Short-term goals <a id="gui-short"></a>
When checked, it means that this feature is going to be a "Current goal" in the next version.

 - [ ] Add a side panel.
 - [ ] Add animations to side panel.
 - [ ] Allow fullscreen using `Fullscreen API`, quick tuto might be found [here](https://davidwalsh.name/fullscreen)

#### Long-term goals <a id="gui-long"></a>
When checked, it means that this feature is going to be a "Short-term goal" in the next version.

 - [ ] Have files on your desktop.
 - [ ] Be able to create, edit, and delete files using a GUI.

### Technical <a id="tech"></a>

#### Past goals - already accomplished <a id="tech-past"></a>

**First**, the load: <a id="tech-past-first"></a>

1. `Jquery & Jquery UI`
<br><br>
2. `consts.js`
3. `oos-engine.js`
4. `sys_apps.js`
6. `oos-start.js`

<br><br>

**Second**, explain what each file does: <a id="tech-past-second"></a>

1. Jquery and Jquery UI are both exterior libraries which make the development of the application much faster.
<br><br>
2. This file contains a series of constant which are used through Orion OS.
3. This file contains all of the logic necessary in order to launch, create, administrate and edit everything inside orion os.
4. These are the apps defined by the system.
5. This file starts up the entire application

<br><br>

**Third**, explain EACH INDIVIDUAL FILE: <a id="tech-past-third"></a>

##### `Jquery & Jquery UI`

These are minified versions of Jquery, that provide functionality like easier to work with the DOM. These are exterior and I am not to modify them.

##### `consts.js`

This file contains all of the constants required by Orion OS.

##### `oos-start.js`

These file is the last to be run, and the one to start oos.

##### `oos-engine.js`

This is perhaps the biggest file for now, and the one which contains all of the methods, objects, and arrays required to use Orion OS.

This file contains only one variable called `oos`, which is an object

The entire object tree will be explained right here:

- `oos`
	1. `lmr`
		1. `sys` - Object. `sys` is the part of lemur that takes over the user files and user applications.
			1. `tmp`
				- Object. An object that can collect info that will later be needed to be discarded.
			2. `tmm()`
				- Funtion. Manages the information found in tmp.
			3. `aa`
				- Object. This object contains the height and width of all windows.
			4. `adp`
				- Object. App drag properties.
			5. `am` - Object. The user app manager is the responsible for creating and organizing new apps.
				1. `exists()`
						- Funtion. Can determine if an app is open when name is given.
				2. `wc()`
						- Function. Window creator creates a fully functional window out of content.
				3. `uaom()`
						- Funtion. `ua` object manager.
				4. `ac`
						- Array. Stores data for `acc()`.
				5. `acc()`
						- Function. Appcon creator is a creator of fully functional appcons out of content.
				6. `vm()`
						- Function. The variable manager grabs data from an app and creates an enviroment for the app to live out of the variables that it creates to send to other functions.
				7. `vl`
						- Object. `vl`(variable list) contains a list of apps which can be opened.
				8. `sk()`
						- Function. The "set keys" function adds data given by `vm` and fills it in `vl`.
				9. `al`
						- Array. List of app classes which can be opened. App list.
				10. `acl`
						- Array. List of app classes which can be opened. Appcon list.
				11. `saa()`
						- Function. The "Set App list and Appcon list" function adds data given by `vm` and fills it in `al` & `acl`.
				12. `duak()`
						- Funtion. Deletes the contents of `ua` upon call.
				13. `ams()`
						- Function. This function starts up `lmr.sys`'s app manager.
			6. `tbm` - Object. Manages the taskbar accordingly.
				1. `uc`
						- String. Color of appcon when app is unminimized.
				2. `mc`
						- String. Color of appcon when app is minimized.
				3. `ud`
						- String. Display type of window when app is unminimized.
				4. `md`
						- String. Display type of window when app is minimized.
				5. `acm()`
						- Function. Creates, minimizes, and unminimizes apps correspond based upon the actions of taken in the taskbar.
				6. `amm()`
						- Function. This is a minimizing manager which can determine what to do when an app is minimized.
				7. `aactl`
						- Array. The add appcon to taskbar list is an array which contains the content to all of the appcons.
				8. `aactlc()`
						- Function. The add appcon to taskbar list constructor, it uses `aactl` to construct appcons.
				9. `tbms()`
						- Function. This function starts up `lmr.sys`'s taskbar manager.
			7. `wb` - Object. Defines the basics for any windows. It is too compicated to explain here.

<br><br>

#### Current goals - Goals for this version <a id="tech-current"></a>
When unchecked, it means that this feature is in progress. When checked, it is done and will not be shown on next version.

 - [ ] Solve the appcon creator function problems. (**delayed**)
 - [ ] Object properties that are not `w`, `c`, `i`, `obj`, or `obj.{object name}`, should not only be ignored, but deleted
 - [ ] Make object-like css styling possible

#### Short-term goals <a id="tech-short"></a>
When checked, it means that this feature is going to be a "Current goal" in the next version.

 - [ ] Create a browser inside Orion OS.
 - [ ] Use hipopo.

#### Long-term goals <a id="tech-long"></a>
When checked, it means that this feature is going to be a "Short-term goal" in the next version.

 - [ ] Make app fully flexible by eliminating the need to manually edit oos engine.
 - [ ] Make an API to create, edit, and delete files.
 - [ ] Be able to create, edit and delete files, with a command prompt. Create a file browser system using commands.

### Bugs - GUI, behaviour, and technical <a id="bugs"></a>
Checked boxes are bugs being solved right now. When no checkbox, bug is on waitlist.

 - [ ] Close side panel when button is clicked again.
 - [ ] Fix sidepanel. It is looking, not very good. More layout. Make look nicer.
 - [ ] Posible Bug. `lmr.sys.aa` contains the height and width of all apps, but not individually, therefore, things could become troublesome if one window was resized.
 - [ ] It is not possible to resize windows at this moment.

## End

To reference data inside the plan, use the following notation `###.####.####.paragraph_number/list_number`.

example: bugs.1, technical.past.sys_window_basics.7.3
