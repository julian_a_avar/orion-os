// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano
var oos = {
	lmr: { // lemur
		sys: { // sys
			tmp: {}, // temporary folder
			tmm: function() { // temporary folder manager
				// every 8h delete contents of tmp
			},

			aa: { // app anatomy
				lc: "0", // coordinates from left
				tc: "0", // coordinates from left

				aw: "100vw", // app height
				ah: "100vh", // app height

				hd: ".hd", // head
				cls: ".cls", // close
				max: ".max", // maximize
				min: ".min", // minimize
				spb: ".spb", // side panel button
				sp: ".sp", // side panel
				spbckg: ".spbckg" // side panel background
			},

			adp: { // app drag properties
				sl: false,
				sp: true,
				sm: "outer",
				st: 7,
				hd: ".hd"
			},

			am: { // app manager
				exists: function(fa) { // app open?
					return $(fa).length; // 1 is true, 0 is false
				},

				wc: function(ac, an, fa) { // window creator
					return "\<div class=\"app "+ac+"\">\<div class=\"hd\"\>\<div class=\"titl\"\>"+an+"\</div\>\<div class=\"buttons\"\>\<div class=\"spb\"\>\</div\>\<div class=\"cls\"\>\</div\>\<div class=\"max\"\>\</div\>\<div class=\"min\"\>\</div\>\</div\>\</div>\<div class=\"content\"\>"+fa+"\</div>\</div>";

					// Give the basic structure for a window
				},

				uaol: function(fa, key, root) { // ua - object looper
					fa = "";
					for(var obj_name in root) {
						console.log(root[obj_name]);
						var w = root[obj_name].w;
						var c = root[obj_name].c;
						var i = root[obj_name].i;
						var o = root[obj_name].obj;


						switch(true) {
							case c && !o:
								fa += oos.lmr.sys.am.uaos(w, c, i); // make object with single content
								break;
							case o && !c:
								root = root[obj_name].obj;

								o = oos.lmr.sys.am.uaol(fa, o, root); // object looper

								fa += oos.lmr.sys.am.uaos(w, o, i); // make object with multiple children
								break;
							case (c && o) || (!c && !o):
								console.log("Error: either the content or the object have to exist, not both or none");
								break;
						}
					}

					return fa;
				},

				uaos: function(w, c, i) { // ua - object sewwer
					var bt = "\<" + w + " style=\"" + i + "\">"; // beggining tag
					var et = "\</" + w + ">"; // end tag

					var r = bt + c + et; // end result

					return r;
				},

				acr: function(fa, key, dlac, at, root) { // app creator
					fa = oos.lmr.sys.am.uaol(fa, key, root); // object looper
					fa = oos.lmr.sys.am.wc(dlac, at, fa); // create window

					return fa;
				},

				ac: [],

				acc: function(dlacc, act) { // appcon creator
					var rs = "\<div class=\"appcon " + dlacc + "\">" + act + "\</div>"// return string

					oos.lmr.sys.tbm.aactl.push(rs);
				},

				vm: function() { // variable manager
					for(var key in ua) {
						var root = ua[key].obj;

						var at = ua[key].at; // app title
						var act = ua[key].act; // appcon title
						var sp = ua[key].sp; // side panel

						var an = "app_sys_appname_" + key; // appname
						var dlac = "app_sys_appclass_" + key; // dotlessappclass
						var dlacc = "app_sys_appconclass_" + key; // dorlessappconclass

						var ac = "." + dlac; // appclass
						var acc = "." + dlacc; // appconclass

						// start making objects!
						var fa = oos.lmr.sys.am.acr(fa, key, dlac, at, root); // full app ; create app
						oos.lmr.sys.am.acc(dlacc, act); // create the appcon
						oos.lmr.sys.am.sk(an, ac, acc, sp, fa); // set the info in a gobally accesible object
						oos.lmr.sys.am.saa(dlac, acc); // get a reliable list of app names and appcon names to associate them
						oos.lmr.sys.am.duakcn(); // de`let`e the keys in ua so I can add new ones & codenames for apps
					}
				},

				vl: {}, // Variable List
				sk: function(an, ac, acc, sp, fa) { // set keys
					oos.lmr.sys.am.vl[an] = {}; // set an object with the name of the app
					oos.lmr.sys.am.vl[an].ac = ac; // set app class
					oos.lmr.sys.am.vl[an].acc = acc; // set appcon class
					oos.lmr.sys.am.vl[an].sp = sp; // set the side panel
					oos.lmr.sys.am.vl[an].fa = fa; // set the app's content
				},

				al: [], // app list
				acl: [], // appcon list

				saa: function(al, acl) { // Set App list and Appcon list
					oos.lmr.sys.am.al.push(al); // add the new app list items
					oos.lmr.sys.am.acl.push(acl); // add the new appcon list items
				},

				duakcn: function() { // delete ua keys & codename
					var k;
					// oos.lmr.sys.tmp.ua = ua.start;
					// oos.lmr.sys.tmp.ua = ua.obj;
					for(k in ua) {
						delete ua[k]; // delete all ua keys
					};
					uaacs = "", uaocs = ""; // deleting codenames by reseting them! Might use `delete` keyword later
					// ua.start = oos.lmr.sys.tmp.ua;
					// ua.obj = oos.lmr.sys.tmp.ua;
				},

				ams: function() { // app manager start
					var acc, ac, fa;
					for(key in oos.lmr.sys.am.vl) {
						acc = oos.lmr.sys.am.vl[key].acc; // set variable so its easier to access values
						ac = oos.lmr.sys.am.vl[key].ac; // set variable so its easier to access values
						fa = oos.lmr.sys.am.vl[key].fa; // set variable so its easier to access values

						oos.lmr.sys.tbm.acm(acc, ac, fa); // set the appcon manager
					}
				}
			},

			tbm: { // taskbar manager
				uc: "#ffffff", // unminimized appcon color
				mc: "#aaaaaa", // minimized appcon color

				ud: "block", // unminimized display
				md: "none", // minimized display

				acm: function(acc, ac, fa) { // appcon manager
					$(acc).click(function() {
						if(oos.lmr.sys.am.exists(ac)) {
							if($(ac).hasClass("mined")) {
								$(ac).css({"display": oos.lmr.sys.tbm.ud});
								$(acc).css({"background-color": oos.lmr.sys.tbm.uc});
								$(ac).removeClass("mined");
							} else {
								alert("app already exists");
							}
						} else {
							$body.append(fa);
							oos.lmr.sys.wb.hd(); // window basics
						}
					});
				},

				amm: function(t) { // app minimizing manager
					for(i = 0; i < oos.lmr.sys.am.al.length; i++) { // Browse through the apps
						if($(t).parents(app).hasClass(oos.lmr.sys.am.al[i])) { // until you find the the right one
							$(oos.lmr.sys.am.acl[i]).css({
								"background-color": oos.lmr.sys.tbm.mc
							});
						}
					}
				},

				aactl: [], // add appcon to taskbar list

				aactlc: function() { // add appcon to taskbar list constructor
					var taskbar_ = "\<div id=\"taskbar\">\<div id=\"start_menu\">\</div>\<div id=\"menu\">\</div>\</div>";
					$body.append(taskbar_);
					// console.log("There is a total of " + oos.lmr.sys.tbm.aactl.length + " apps");
					for(var i = 0; i < oos.lmr.sys.tbm.aactl.length; i++) {
						$("#menu").append(oos.lmr.sys.tbm.aactl[i]);
					}
				},

				tbms: function() { // taskbar manager start
					oos.lmr.sys.am.ams(); // starts up the appcon manager
				}
			},

			wb: { // window basics
				zindex: 0,
				hd: function() {
					$(app).draggable({
						scroll: oos.lmr.sys.adp.sl,
						snap: oos.lmr.sys.adp.sp,
						snapMode: oos.lmr.sys.adp.sm,
						snapTolerance: oos.lmr.sys.adp.st,
						handle: oos.lmr.sys.adp.hd
					}).resizable();

					$(app).mousedown(function() {
						$(this).css({
							"z-index": oos.lmr.sys.wb.zindex
						});
						// oos.lmr.sys.wb.zindex = oos.lmr.sys.wb.zindex + 1;
						oos.lmr.sys.wb.zindex++;
					});

					$(oos.lmr.sys.aa.cls).click(function() {
						$(this).parents(app).remove();
					});

					$(oos.lmr.sys.aa.max).click(function() {
						switch($(this).parents(app).hasClass("maxd")) {
							case true:
								$(this).parents(app).css({
									"top": oos.lmr.sys.aa.tc,
									"left": oos.lmr.sys.aa.lc,
									"width": oos.lmr.sys.aa.aw,
									"height": oos.lmr.sys.aa.ah
								});
								oos.lmr.sys.aa.lc = $(this).parents(app).css("left");
								oos.lmr.sys.aa.tc = $(this).parents(app).css("top");
								oos.lmr.sys.aa.ah = $(this).parents(app).css("height");
								oos.lmr.sys.aa.aw = $(this).parents(app).css("width");
								$(this).parents(app).removeClass("maxd");
								break;
							case false:
								oos.lmr.sys.aa.lc = $(this).parents(app).css("left");
								oos.lmr.sys.aa.tc = $(this).parents(app).css("top");
								oos.lmr.sys.aa.ah = $(this).parents(app).css("height");
								oos.lmr.sys.aa.aw = $(this).parents(app).css("width");
								$(this).parents(app).css({
									"top": "0",
									"left": "0",
									"width": "100vw",
									"height": "100vh"
								});
								$(this).parents(app).addClass("maxd");
								break;
						}
					});

					$(oos.lmr.sys.aa.min).click(function() {
						if(!$(this).parents(app).hasClass("mined")) {
							$(this).parents(app).css({
								"display": oos.lmr.sys.tbm.md
							});

							oos.lmr.sys.tbm.amm(this);

							$(this).parents(app).addClass("mined");
						}
					});

					$(oos.lmr.sys.aa.spb).click(function() {
						switch($(this).parents(app).hasClass("sp_open")) {
							case true:
								$(this).parents(app).removeClass("sp_open");
								break;
							case false:
								$(this).parents(app).append($sp + $sp_background);
								$(this).parents(app).find(oos.lmr.sys.aa.spbckg).css({
									"width": "100%",
									"height": "2em",
									"background-color": "rgba(0, 0, 0, 0.1)"
								});
								$(this).parents(app).find(oos.lmr.sys.aa.sp).css({
									"width": "20%",
									"height": "2em",
									"background-color": "red"
								});
								$(this).parents(app).addClass("sp_open");
								break;
						}
					});
				}
			}
		}
	}
};
