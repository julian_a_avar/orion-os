// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano.

/*
ua => user apps | is a reusable object that continuisly gets filled, emptied, and refilled.
ua is a list containing the content of the application. To add other programs, you need to select ua.af (additional features).

Adding objects other than one simple object can become a little more difficult.
First, `ua.reader.content` must have the tag `{multiple}`, then you must add the objects.
Adding objects requires four steps. First add a new statement `ua.reader.obj.{name of obj}`, then `ua.reader.obj.{name of obj}.w` must equal to the tag name of the wrapper obj, in a string.
Third, you can go ahead to add your content by writing `ua.reader.obj.{name of obj}.c` equal to the content. Notice that you may make nested objs by using the `{multiple}` tag again. And fourth, you can style it! Instead of a "c" ot "w", use an "s" for the style.

All of these are familiar to having a terminal. They are mostly single line snippets.
*/

uasi("reader");
uasoi("text");

ua.reader.obj.text.w = "p"; // wrapper
ua.reader.obj.text.c = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis."; // content
ua.reader.obj.text.i = "background-color: #333333; color: #eeeeee"; // interface

uasoi("button_cont");
ua.reader.obj.button_cont.w = "div";

uasoi("button_cont", "previous");
ua.reader.obj.button_cont.obj.previous.w = "button";
ua.reader.obj.button_cont.obj.previous.c = "previous";

uasoi("button_cont", "next");
ua.reader.obj.button_cont.obj.next.w = "button";
ua.reader.obj.button_cont.obj.next.c = "next";

ua.reader.at = "Reader"; // apptitle
ua.reader.act = "R"; // appcontitle
ua.reader.sp = "This is a sample application that displays text"; // side panel

oos.lmr.sys.am.vm();



uasi("notepad");
uasoi("textarea");
ua.notepad.obj.textarea.w = "textarea";
// ua.notepad.cws = {
// 	"width": "100%";
// 	"background-color": "#00000";
// 	"border": "none"
// };
ua.notepad.obj.textarea.c = "Welcome to Orion OS.\n This is one of the firsts interactive apps in this platform!";
ua.notepad.obj.textarea.i = "width: 100%; height: 100%; background-color: #00000; border: none; padding-top: 1em; margin-top: 3px"; // content wrapper interface

ua.notepad.at = "NotePad";
ua.notepad.act ="NP";
ua.notepad.sp = "<h1>Notepad</h1><p>This is my first attempt to construct an interactive application</p><p>This app, can edit text, plain text, with no formating for now :(</p>";
oos.lmr.sys.am.vm();
