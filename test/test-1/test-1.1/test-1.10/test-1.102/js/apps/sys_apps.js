// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano.

/*
ua => user apps | is a reusable object that continuisly gets filled, emptied, and refilled.
ua is a list containing the content of the application. To add other programs, you need to select ua.af (additional features).

The format is as follows:
1. create a property inside `ua` named after the codename of your application and make it an object
2. create a porperty inside you applications codename called `content`, which will contain the contents of your application
  a. "{" & "}" must be escaped like the following ":{;" & ":};".
	b. In between nonescaped curly braces, you may have codes that do different tasks:
		I. {empty} - there is no content to the app. If this is not included, it will cause an error.
		II. {multiple} - meaning that there is more than one object in the application. This will make life easier than having one content and then having to add objects for the rest.
3. create a porperty inside you applications codename called `at`, which will contain the window head title of your application
4. create a porperty inside you applications codename called `act`, which will contain the appcon app name of your application
5. call `lmr.sys.am.vm()`

You can wrap ALL of your content in a tag by specifying ua.reader.cw and then styling it by applying stylies in ua.reader.cws
The content wrapper is a div with class of "`appname`-obj-div", and after you specify a new content wrapper, a new tag inside of the div will be generated wiht a class of "`appname`-obj-div-`tagname`". You can do this as many time as wanted.

Adding objects other than one simple object can become a little more difficult.
First, `ua.reader.content` must have the tag `{multiple}`, then you must add the objects.
Adding objects requires four steps. First add a new statement `ua.reader.obj.{name of obj}`, then `ua.reader.obj.{name of obj}.w` must equal to the tag name of the wrapper obj, in a string.
Third, you can go ahead to add your content by writing `ua.reader.obj.{name of obj}.c` equal to the content. Notice that you may make nested objs by using the `{multiple}` tag again. And fourth, you can style it! Instead of a "c" ot "w", use an "s" for the style.

All of these are familiar to having a terminal. They are mostly single line snippets.
*/

// ua.reader.content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.";
//
// ua.reader.cw = "p"; // content wrapper
// ua.reader.cws = "";
// // ua.reader.cws = {}; // content wrapper style
//
// ua.reader.obj.next = "button";

uasi("reader");
uasoi("text");
ua.reader.obj.text.w = "p"; // wrapper
ua.reader.obj.text.c = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis."; // content
ua.reader.obj.text.i = ""; // interface

uasoi("previous");
ua.reader.obj.previous.w = "button";
ua.reader.obj.previous.c = "previous";
ua.reader.obj.previous.i = "";

uasoi("next");
ua.reader.obj.next.w = "button";
ua.reader.obj.next.c = "next";
ua.reader.obj.next.i = "";

ua.reader.at = "Reader"; // apptitle
ua.reader.act = "R"; // appcontitle
ua.reader.sp = "This is a sample application that displays text"; // side panel
oos.lmr.sys.am.vm();



uasi("notepad");
// ua.start();

uasoi("textarea");
ua.notepad.obj.textarea.w = "textarea";
// ua.notepad.cws = {
// 	"width": "100%";
// 	"background-color": "#00000";
// 	"border": "none"
// };
ua.notepad.obj.textarea.c = "";
ua.notepad.obj.textarea.i = "width: 100%; background-color: #00000; border: none"; // content wrapper interface

ua.notepad.at = "NotePad";
ua.notepad.act ="NP";
ua.notepad.sp = "<h1>Notepad</h1><p>This is my first attempt to construct an interactive application</p><p>This app, can edit text, plain text, with no formating for now :(</p>";
oos.lmr.sys.am.vm();
