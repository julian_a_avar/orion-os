// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano
var oos = {
	lmr: { // lemur
		sys: { // sys
			aa: { // app anatomy
				lc: "0", // coordinates from left
				tc: "0", // coordinates from left

				aw: "100vw", // app height
				ah: "100vh", // app height

				hd: ".hd", // head
				cls: ".cls", // close
				max: ".max", // maximize
				min: ".min", // minimize
				sp: ".sp" // side panel
			},

			adp: { // app drag properties
				sl: false,
				sp: true,
				sm: "outer",
				st: 7,
				hd: ".hd"
			},

			am: { // app manager
				// TODO:0 Add new note about the variable model
				/*
				Variable model:
				app_sys_apptitle_appname => appname in string => example: app_sys_apptitle_reader = "Reader"
				app_sys_appcontitle_appname => appconname in string => example: app_sys_appcontitle_reader = "R"
				app_sys_fullapp_appname => content of app => example: app_sys_fullapp_reader = "blablabla"
				app_sys_fullappcon_appname => content of appcon => example: app_sys_fullappcon_reader = "blablabla"
				app_sys_appclass_appclass => app class => example: app_sys_appclass_reader = ".app_sys_appclass_reader"
				app_sys_appclass_appconclass => appcon class => example: app_sys_appclass_reader = ".app_sys_appclass_reader"
				*/

				exists: function(fa) { // app open?
					return $(fa).length;
				},

				wc: function(ac, an, fa) { // window creator
					return "<div class=\"app " + ac + "\"><div class=\"hd\"><div class=\"title\">" + an + "</div><div class=\"buttons\"><div class=\"sp\"></div><div class=\"cls\"></div><div class=\"max\"></div><div class=\"min\"></div></div></div><div class=\"content\">" + fa + "</div></div>"
				},

				ac: [],

				acc: function(dlacc, act) { // appcon creator
					// var rs = "<div class=\"appcon " + dotlessappconclass + "\">" + appcontitle + "</div>"// return string
					// oos.lmr.sys.tbm.aactl.push(rs);
					// for(i = 0; i < oos.lmr.sys.tbm.aactl.length; i++) {
					// console.log("there is " + oos.lmr.sys.tbm.aactl[i] + "items!");
					// }
				},

				vm: function() { // variable manager
					for(key in ua) {
						var at = ua[key].at; // app title
						var act = ua[key].act; // appcon title

						var an = "app_sys_appname_" + key; // appname
						var ac = ".app_sys_appclass_" + key; // appclass
						var acc = ".app_sys_appconclass_" + key; // appconclass

						var dlac = ac.substring(1); // dotlessappclass
						var dlacc = acc.substring(1); // dorlessappconclass

						var fa = ua[key].content; // full app
						fa = oos.lmr.sys.am.wc(dlac, at, fa); // create window

						// TODO: WTF is going on right here?
						oos.lmr.sys.am.acc(dlacc, act);
						oos.lmr.sys.am.sk(an, ac, acc, fa);
						oos.lmr.sys.am.saa(dlac, acc);
						oos.lmr.sys.am.duak();
					}
				},

				vl: {}, // Variable List
				sk: function(an, ac, acc, fa) { // set keys
					oos.lmr.sys.am.vl[an] = {};
					oos.lmr.sys.am.vl[an].ac = ac;
					oos.lmr.sys.am.vl[an].acc = acc;
					oos.lmr.sys.am.vl[an].fa = fa;
				},

				al: [], // app list
				acl: [], // appcon list

				saa: function(al, acl) { // Set App list and Appcon list
					oos.lmr.sys.am.al.push(al);
					oos.lmr.sys.am.acl.push(acl);
				},

				duak: function() { // delete ua keys
					var k;
					for(k in ua) {
						delete ua[k];
					};
				},

				ams: function() { // app manager start
					var acc, ac, fa;
					for(key in oos.lmr.sys.am.vl) {
						acc = oos.lmr.sys.am.vl[key].acc;
						ac = oos.lmr.sys.am.vl[key].ac;
						fa = oos.lmr.sys.am.vl[key].fa;

						oos.lmr.sys.tbm.acm(acc, ac, fa);
					}
				}
			},

			tbm: { // taskbar manager
				uc: "#ffffff", // unminimized appcon color
				mc: "#aaaaaa", // minimized appcon color

				ud: "block", // unminimized display
				md: "none", // minimized display

				acm: function(acc, ac, fa) { // appcon manager
					$(acc).click(function() {
						if(oos.lmr.sys.am.exists(ac)) {
							if($(ac).hasClass("mined")) {
								$(ac).css({"display": oos.lmr.sys.tbm.ud});
								$(acc).css({"background-color": oos.lmr.sys.tbm.uc});
								$(ac).removeClass("mined");
							} else {
								alert("app already exists");
							}
						} else {
							$body.append(fa);
							oos.lmr.sys.wb.hd();
						}
					});
				},

				amm: function(t) { // app minimizing manager
					for(i = 0; i < oos.lmr.sys.am.al.length; i++) { // Browse through the apps
						if($(t).parents(app).hasClass(oos.lmr.sys.am.al[i])) { // until you find the the right one
							$(oos.lmr.sys.am.acl[i]).css({
								"background-color": oos.lmr.sys.tbm.mc
							});
						}
					}
				},

				aactl: ["<div class=\"appcon app_sys_appconclass_app1\">A1</div>", "<div class=\"appcon app_sys_appconclass_app2\">A2</div>", "<div class=\"appcon app_sys_appconclass_reader\">R</div>", "<div class=\"appcon app_sys_appconclass_notepad\">NP</div>"], // add appcon to taskbar list
				// aactl: [], // add appcon to taskbar list
				aactlc: function() { // add appcon to taskbar list constructor
					var taskbar_ = "<div id=\"taskbar\"><div id=\"start_menu\"></div><div id=\"menu\"></div></div>";
					$body.append(taskbar_);
					// console.log("There is a total of " + oos.lmr.sys.tbm.aactl.length + " apps");
					for(var i = 0; i < oos.lmr.sys.tbm.aactl.length; i++) {
						$("#menu").append(oos.lmr.sys.tbm.aactl[i]);
					}
				},

				tbms: function() { // taskbar manager start
					oos.lmr.sys.am.ams();
				}
			},

			wb: { // window basics
				zindex: 0,
				hd: function() {
					$(app).draggable({
						scroll: oos.lmr.sys.adp.sl,
						snap: oos.lmr.sys.adp.sp,
						snapMode: oos.lmr.sys.adp.sm,
						snapTolerance: oos.lmr.sys.adp.st,
						handle: oos.lmr.sys.adp.he
					}).resizable();

					$(app).mousedown(function() {
						$(this).css({
							"z-index": oos.lmr.sys.wb.zindex
						});
						// oos.lmr.sys.wb.zindex = oos.lmr.sys.wb.zindex + 1;
						oos.lmr.sys.wb.zindex++;
					});

					$(oos.lmr.sys.aa.cls).click(function() {
						$(this).parents(app).remove();
					});

					$(oos.lmr.sys.aa.max).click(function() {
						switch($(this).parents(app).hasClass("maxd")) {
							case true:
								$(this).parents(app).css({
									"top": oos.lmr.sys.aa.tc,
									"left": oos.lmr.sys.aa.lc,
									"width": oos.lmr.sys.aa.aw,
									"height": oos.lmr.sys.aa.ah
								});
								oos.lmr.sys.aa.lc = $(this).parents(app).css("left");
								oos.lmr.sys.aa.tc = $(this).parents(app).css("top");
								oos.lmr.sys.aa.ah = $(this).parents(app).css("height");
								oos.lmr.sys.aa.aw = $(this).parents(app).css("width");
								$(this).parents(app).removeClass("maxd");
								break;
							case false:
								oos.lmr.sys.aa.lc = $(this).parents(app).css("left");
								oos.lmr.sys.aa.tc = $(this).parents(app).css("top");
								oos.lmr.sys.aa.ah = $(this).parents(app).css("height");
								oos.lmr.sys.aa.aw = $(this).parents(app).css("width");
								$(this).parents(app).css({
									"top": "0",
									"left": "0",
									"width": "100vw",
									"height": "100vh"
								});
								$(this).parents(app).addClass("maxd");
								break;
						}
					});

					$(oos.lmr.sys.aa.min).click(function() {
						if(!$(this).parents(app).hasClass("mined")) {
							$(this).parents(app).css({
								"display": oos.lmr.sys.tbm.md
							});

							oos.lmr.sys.tbm.amm(this);

							$(this).parents(app).addClass("mined");
						}
					});

					$(oos.lmr.sys.aa.sp).click(function() {

						switch($(this).parents(app).hasClass("sp_open")) {
							case true:
								$(this).parents(app).removeClass("sp_open");
								break;
							case false:
								$(this).parents(app).append("<aside id=\"sp\"></aside><div id=\"sp_background\"></div>");
								$(this).parents(app).addClass("sp_open");
								break;
						}
					});
				}
			}
		}
	}
};
