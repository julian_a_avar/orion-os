// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano.

/*
ua => user apps | is a reusable object that continuisly gets filled, emptied, and refilled.
ua is a list containing the content of the application. To add other programs, you need to select ua.af (additional features).

The format is as follows:
1. create a property inside `ua` named after the codename of your application and make it an object
2. create a porperty inside you applications codename called `content`, which will contain the contents of your application
3. create a porperty inside you applications codename called `at`, which will contain the window head title of your application
4. create a porperty inside you applications codename called `act`, which will contain the appcon app name of your application
5. call `lmr.sys.am.vm()`
*/

ua.reader = {};
ua.reader.content = "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>";
ua.reader.at = "Reader"; // apptitle
ua.reader.act = "R"; // appcontitle
ua.reader.sp = "This is a sample application that displays text"; // side panel
oos.lmr.sys.am.vm();

ua.notepad = {};
ua.notepad.content = "<textarea id=\"notepad_textarea\" style=\"width:100%;background-color:#00000;border:none\"></textarea>";
ua.notepad.at = "NotePad";
ua.notepad.act ="NP";
ua.notepad.sp = "<h1>Notepad</h1><p>This is my first attempt to construct an interactive application</p><p>This app, can edit text, plain text, with no formating for now :(</p>";
oos.lmr.sys.am.vm();
