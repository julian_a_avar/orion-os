// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano
var usrtbm = { // taskbar manager
	unmnmzd_clr: "#ffffff", // unminimized appcon color
	mnmzd_clr: "#aaaaaa", // minimized appcon color

	unmnmzd_dsply: "block", // unminimized display
	mnmzd_dsply: "none", // minimized display

	exists: function(fullapp) { // app open?
		return $(fullapp).length;
	},

	acm: function(appconclass, appclass, fullapp) { // appcon manager
		$(appconclass).click(function() {
			if(usrtbm.exists(appclass)) {
				if($(appclass).hasClass("minimized")) {
					$(appclass).css({"display": usrtbm.unmnmzd_dsply});
					$(appconclass).css({"background-color": usrtbm.unmnmzd_clr});
					$(appclass).removeClass("minimized");
				} else {
					alert("app already exists");
				}
			} else {
				$body.append(fullapp);
				usr_window_basics();
			}
		});
	},

	amm: function(t) { // app minimizing manager
		for(i = 0; i < usram.usr_al.length; i++) { // Browse through the apps
			if($(t).parents(app).hasClass(usram.usr_al[i])) { // until you find the the right one
				$(usram.usr_acl[i]).css({
					"background-color": usrtbm.mnmzd_clr
				});
			}
		}
	},

	aactlc: function() { // add appcon to taskbar list constructor
		var taskbar_start = "<div id=\"taskbar\"><div id=\"start_menu\"></div><div id=\"menu\"><div class=\"appcon appcon1\">W1</div><div class=\"appcon appcon2\">W2</div>";
		var taskbar_end = "</div></div>";
		var taskbar_ = taskbar_start + systbm.aactl[0] + taskbar_end;
		$body.append(taskbar_);
	},

	usras: function() {
		usram.usras();
	}
};
