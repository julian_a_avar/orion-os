# TEST 1.090 plan

## Description

The plan for every version will be found inside it's respective folder, and will be written previous to it's development.

Although the History file contains information about what are the goals, I've found that there is the need for more technical procedure explanation.

The History file will contain the language information. The History file will be much like a log. Inside it, you will find what I think of what I am doing, what I can do to progress, comments on my previous failures, as well as successes. But it will not contain technical information, for that is not the purpose, the purpose is to have a place where I can find a History book and see what the time and place was, and "WTF was I thinking!!!???".

The plan file will contain the plan for the entire app until now. It will define and explain every function and file. This way, I can understand what I was planning after I am lost in many lines of code.

And as I said, the plan file will contain ALL of the PROGRESS that WAS achieved and WILL be achieved. The plan file, as well as the history file, is not definite. If I decide to change what I am doing, I must report that to the plan file during progress, and to the history file in EDevde, once finished.

## Plan

### GUI and behavior

#### Past(goals - already accomplished)

Orion OS is be an independent app powered by `Node.js`, `Javascript`, `HTML5`, `CSS3`, `JSON`. Orion will be able to run both online and offline.

Orion OS has a taskbar, which contains all possible apps, in the form of tiny icons called appcons. When an appcon is clicked, it's respected `div` element, illustrated as a window, is appended into the `body`, or it's `display` is set back to `block` as explained in the following paragraph. A window may be created only once.

All windows(or apps), have a head and a body. The head displays it's title and the close, minimize, and maximize/unmaximze buttons. It's body contains it's contents. The close button removes the `div` from the `body`. The maximize button sets the window to fullscreen, and once it is on fullscreen, it serves as unmaximize, which reverts it's size to what it was, as well as it's position. The minimize button set's the `display` to `none`(so like invisible), and the only way to revert this effect is by clicking it's respective appcon.

#### Current(goals - Goals for this version)

None.

#### Short(term goals)

Change the z-index of windows when clicked on.

Instead of having your appcons lined up in the taskbar taking up space, add a menu on the left, that when clicked bring up a box with all the possible apps. The taskbar will only contain the open apps. Apps can be opened as many times as specified. Add app shortcuts in the desktop.

#### Long(term goals)

Be able to create, edit and delete files, with a command prompt. Have files on your desktop. Create a file browser system using commands and later a GUI. Be able to create, edit, and delete files using a GUI.

Create a browser inside Orion OS.

### Technical

#### Past(goals - already accomplished)

First, the load:

 1. Jquery & Jquery UI
<br><br>
 2. sys_taskbar_manager.js
 3. sys_window_basics.js
 4. taskbar.js
 5. sys_apps.js
 6. sys_start.js
<br><br>
 7. usr_app_manager.js
 8. usr_taskbar_manager.js
 9. usr_window_basics.js
 10. reader.js
 11. usr_start.js

Second, explain what each file does:

 1. Jquery and Jquery UI are both exterior libraries which make the development of the application much faster.
<br><br>
 2. Load the taskbar manager, since I first need to make sure that I can properly deal with opening, closing, and un/minimizing applications.
 3. Add the functions that are shared by all apps, such as closing, minimizing, maximizing and unmaximizing.
 4. Load the taskbar, or the buttons to properly deal with opening, closing, and un/minimizing.
 5. Load the main applications that are added with the OS.
 6. Now start Orion with all of the functions and features explained above.
<br><br>
 7. Load a manager for the variables and location of all apps(instead of hardcoding it into the taskbar manager).
 8. Load a flexible taskbar manager, which takes advantage of the app manager to easilly create new applications.
 9. Load the same functions shared by all apps, but this time, add the list from app manager instead of hardcoded ones from  the taskbar manager.
 10. Create an application.
 11. Initialize user applications.

Third, explain EACH INDIVIDUAL FILE:

##### Jquery & Jquery UI

These are minified versions of Jquery, that provide functionality like easier to work with the DOM. These are exterior and I am not to modify them.

##### sys_taskbar_manager.js

This file has functions to manage the taskbar apropiately, using the SYSTEM rules.

This file includes 9 variables.

`i` and `$body` are both constants. `i` is used solely on loops. `$body` is a variable used in order to append and check what exactly is itself.

`app1`, `app2`, `appcon1`, and `appcon2` are also constants, with the difference that these are used to manage apps. Later these will be replaced by a system similar to the one used in the user system.

`al` and `acl` are both arrays which purpose is to identify that for every appclass, there is a respective appconclass. These two only serve purpose on system applications.

Finally, the main event, the object `systbm`.

`systbm` has the following properties:

 1. `unmnmzd_clr`
  - string. color of not minimized appcon
 2. `mnmzd_clr`
  - string. color of minimized appcon
 3. `unmnmzd_dsply`
  - string. color of not minimized appcon
 4. `mnmzd_dsply`
  - string. color of not minimized appcon
 5. `exists()`
  - method. determines if a certain class is existant inside the DOM
 6. `acm()`
  - method. determines weather to unminimize or create a new app
 7. `amm()`
  - method. determines what to do to minimize a window
 8. `aactl`
  - array. contains the html for all the apps appcons
 9. `aactlc()`
  - method. creates new appcons and appends them to `#taskbar` using the array `aactl`

##### sys_window_basics.js

This file deals with the everyday life of any default SYSTEM window.

This file contains 4 variables.

`app` is a constant that defines the class that every app has to have.

<!-- NOTE: app should be later be moved to systbm -->

`sysaa` is the apps anatomy, it contains all of the graphic information as well as the classes for every `window_basics` GUI. This does contain the information to properly un/maximize.

`sysapp_dragprop` contains the required information to make a window draggable. It's purpose is to be able to contain things in a more properly organized way.

The main reason for this file however is `sys_window_basics`, a function containing a series of JQuery queries to deal with the basic window.

Inside this function you may find four main queries:

 1. `$(app).draggable();`
  - makes all windows draggable
 2. `$(sysaa.close).click();`
  - defines what to do when close button is clicked, delete that window from the DOM
 3. `$(sysaa.maximize).click();`
  - defines what to do when maximize button is clicked, fullscreen or contrary based upon its current state
 4. `$(sysaa.minimize).click();`
  - defines what to do when minimize button is clicked, change the CSS settings appropiatelly to hide the current window from user sight

Each query has it's own logic.

##### taskbar.js

This file loads `systbm.aactlc()`, which of curse loads the taskbar.

<!-- NOTE: might change later, this might end up being in sys_start.js, therefore deleting this file -->

##### sys_apps.js

This file contains two constants, the two apps which are contained by the system; `app_1` & `app_2`.

##### sys_start.js

This file loads `sys_window_basics()` and `systbm.sysas()`, which loads all the basic operations of a window, as well as the apps.

<!-- NOTE: there is no need to load sys_window_basics() since this is the sys system therefore there is no need to further make any of that.-->

##### usr_app_manager.js

This file contains only one variable object `usram` containing a bunch of methods.

The methods inside `usram`, include:

 1. `wc()`
  - method. creates html for a window utilizing the content given by the app.
 2. `acc()`
  - method. creates appcons. experimental.
 3. `vm()`
  - method. calls `wc()`, `acc()`, `sk()`, and `saa()`, and provides them all with the necessary information.
 4. `vl`
  - object. contains all of the apps with their information.
 5. `sk()`
  - method. appends all the apps information to `vl`.
 6. `usr_al`
  - array. contains the app classes of all apps in order.
 7. `usr_acl`
  - array. contains the appcon classes of all apps in order.
 8. `saa()`
  - method. pushes the classes to `usr_al` & `usr_acl`.
 9. `usras()`
  - mehod. calls `usrtbm.acm()` with the required information.

##### usr_taskbar_manager.js

This file has functions to manage the taskbar apropiately, using the USER rules.

This file contains only one variable `usrtbm`

`usrtbm` has the following properties:

 1. `unmnmzd_clr`
  - string. color of not minimized appcon
 2. `mnmzd_clr`
  - string. color of minimized appcon
 3. `unmnmzd_dsply`
  - string. color of not minimized appcon
 4. `mnmzd_dsply`
  - string. color of not minimized appcon
 5. `exists()`
  - method. determines if a certain class is existant inside the DOM
 6. `acm()`
  - method. determines weather to unminimize or create a new app
 7. `amm()`
  - method. determines what to do to minimize a window
 9. `aactlc()`
  - method. creates new appcons and appends them to `#taskbar` using the array `aactl`

##### usr_window_basics.js

This file deals with the everyday life of any default USER window.

This file contains 3 variables.

`usraa` is the apps anatomy, it contains all of the graphic information as well as the classes for every `window_basics` GUI. This does contain the information to properly un/maximize.

`usrapp_dragprop` contains the required information to make a window draggable. It's purpose is to be able to contain things in a more properly organized way.

The main reason for this file however is `usr_window_basics`, a function containing a series of JQuery queries to deal with the basic window.

Inside this function you may find four main queries:

 1. `$(app).draggable();`
  - makes all windows draggable
 2. `$(usraa.close).click();`
  - defines what to do when close button is clicked, delete that window from the DOM
 3. `$(usraa.maximize).click();`
  - defines what to do when maximize button is clicked, fullscreen or contrary based upon its current state
 4. `$(usraa.minimize).click();`
  - defines what to do when minimize button is clicked, change the CSS settings appropiatelly to hide the current window from user sight

Each query has it's own logic.


##### reader.js

This is the first application that can be created by not using hard coding but rather a bunch of functions to control that.

It contains link and variables to properly create that application.

##### usr_start.js

Start up the systems functionality.

Fourth, walk through the steps that initialize the application:

Here I will explain step by step how the application loads and responds to user input.

 1. start
  1. makes sure I can use JQuery/UI an all files
  2. make sure I can use all of the methods and keys inside `sys_taskbar_manager.js` and `sys_window_basics.js`
	3. run `systbm.aactlc()` as stated in `taskbar.js`
	 - generate all the taskbar and appcons
	4. Load the system apps
	5. run `systbm.sysas()` as stated in `sys_start.js`
	 1. run `systbm.acm()` with all the information required to start the system applications
	 2. get ready for a user to click on an appcon
	6. make sure I can use all of the methods and keys inside `usr_app_manager.js`, `usr_taskbar_manager.js` and `usr_window_basics.js`
	7. load `reader`, the usr app
   - run `usram.vm()`
    1. run `wc()` and put it in `fullapp`
    2. run `acc()`
		 - set the apps appcon
    3. run `sk()`
		 - get `vl` the information needed to properly store the app
    4. run `saa()`
		 - get `usr_al` and `usr_acl` the proper information
	8. run `usrtbm.usras()` as stated in `usr_start.js`
	 - run `usram.usras()`
	  1. run `usrtbm.acm()` and give it the information provided in 7.3
		2. get ready for a user to click on an appcon


Inputs in 5.2 and 8.2

In either one, the process will much like this(taking the example of usr):

 9. user clicked on appcon of application
  - if the application does not exist
   1. append the application to `body`
  	2. run `usr_window_basics()`
  	 1. make the window draggable
  	 2. get ready for a user to click on `.close`
  	 3. get ready for a user to click on `.maximize`
  	 4. get ready for a user to click on `.minimize`

If user clicks `.close`

 10. user clicked `.close`
  - remove application

If user clicked `.maximize`

 11. user clicked `.maximize`
  1. if window has class `.maximized`
	 1. revert the state of the application to the position and size that was saved before adding `.maximized`
	 2. remove `.maximized`
	2. else
	 1. save its current position and size
	 2. set the application to cover the who screen
	 3. add the `.maximized`

If user clicks `.minimize`

 12. user clicked `.minimize`
  1. if application does not have the `.minimized` class
	 1. hide the application visualy
	 2. run `amm()`
	  - color it's corresponding appcon
	 3. add the class `.minimized`

If a window is minimized

 13. user clicked on appcon from application that is loaded
  - if the application exists
	 1. if the application has `.minimized`
	  1. make it visible again
		2. uncolor the appcon
		3. remove `.minimized` class
	 2. else
	  - alert the user that the application already exists and therefore cannot be opened again

#### Current(goals - Goals for this version)

None.

#### Short(term goals)

Create a new file for every constant.

Combine all variables and methods as well as functions into a single object.

Use hipopo.

Change the z-index of windows when clicked on.

Change the sys system to replicate the state of the usr system.

#### Long(term goals)

Be able to create, edit and delete files, with a command prompt. Have files on your desktop. Create a file browser system using commands and later a GUI. Be able to create, edit, and delete files using a GUI.

Create a browser inside Orion OS.

### Bugs(GUI, behaviour, and technical)

 1. If I open a new app and there is already one open, the already open's maximize function wont work(probably something wrong with `window basics`). Unexpected behavior.
 2. appcon for reader is not being created

## End

To reference data inside the plan, use the following notation ###.####.####.paragraph_number/list_number.

example: bugs.1, technical.past.sys_window_basics.7.3
