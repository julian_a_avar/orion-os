// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano
$(function() {
	// sys system
	lmr.sys.tbm.aactlc(); // initialize taskbar
	lmr.sys.tbm.tbms(); // initialize orion os

	// usr system
	lmr.usr.tbm.tbms(); // initialize orion os
});
