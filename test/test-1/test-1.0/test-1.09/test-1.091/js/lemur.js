// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano
var lmr = {
	sys: { // sys lemur
		al: ["app1", "app2"], // app list
		acl: [appcon1, appcon2], // appcon list

		aa: { // app anatomy
			left_coord: "0", // coordinates from left
			top_coord: "0", // coordinates from left

			app_width: "100vw", // app height
			app_height: "100vh", // app height

			head: ".head",
			close: ".close",
			maximize: ".maximize",
			minimize: ".minimize"
		},

		adp: { // sys app drag properties
			sl: false, // al scroll
			sp: true, // sp snap
			sm: "outer", // sm snapMode
			st: 7, // at snapTolerance
			he: ".head" // he handle
		},

		tbm: { // taskbar manager
			uc: "#ffffff", // unminimized appcon color
			mc: "#aaaaaa", // minimized appcon color

			ud: "block", // unminimized display
			md: "none", // minimized display

			exists: function(app_name) { // app open?
				return $(app_name).length;
			},

			acm: function(appcon_name, app_name, app_content_name) { // appcon manager
				$(appcon_name).click(function() {
					if(lmr.sys.tbm.exists(app_name)) {
						if($(app_name).hasClass("minimized")) {
							$(app_name).css({"display":lmr.sys.tbm.ud});
							$(appcon_name).css({"background-color":lmr.sys.tbm.uc});
							$(app_name).removeClass("minimized");
						} else {
							alert("app already exists");
						}
					} else {
						$body.append(app_content_name);
						lmr.sys.wb();
					}
				});
			},

			amm: function(element_this) { // When minimizing and app, do amm(), app minimizing manager
				for(i = 0; i < lmr.sys.al.length; i++) {
					if($(element_this).parents(app).hasClass(lmr.sys.al[i])) {
						$(lmr.sys.acl[i]).css({
							"background-color": lmr.sys.tbm.mc
						});
					}
				}
			},

			aactl: ["<div class=\"appcon app_usr_appconclass_reader\">R</div>"], // add appcon to taskbar list
			aactlc: function() { // add appcon to taskbar list constructor
				var taskbar_ = "<div id=\"taskbar\"><div id=\"start_menu\"></div><div id=\"menu\"><div class=\"appcon appcon1\">W1</div><div class=\"appcon appcon2\">W2</div></div></div>";
				/*for (var i = 0; i < aactl.length; i++) {
					var tasbar_ = taskbar_start + lmr.sys.tbm.aactl[i];
				}*/
				$body.append(taskbar_);
				$("#menu").append(lmr.sys.tbm.aactl[0]);
				// console.log(lmr.sys.tbm.aactl.length);
				// for(i = 0; i > lmr.sys.tbm.aactl.length; i++) {
				// 	$("#menu").append(lmr.sys.tbm.aactl[i]);
				// 	console.log(i);
				// 	console.log(lmr.sys.tbm.aactl[i]);
				// }
			},

			tbms: function() { // taskbar manager start
				lmr.sys.tbm.acm(appcon1, app1, app_1);
				lmr.sys.tbm.acm(appcon2, app2, app_2);
			}
		},

		wb: function() { // window basics
			$(app).draggable({
				scroll: lmr.sys.adp.sl,
				snap: lmr.sys.adp.sp,
				snapMode: lmr.sys.adp.sm,
				snapTolerance: lmr.sys.adp.st,
				handle: lmr.sys.adp.he
			}).resizable();

			$(lmr.sys.aa.close).click(function() {
				$(this).parents(app).remove();
			});

			$(lmr.sys.aa.maximize).click(function() {
				switch($(this).parents(app).hasClass("maximized")) {
					case true:
						$(this).parents(app).css({
							"top": lmr.sys.aa.top_coord,
							"left": lmr.sys.aa.left_coord,
							"width": lmr.sys.aa.app_width,
							"height": lmr.sys.aa.app_height
						});
						lmr.sys.aa.left_coord = $(this).parents(app).css("left");
						lmr.sys.aa.top_coord = $(this).parents(app).css("top");
						lmr.sys.aa.app_height = $(this).parents(app).css("height");
						lmr.sys.aa.app_width = $(this).parents(app).css("width");
						$(this).parents(app).removeClass("maximized");
						break;
					case false:
						lmr.sys.aa.left_coord = $(this).parents(app).css("left");
						lmr.sys.aa.top_coord = $(this).parents(app).css("top");
						lmr.sys.aa.app_height = $(this).parents(app).css("height");
						lmr.sys.aa.app_width = $(this).parents(app).css("width");
						$(this).parents(app).css({
							"top": "0",
							"left": "0",
							"width": "100vw",
							"height": "100vh"
						});
						$(this).parents(app).addClass("maximized");
						break;
				}
			});

			$(lmr.sys.aa.minimize).click(function() {
				if(!$(this).parents(app).hasClass("minimized")) {
					$(this).parents(app).css({
						"display": lmr.sys.tbm.md
					});

					lmr.sys.tbm.amm(this);

					$(this).parents(app).addClass("minimized");
				}
			});
		}
	},

	usr: { // usr lemur
		aa: { // usr app anatomy
			left_coord: "0", // coordinates from left
			top_coord: "0", // coordinates from left

			app_width: "100vw", // app height
			app_height: "100vh", // app height

			head: ".head",
			close: ".close",
			maximize: ".maximize",
			minimize: ".minimize"
		},

		adp: { // usr app drag properties
			sl: false,
			sp: true,
			sm: "outer",
			st: 7,
			he: ".head"
		},

		am: { // app manager
			// TODO:0 Add new note about the variable model
			/*
				Variable model:
					app_usr_apptitle_appname => appname in string => example: app_usr_apptitle_reader = "Reader"
					app_usr_appcontitle_appname => appconname in string => example: app_usr_appcontitle_reader = "R"
			  	app_usr_fullapp_appname => content of app => example: app_usr_fullapp_reader = "blablabla"
					app_usr_fullappcon_appname => content of appcon => example: app_usr_fullappcon_reader = "blablabla"
					app_usr_appclass_appclass => app class => example: app_usr_appclass_reader = ".app_usr_appclass_reader"
					app_usr_appclass_appconclass => appcon class => example: app_usr_appclass_reader = ".app_usr_appclass_reader"
			*/
			wc: function(appclass, appname, fullapp) { // window creator
				return "<div class=\"app " + appclass + "\"><div class=\"head\"><div class=\"title\">" + appname + "</div><div class=\"buttons\"><div class=\"close\"></div><div class=\"maximize\"></div><div class=\"minimize\"></div></div></div><div class=\"content\">" + fullapp + "</div></div>"
			},

			acc: function(dotlessappconclass, appcontitle) { // appcon creator
				var rs = "<div class=\"appcon " + dotlessappconclass + "\">" + appcontitle + "</div>"// return string
				lmr.sys.tbm.aactl.push(rs);
				for(i = 0; i < lmr.sys.tbm.aactl.length; i++) {
					console.log("there is " + lmr.sys.tbm.aactl[i] + "items!");
				}
			},

			vm: function(object) { // variable manager
				for(key in object) {
					var apptitle = at;
					var appcontitle = act;

					var appname = "app_usr_apptitle_" + key;
					var appclass = ".app_usr_appclass_" + key;
					var appconclass = ".app_usr_appconclass_" + key;

					var dotlessappclass = appclass.substring(1);
					var dotlessappconclass = appconclass.substring(1);

					var fullapp = object[key];
					fullapp = lmr.usr.am.wc(dotlessappclass, apptitle, fullapp); // name

					lmr.usr.am.acc(dotlessappconclass, appcontitle);
					lmr.usr.am.sk(appname, appclass, appconclass, fullapp);
					lmr.usr.am.saa(dotlessappclass, appconclass);
				}
			},

			vl: {}, // Variable List
			sk: function(appname, appclass, appconclass, fullapp) { // set keys
				lmr.usr.am.vl[appname] = {};
				lmr.usr.am.vl[appname].appclass = appclass;
				lmr.usr.am.vl[appname].appconclass = appconclass;
				lmr.usr.am.vl[appname].fullapp = fullapp;
			},

			al: [], // app list
			acl: [], // appcon list

			saa: function(al, acl) { // Set App list and Appcon list
				lmr.usr.am.al.push(al);
				lmr.usr.am.acl.push(acl);
			},

			/*
			acn = appconclass
			acs = appclass
			fap = fullapp
			*/

			ams: function() { // app manager start
				var acn, acs, fap;
				for(key in lmr.usr.am.vl) {
					acn = lmr.usr.am.vl[key].appconclass;
					acs = lmr.usr.am.vl[key].appclass;
					fap = lmr.usr.am.vl[key].fullapp;

					lmr.usr.tbm.acm(acn, acs, fap);
				}
			}
		},

		tbm: { // taskbar manager
			uc: "#ffffff", // unminimized appcon color
			mc: "#aaaaaa", // minimized appcon color

			ud: "block", // unminimized display
			md: "none", // minimized display

			exists: function(fullapp) { // app open?
				return $(fullapp).length;
			},

			acm: function(appconclass, appclass, fullapp) { // appcon manager
				$(appconclass).click(function() {
					if(lmr.usr.tbm.exists(appclass)) {
						if($(appclass).hasClass("minimized")) {
							$(appclass).css({"display": lmr.usr.tbm.ud});
							$(appconclass).css({"background-color": lmr.usr.tbm.uc});
							$(appclass).removeClass("minimized");
						} else {
							alert("app already exists");
						}
					} else {
						$body.append(fullapp);
						lmr.usr.wb();
					}
				});
			},

			amm: function(t) { // app minimizing manager
				for(i = 0; i < lmr.usr.am.al.length; i++) { // Browse through the apps
					if($(t).parents(app).hasClass(lmr.usr.am.al[i])) { // until you find the the right one
						$(lmr.usr.am.al[i]).css({
							"background-color": lmr.usr.tbm.mc
						});
					}
				}
			},

			aactlc: function() { // add appcon to taskbar list constructor
				var taskbar_start = "<div id=\"taskbar\"><div id=\"start_menu\"></div><div id=\"menu\"><div class=\"appcon appcon1\">W1</div><div class=\"appcon appcon2\">W2</div>";
				var taskbar_end = "</div></div>";
				var taskbar_ = taskbar_start + systbm.aactl[0] + taskbar_end;
				$body.append(taskbar_);
			},

			tbms: function() { // taskbar manager start
				lmr.usr.am.ams();
			}
		},

		wb: function() { // window basics
			$(app).draggable({
				scroll: lmr.usr.adp.sl,
				snap: lmr.usr.adp.sp,
				snapMode: lmr.usr.adp.sm,
				snapTolerance: lmr.usr.adp.st,
				handle: lmr.usr.adp.he
			}).resizable();

			$(lmr.usr.aa.close).click(function() {
				$(this).parents(app).remove();
			});

			$(lmr.usr.aa.maximize).click(function() {
				switch($(this).parents(app).hasClass("maximized")) {
					case true:
						$(this).parents(app).css({
							"top": lmr.usr.aa.top_coord,
							"left": lmr.usr.aa.left_coord,
							"width": lmr.usr.aa.app_width,
							"height": lmr.usr.aa.app_height
						});
						lmr.usr.aa.left_coord = $(this).parents(app).css("left");
						lmr.usr.aa.top_coord = $(this).parents(app).css("top");
						lmr.usr.aa.app_height = $(this).parents(app).css("height");
						lmr.usr.aa.app_width = $(this).parents(app).css("width");
						$(this).parents(app).removeClass("maximized");
						break;
					case false:
						lmr.usr.aa.left_coord = $(this).parents(app).css("left");
						lmr.usr.aa.top_coord = $(this).parents(app).css("top");
						lmr.usr.aa.app_height = $(this).parents(app).css("height");
						lmr.usr.aa.app_width = $(this).parents(app).css("width");
						$(this).parents(app).css({
							"top": "0",
							"left": "0",
							"width": "100vw",
							"height": "100vh"
						});
						$(this).parents(app).addClass("maximized");
						break;
				}
			});

			$(lmr.usr.aa.minimize).click(function() {
				if(!$(this).parents(app).hasClass("minimized")) {
					$(this).parents(app).css({
						"display": lmr.usr.tbm.md
					});

					lmr.usr.tbm.amm(this);

					$(this).parents(app).addClass("minimized");
				}
			});
		}
	}
};
