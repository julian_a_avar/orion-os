# README of Orion OS Test 1.098

## Description

In version test 1.098, I finally finish creating `hipopo`, making it the first version with more than one component.

This created some additional hundred lines, and making me have to add components. Components are sections of the code that do different things, much like "modules" in `Node.js`.

## OOS

To check out the current components used click [here](components.md).  
To see my current plan, please click [here](plan.md).

Additionally, you may for now check out my latest [`hpp` model](thinking-on-hpp-management.md).
