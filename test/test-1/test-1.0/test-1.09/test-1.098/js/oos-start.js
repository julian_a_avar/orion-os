// The GNU General Public License v2.0, Copyright (c) 2015 - 2016 Copyright Julian Antonio Avar Campopiano
$(function() {
	// sys system
	oos.lmr.sys.tbm.aactlc(); // initialize taskbar
	oos.lmr.sys.tbm.tbms(); // initialize orion os
});
