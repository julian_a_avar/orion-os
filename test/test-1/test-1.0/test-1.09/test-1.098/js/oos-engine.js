// The GNU General Public License v2.0, Copyright (c) 2015 - 2016 Copyright Julian Antonio Avar Campopiano
var oos = {
	lmr: { // lemur
		sys: { // sys
			aa: { // app anatomy
				left_coord: "0", // coordinates from left
				top_coord: "0", // coordinates from left

				app_width: "100vw", // app height
				app_height: "100vh", // app height

				head: ".head",
				close: ".close",
				maximize: ".maximize",
				minimize: ".minimize"
			},

			adp: { // app drag properties
				sl: false,
				sp: true,
				sm: "outer",
				st: 7,
				hd: ".head"
			},

			am: { // app manager
				// TODO:0 Add new note about the variable model
				/*
				Variable model:
				app_sys_apptitle_appname => appname in string => example: app_sys_apptitle_reader = "Reader"
				app_sys_appcontitle_appname => appconname in string => example: app_sys_appcontitle_reader = "R"
				app_sys_fullapp_appname => content of app => example: app_sys_fullapp_reader = "blablabla"
				app_sys_fullappcon_appname => content of appcon => example: app_sys_fullappcon_reader = "blablabla"
				app_sys_appclass_appclass => app class => example: app_sys_appclass_reader = ".app_sys_appclass_reader"
				app_sys_appclass_appconclass => appcon class => example: app_sys_appclass_reader = ".app_sys_appclass_reader"
				*/

				exists: function(fullapp) { // app open?
					return $(fullapp).length;
				},

				wc: function(appclass, appname, fullapp) { // window creator
					return "<div class=\"app " + appclass + "\"><div class=\"head\"><div class=\"title\">" + appname + "</div><div class=\"buttons\"><div class=\"close\"></div><div class=\"maximize\"></div><div class=\"minimize\"></div></div></div><div class=\"content\">" + fullapp + "</div></div>"
				},

				acc: function(dotlessappconclass, appcontitle) { // appcon creator
					// var rs = "<div class=\"appcon " + dotlessappconclass + "\">" + appcontitle + "</div>"// return string
					// oos.lmr.sys.tbm.aactl.push(rs);
					// for(i = 0; i < oos.lmr.sys.tbm.aactl.length; i++) {
					// console.log("there is " + oos.lmr.sys.tbm.aactl[i] + "items!");
					// }
				},

				vm: function() { // variable manager
					for(key in ua) {
						var apptitle = ua[key].at;
						var appcontitle = ua[key].act;

						var appname = "app_sys_appname_" + key;
						var appclass = ".app_sys_appclass_" + key;
						var appconclass = ".app_sys_appconclass_" + key;

						var dotlessappclass = appclass.substring(1);
						var dotlessappconclass = appconclass.substring(1);

						var fullapp = ua[key].content;
						fullapp = oos.lmr.sys.am.wc(dotlessappclass, apptitle, fullapp); // name

						// TODO: WTF is going on right here?
						oos.lmr.sys.am.acc(dotlessappconclass, appcontitle);
						oos.lmr.sys.am.sk(appname, appclass, appconclass, fullapp);
						oos.lmr.sys.am.saa(dotlessappclass, appconclass);
						oos.lmr.sys.am.duak();
					}
				},

				vl: {}, // Variable List
				sk: function(appname, appclass, appconclass, fullapp) { // set keys
					oos.lmr.sys.am.vl[appname] = {};
					oos.lmr.sys.am.vl[appname].appclass = appclass;
					oos.lmr.sys.am.vl[appname].appconclass = appconclass;
					oos.lmr.sys.am.vl[appname].fullapp = fullapp;
				},

				al: [], // app list
				acl: [], // appcon list

				saa: function(al, acl) { // Set App list and Appcon list
					oos.lmr.sys.am.al.push(al);
					oos.lmr.sys.am.acl.push(acl);
				},

				duak: function() { // delete ua keys
					var k;
					for(k in ua) {
						delete ua[k];
					};
				},

				/*
				acn = appconclass
				acs = appclass
				fap = fullapp
				*/

				ams: function() { // app manager start
					var acn, acs, fap;
					for(key in oos.lmr.sys.am.vl) {
						acn = oos.lmr.sys.am.vl[key].appconclass;
						acs = oos.lmr.sys.am.vl[key].appclass;
						fap = oos.lmr.sys.am.vl[key].fullapp;

						oos.lmr.sys.tbm.acm(acn, acs, fap);
					}
				}
			},

			tbm: { // taskbar manager
				uc: "#ffffff", // unminimized appcon color
				mc: "#aaaaaa", // minimized appcon color

				ud: "block", // unminimized display
				md: "none", // minimized display

				acm: function(appconclass, appclass, fullapp) { // appcon manager
					$(appconclass).click(function() {
						if(oos.lmr.sys.am.exists(appclass)) {
							if($(appclass).hasClass("minimized")) {
								$(appclass).css({"display": oos.lmr.sys.tbm.ud});
								$(appconclass).css({"background-color": oos.lmr.sys.tbm.uc});
								$(appclass).removeClass("minimized");
							} else {
								alert("app already exists");
							}
						} else {
							$body.append(fullapp);
							oos.lmr.sys.wb.head();
						}
					});
				},

				amm: function(t) { // app minimizing manager
					for(i = 0; i < oos.lmr.sys.am.al.length; i++) { // Browse through the apps
						if($(t).parents(app).hasClass(oos.lmr.sys.am.al[i])) { // until you find the the right one
							$(oos.lmr.sys.am.acl[i]).css({
								"background-color": oos.lmr.sys.tbm.mc
							});
						}
					}
				},

				aactl: ["<div class=\"appcon app_sys_appconclass_app1\">A1</div>", "<div class=\"appcon app_sys_appconclass_app2\">A2</div>", "<div class=\"appcon app_sys_appconclass_reader\">R</div>"], // add appcon to taskbar list
				// aactl: [], // add appcon to taskbar list
				aactlc: function() { // add appcon to taskbar list constructor
					var taskbar_ = "<div id=\"taskbar\"><div id=\"start_menu\"></div><div id=\"menu\"></div></div>";
					$body.append(taskbar_);
					console.log("There is a total of " + oos.lmr.sys.tbm.aactl.length + " apps");
					for(var i = 0; i < oos.lmr.sys.tbm.aactl.length; i++) {
						$("#menu").append(oos.lmr.sys.tbm.aactl[i]);
					}
				},

				tbms: function() { // taskbar manager start
					oos.lmr.sys.am.ams();
				}
			},

			wb: { // window basics
				zindex: 0,
				head: function() {
					$(app).draggable({
						scroll: oos.lmr.sys.adp.sl,
						snap: oos.lmr.sys.adp.sp,
						snapMode: oos.lmr.sys.adp.sm,
						snapTolerance: oos.lmr.sys.adp.st,
						handle: oos.lmr.sys.adp.he
					}).resizable();

					$(app).mousedown(function() {
						$(this).css({
							"z-index": oos.lmr.sys.wb.zindex
						});
						// oos.lmr.sys.wb.zindex = oos.lmr.sys.wb.zindex + 1;
						oos.lmr.sys.wb.zindex++;
					});

					$(oos.lmr.sys.aa.close).click(function() {
						$(this).parents(app).remove();
					});

					$(oos.lmr.sys.aa.maximize).click(function() {
						switch($(this).parents(app).hasClass("maximized")) {
							case true:
								$(this).parents(app).css({
									"top": oos.lmr.sys.aa.top_coord,
									"left": oos.lmr.sys.aa.left_coord,
									"width": oos.lmr.sys.aa.app_width,
									"height": oos.lmr.sys.aa.app_height
								});
								oos.lmr.sys.aa.left_coord = $(this).parents(app).css("left");
								oos.lmr.sys.aa.top_coord = $(this).parents(app).css("top");
								oos.lmr.sys.aa.app_height = $(this).parents(app).css("height");
								oos.lmr.sys.aa.app_width = $(this).parents(app).css("width");
								$(this).parents(app).removeClass("maximized");
								break;
							case false:
								oos.lmr.sys.aa.left_coord = $(this).parents(app).css("left");
								oos.lmr.sys.aa.top_coord = $(this).parents(app).css("top");
								oos.lmr.sys.aa.app_height = $(this).parents(app).css("height");
								oos.lmr.sys.aa.app_width = $(this).parents(app).css("width");
								$(this).parents(app).css({
									"top": "0",
									"left": "0",
									"width": "100vw",
									"height": "100vh"
								});
								$(this).parents(app).addClass("maximized");
								break;
						}
					});

					$(oos.lmr.sys.aa.minimize).click(function() {
						if(!$(this).parents(app).hasClass("minimized")) {
							$(this).parents(app).css({
								"display": oos.lmr.sys.tbm.md
							});

							oos.lmr.sys.tbm.amm(this);

							$(this).parents(app).addClass("minimized");
						}
					});
				}
			}
		}
	},

	hpp: {
		mgr: { // manager
			sys: {

			},
			usr: {

			}
		},
		tmy: { // tommy(as in hipopo tommy)
			o: {
				root: oos.hpp.tmy.o,
				prp: {
					typeprop: "::unvrs>>",
					share: "::o>>"
				},
				ctt: {
					boot: {
						prp: {
							typeprop: "::clstr>>",
							share: "::o>>"
						},
						ctt: {}
					},
					lib: {
						prp: {
							typeprop: "::clstr>>",
							share: "::o>>"
						},
						ctt: {
						}
					},
					tmp: {
						prp: {
							typeprop: "::clstr>>",
							share: "::o>>"
						},
						ctt: {
						}
					},
					ou: {
						root: oos.hpp.tmy.o.ctt.ou,
						prp: {
							typeprop: "::glx>>",
							share: "::ou>>"
						},
						ctt: {
							desktop: {
								prp: {
									typeprop: "::clstr>>",
									share: "::ou>>"
								},
								ctt: {}
							},
							apps: {
								prp: {
									typeprop: "::clstr>>",
									share: "::ou>>"
								},
								ctt: {}
							}
						}
					},
					nausr: {
						prp: {
							typeprop: "::glx>>",
							share: "::all>>"
						},
						ctt: {
							julian: {
								prp: {
									typeprop: "::clstr>>",
									share: "::nausr>:julian>>"
								},
								ctt: {
									desktop: {
										prp: {
											typeprop: "::clstr>>",
											share: "::nausr>:julian>>"
										},
										ctt: {
											readerapp: {
												prp: {
													typeprop: "::str>>",
													share: "::nausr>:julian>>"
												},
												ctt: {
													main: "ua.reader={};ua.reader.content=\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>\";ua.reader.at=\"Reader\";ua.reader.act=\"R\";lmr.sys.am.vm();"
												}
											}
										}
									},
									apps: {
										prp: {
											typeprop: "::clstr>:app>>",
											share: "::nausr>:julian>>"
										},
										ctt: {
											app1: {
												typeprop: "::clstr>:app>:main>>",
												share: "::nausr>:julian>>"
											},
											ctt: {
												head: {
													prp: {
														typeprop: "::clstr>:app>:head>:nbl>>",
														share: "::nausr>:julian>>"
													},
													ctt: {
														name: "app1",
														apptitle: "App 1",
														appcontitle: "A1"
													}
												},
												body: {
													prp: {
														typeprop: "::clstr>:app>:head>:str>>",
														share: "::nausr>:julian>>"
													},
													ctt: {
														main: "<p>1</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>"
													}
												}
											}
										}
									}
								}
							}
						}
					},
					apps: { // global apps
						prp: {
							typeprop: "::clstr>:app>>",
							share: "::all>>"
						},
						ctt: {
							app2: {
								prp: {
									typeprop: "app",
									share: "::all>>"
								},
								ctt: {
									head: {
										prp: {
											typeprop: "::clstr>:app>:head>:nbl>>",
											share: "::all>>"
										},
										ctt: {
											name: "app2",
											apptitle: "App 2",
											appcontitle: "A2"
										}
									},
									body: {
										prp: {
											typeprop: "::clstr>:app>:body>:str>>",
											share: "::all>>"
										},
										ctt: {
											main: "<p>2</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>"
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
};

//
// console.log(oos.hpp.tmy.o);
// var tmy = CircularJSON.stringify(oos.hpp.tmy);
// console.log(tmy);
