// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano.

/*
ua => user apps | is a reusable object that continuisly gets filled, emptied, and refilled.

*/

/*
ua is a list containing the content of the application. To add other programs, you need to select ua.af (additional features).
*/

ua.reader = {};
ua.reader.content = "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>";
ua.reader.at = "Reader"; // apptitle
ua.reader.act = "R"; // appcontitle
lmr.sys.am.vm(ua);

ua.app1 = {};
ua.app1.content = "1<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>";
ua.app1.at = "App 1";
ua.app1.act = "A1";
lmr.sys.am.vm(ua);

ua.app2 = {};
ua.app2.content = "2<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>";
ua.app2.at = "App 2";
ua.app2.act ="A2";
lmr.sys.am.vm(ua);
