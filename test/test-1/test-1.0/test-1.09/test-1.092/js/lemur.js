// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano
var lmr = {
	sys: { // sys lemur
		aa: { // sys app anatomy
			left_coord: "0", // coordinates from left
			top_coord: "0", // coordinates from left

			app_width: "100vw", // app height
			app_height: "100vh", // app height

			head: ".head",
			close: ".close",
			maximize: ".maximize",
			minimize: ".minimize"
		},

		adp: { // sys app drag properties
			sl: false,
			sp: true,
			sm: "outer",
			st: 7,
			he: ".head"
		},

		am: { // app manager
			// TODO:0 Add new note about the variable model
			/*
				Variable model:
					app_sys_apptitle_appname => appname in string => example: app_sys_apptitle_reader = "Reader"
					app_sys_appcontitle_appname => appconname in string => example: app_sys_appcontitle_reader = "R"
			  	app_sys_fullapp_appname => content of app => example: app_sys_fullapp_reader = "blablabla"
					app_sys_fullappcon_appname => content of appcon => example: app_sys_fullappcon_reader = "blablabla"
					app_sys_appclass_appclass => app class => example: app_sys_appclass_reader = ".app_sys_appclass_reader"
					app_sys_appclass_appconclass => appcon class => example: app_sys_appclass_reader = ".app_sys_appclass_reader"
			*/
			wc: function(appclass, appname, fullapp) { // window creator
				return "<div class=\"app " + appclass + "\"><div class=\"head\"><div class=\"title\">" + appname + "</div><div class=\"buttons\"><div class=\"close\"></div><div class=\"maximize\"></div><div class=\"minimize\"></div></div></div><div class=\"content\">" + fullapp + "</div></div>"
			},

			acc: function(dotlessappconclass, appcontitle) { // appcon creator
				// var rs = "<div class=\"appcon " + dotlessappconclass + "\">" + appcontitle + "</div>"// return string
				// lmr.sys.tbm.aactl.push(rs);
				// for(i = 0; i < lmr.sys.tbm.aactl.length; i++) {
					// console.log("there is " + lmr.sys.tbm.aactl[i] + "items!");
				// }
			},

			vm: function(object) { // variable manager
				for(key in object) {
					var apptitle = object[key].at;
					var appcontitle = object[key].act;

					var appname = "app_sys_apptitle_" + key;
					var appclass = ".app_sys_appclass_" + key;
					var appconclass = ".app_sys_appconclass_" + key;

					var dotlessappclass = appclass.substring(1);
					var dotlessappconclass = appconclass.substring(1);

					var fullapp = object[key].content;
					fullapp = lmr.sys.am.wc(dotlessappclass, apptitle, fullapp); // name

					// TODO: WTF is going on right here?
					lmr.sys.am.acc(dotlessappconclass, appcontitle);
					lmr.sys.am.sk(appname, appclass, appconclass, fullapp);
					lmr.sys.am.saa(dotlessappclass, appconclass);
					lmr.sys.am.duak();
				}
			},

			vl: {}, // Variable List
			sk: function(appname, appclass, appconclass, fullapp) { // set keys
				lmr.sys.am.vl[appname] = {};
				lmr.sys.am.vl[appname].appclass = appclass;
				lmr.sys.am.vl[appname].appconclass = appconclass;
				lmr.sys.am.vl[appname].fullapp = fullapp;
			},

			al: [], // app list
			acl: [], // appcon list

			saa: function(al, acl) { // Set App list and Appcon list
				lmr.sys.am.al.push(al);
				lmr.sys.am.acl.push(acl);
			},

			duak: function() { // delete ua keys
				var k;
				for(k in ua) {
					delete ua[k];
				};
			},

			/*
			acn = appconclass
			acs = appclass
			fap = fullapp
			*/

			ams: function() { // app manager start
				var acn, acs, fap;
				for(key in lmr.sys.am.vl) {
					acn = lmr.sys.am.vl[key].appconclass;
					acs = lmr.sys.am.vl[key].appclass;
					fap = lmr.sys.am.vl[key].fullapp;

					lmr.sys.tbm.acm(acn, acs, fap);
				}
			}
		},

		tbm: { // taskbar manager
			uc: "#ffffff", // unminimized appcon color
			mc: "#aaaaaa", // minimized appcon color

			ud: "block", // unminimized display
			md: "none", // minimized display

			exists: function(fullapp) { // app open?
				return $(fullapp).length;
			},

			acm: function(appconclass, appclass, fullapp) { // appcon manager
				$(appconclass).click(function() {
					if(lmr.sys.tbm.exists(appclass)) {
						if($(appclass).hasClass("minimized")) {
							$(appclass).css({"display": lmr.sys.tbm.ud});
							$(appconclass).css({"background-color": lmr.sys.tbm.uc});
							$(appclass).removeClass("minimized");
						} else {
							alert("app already exists");
						}
					} else {
						$body.append(fullapp);
						lmr.sys.wb();
					}
				});
			},

			amm: function(t) { // app minimizing manager
				for(i = 0; i < lmr.sys.am.al.length; i++) { // Browse through the apps
					if($(t).parents(app).hasClass(lmr.sys.am.al[i])) { // until you find the the right one
						$(lmr.sys.am.acl[i]).css({
							"background-color": lmr.sys.tbm.mc
						});
					}
				}
			},

			aactl: ["<div class=\"appcon app_sys_appconclass_app1\">A1</div>", "<div class=\"appcon app_sys_appconclass_app2\">A2</div>", "<div class=\"appcon app_sys_appconclass_reader\">R</div>"], // add appcon to taskbar list
			// aactl: [], // add appcon to taskbar list
			aactlc: function() { // add appcon to taskbar list constructor
				var taskbar_ = "<div id=\"taskbar\"><div id=\"start_menu\"></div><div id=\"menu\"></div></div>";
				$body.append(taskbar_);
				console.log("There is a total of " + lmr.sys.tbm.aactl.length + " apps");
				for(var i = 0; i < lmr.sys.tbm.aactl.length; i++) {
					$("#menu").append(lmr.sys.tbm.aactl[i]);
				}
			},

			tbms: function() { // taskbar manager start
				lmr.sys.am.ams();
			}
		},

		wb: function() { // window basics
			$(app).draggable({
				scroll: lmr.sys.adp.sl,
				snap: lmr.sys.adp.sp,
				snapMode: lmr.sys.adp.sm,
				snapTolerance: lmr.sys.adp.st,
				handle: lmr.sys.adp.he
			}).resizable();

			$(lmr.sys.aa.close).click(function() {
				$(this).parents(app).remove();
			});

			$(lmr.sys.aa.maximize).click(function() {
				switch($(this).parents(app).hasClass("maximized")) {
					case true:
						$(this).parents(app).css({
							"top": lmr.sys.aa.top_coord,
							"left": lmr.sys.aa.left_coord,
							"width": lmr.sys.aa.app_width,
							"height": lmr.sys.aa.app_height
						});
						lmr.sys.aa.left_coord = $(this).parents(app).css("left");
						lmr.sys.aa.top_coord = $(this).parents(app).css("top");
						lmr.sys.aa.app_height = $(this).parents(app).css("height");
						lmr.sys.aa.app_width = $(this).parents(app).css("width");
						$(this).parents(app).removeClass("maximized");
						break;
					case false:
						lmr.sys.aa.left_coord = $(this).parents(app).css("left");
						lmr.sys.aa.top_coord = $(this).parents(app).css("top");
						lmr.sys.aa.app_height = $(this).parents(app).css("height");
						lmr.sys.aa.app_width = $(this).parents(app).css("width");
						$(this).parents(app).css({
							"top": "0",
							"left": "0",
							"width": "100vw",
							"height": "100vh"
						});
						$(this).parents(app).addClass("maximized");
						break;
				}
			});

			$(lmr.sys.aa.minimize).click(function() {
				if(!$(this).parents(app).hasClass("minimized")) {
					$(this).parents(app).css({
						"display": lmr.sys.tbm.md
					});

					lmr.sys.tbm.amm(this);

					$(this).parents(app).addClass("minimized");
				}
			});
		}
	}
};
