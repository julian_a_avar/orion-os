# TEST 1.092 plan

## Description

The plan for every version will be found inside it's respective folder, and will be written previous to it's development.

Although the History file contains information about what are the goals, I've found that there is the need for more technical procedure explanation.

The History file will contain the language information. The History file will be much like a log. Inside it, you will find what I think of what I am doing, what I can do to progress, comments on my previous failures, as well as successes. But it will not contain technical information, for that is not the purpose, the purpose is to have a place where I can find a History book and see what the time and place was, and "WTF was I thinking!!!???".

The plan file will contain the plan for the entire app until now. It will define and explain every function and file. This way, I can understand what I was planning after I am lost in many lines of code.

And as I said, the plan file will contain ALL of the PROGRESS that WAS achieved and WILL be achieved. The plan file, as well as the history file, is not definite. If I decide to change what I am doing, I must report that to the plan file during progress, and to the history file in EDevde, once finished.

## Index

1. [GUI and behavior](#gui)
  - [Past goals - Already accomplished](#gui-past)
  - [Current goals - Goals for this version](#gui-current)
  - [Short-term goals](#gui-short)
  - [Long-term goals](#gui-long)
2. [Technical](#tech)
  - [Past goals - Already accomplished](#tech-past)
    1. [The load](#tech-past-first)
    2. [Summary of files](#tech-past-second)
    3. [Detailed explanation of files](#tech-past-third)
    4. [Application walk-through](#tech-past-fourth)
  - [Current goals - Goals for this version](#tech-current)
  - [Short-term goals](#tech-short)
  - [Long-term goals](#tech-long)
3. [Bugs](#bugs)

## Plan

### GUI and behavior <a id="gui"></a>

#### Past goals - Already accomplished <a id="gui-past"></a>

Orion OS is be an independent app powered by `Node.js`, `Javascript`, `HTML5`, `CSS3`, `JSON`. Orion will be able to run both online and offline.

Orion OS has a taskbar, which contains all possible apps, in the form of tiny icons called appcons. When an appcon is clicked, it's respected `div` element, illustrated as a window, is appended into the `body`, or it's `display` is set back to `block` as explained in the following paragraph. A window may be created only once.

All windows(or apps), have a head and a body. The head displays it's title and the close, minimize, and maximize/unmaximze buttons. It's body contains it's contents. The close button removes the `div` from the `body`. The maximize button sets the window to fullscreen, and once it is on fullscreen, it serves as unmaximize, which reverts it's size to what it was, as well as it's position. The minimize button set's the `display` to `none`(so like invisible), and the only way to revert this effect is by clicking it's respective appcon.

#### Current goals - Goals for this version <a id="gui-current"></a>
When unchecked, it means that this feature is in progress.

 - [ ] None.

#### Short-term goals <a id="gui-short"></a>
When checked, it means that this feature is going to be a "Current goal" in the next version.

 - [ ] Change the z-index of windows when clicked on.

 - [ ] Instead of having your appcons lined up in the taskbar taking up space, add a menu on the left, that when clicked bring up a box with all the possible apps. The taskbar will only contain the open apps. Apps can be opened as many times as specified. Add app shortcuts in the desktop.

#### Long-term goals <a id="gui-long"></a>
When checked, it means that this feature is going to be a "Short-term goal" in the next version.

 - [ ] Be able to create, edit and delete files, with a command prompt. Have files on your desktop. Create a file browser system using commands and later a GUI. Be able to create, edit, and delete files using a GUI.

 - [ ] Create a browser inside Orion OS.

### Technical <a id="tech"></a>

#### Past goals - already accomplished <a id="tech-past"></a>

**First**, the load: <a id="tech-past-first"></a>

1. `Jquery & Jquery UI`
<br><br>
2. `consts.js`
<br><br>
3. `lemur.js`
<br><br>
4. `sys_apps.js`
5. `reader.js`
<br><br>
6. `orion-os.js`

<br><br>

**Second**, explain what each file does: <a id="tech-past-second"></a>

1. Jquery and Jquery UI are both exterior libraries which make the development of the application much faster.
<br><br>
2. This file contains a series of constant which are used through Orion OS.
<br><br>
3. This file contains all of the logic necessary in order to launch, create, administrate and edit everything inside orion os.
<br><br>
4. These are the apps defined by the system.
5. This is an app defined by user.
<br><br>
6. This file starts up the entire application

<br><br>

**Third**, explain EACH INDIVIDUAL FILE: <a id="tech-past-third"></a>

##### `Jquery & Jquery UI`

These are minified versions of Jquery, that provide functionality like easier to work with the DOM. These are exterior and I am not to modify them.

##### `consts.js`

This file contains all of the constants required by Orion OS.

##### `lemur.js`

This is perhaps the biggest file for now, and the one which contains all of the methods, objects, and arrays required to use Orion OS.

This file contains only one variable called `lmr`, which is an object

The entire object tree will be explained right here:

- `lmr`
  2. `sys` - Object. `sys` is the part of lemur that takes over the user files and user applications.
    1. `aa`
      - Object. This object contains the height and width of all windows.
      - TODO: [This object contains the height and width of all apps, but not individually, therefore, things could become troublesome if one window was resized.](#TODO)
    2. `adp`
      - Object. App drag properties.
    3. `am` - Object. The user app manager is the responsible for creating and organizing new apps.
      1. `wc()`
          - Function. Window creator creates a fully functional window out of content.
      2. `acc()`
          - Function. Appcon creator is a creator of fully functional appcons out of content.
      3. `vm()`
          - Function. The variable manager grabs data from an app and creates an enviroment for the app to live out of the variables that it creates to send to other functions.
      4. `vl`
          - Object. `vl`(variable list) contains a list of apps which can be opened.
      5. `sk()`
          - Function. The "set keys" function adds data given by `vm` and fills it in `vl`.
      6. `al`
          - Array. List of app classes which can be opened. App list.
      7. `acl`
          - Array. List of app classes which can be opened. Appcon list.
      8. `saa()`
          - Function. The "Set App list and Appcon list" function adds data given by `vm` and fills it in `al` & `acl`.
      9. `ams()`
          - Function. This function starts up `lmr.sys`'s app manager.
    4. `tbm()`
      1. `uc`
          - String. Color of appcon when app is unminimized.
      2. `mc`
          - String. Color of appcon when app is minimized.
      3. `ud`
          - String. Display type of window when app is unminimized.
      4. `md`
          - String. Display type of window when app is minimized.
      5. `exists()`
          - Function. Checks weather an object with the given class is existent in the DOM.
      6. `acm()`
          - Function. Creates, minimizes, and unminimizes apps correspond based upon the actions of taken in the taskbar.
      7. `amm()`
          - Function. This is a minimizing manager which can determine what to do when an app is minimized.
      8. `aactl`
          - Array. The add appcon to taskbar list is an array which contains the content to all of the appcons.
      9. `aactlc()`
          - Function. The add appcon to taskbar list constructor, it uses `aactl` to construct appcons.
      10. `tbms()`
          - Function. This function starts up `lmr.sys`'s taskbar manager.
    5. `wb()`

<br><br>

**Fourth**, walk through the steps that initialize the application: <a id="tech-past-fourth"></a>

Here I will explain step by step how the application loads and responds to user input.

1. start
  1. makes sure I can use JQuery/UI in all files
  2. load all constants!
  3. load `lemur.js`
  4. run `lmr.sys.tbm.aactlc()` as stated in `taskbar.js`
    - generate all the taskbar and appcons
  5. run `lmr.sys.tbm.tbms()`
    1. run `lmr.sys.tbm.acm()` with all the information required to start the system applications
    2. get ready for a user to click on an appcon
  6. make sure I can use all of the methods and keys inside `lemur.js`
  7. Load the system apps
  8. load `reader`, the sys app
    - run `sysam.vm()`
      1. run `wc()` and put it in `fullapp`
      2. run `acc()`
        - set the apps appcon
      3. run `sk()`
        - get `vl` the information needed to properly store the app
      4. run `saa()`
        - get `al` and `acl` the proper information
  9. run `lmr.sys.tbm.tbms()` in `orion-os.js`
    - run `lmr.sys.am.ams()`
      1. run `lmr.sys.tbm.acm()` and give it the information provided in 7.3
      2. get ready for a user to click on an appcon

Inputs in 5.2 and 8.2

In either one, the process will much like this(taking the example of sys):

  10. user clicked on appcon of application
    - if the application does not exist
      1. append the application to `body`
      2. run `lmr.sys.wb()`
        1. make the window draggable
        2. get ready for a user to click on `.close`
        3. get ready for a user to click on `.maximize`
        4. get ready for a user to click on `.minimize`

If user clicks `.close`

  11. user clicked `.close`
    - remove application

If user clicked `.maximize`

  12. user clicked `.maximize`
    1. if window has class `.maximized`
      1. revert the state of the application to the position and size that was saved before adding `.maximized`
      2. remove `.maximized`
    2. else
      1. save its current position and size
      2. set the application to cover the who screen
      3. add the `.maximized`

If user clicks `.minimize`

  13. user clicked `.minimize`
    1. if application does not have the `.minimized` class
      1. hide the application visualy
      2. run `amm()`
        - color it's corresponding appcon
      3. add the class `.minimized`

If a window is minimized

  14. user clicked on appcon from application that is loaded
    - if the application exists
      1. if the application has `.minimized`
        1. make it visible again
        2. uncolor the appcon
        3. remove `.minimized` class
      2. else
        - alert the user that the application already exists and therefore cannot be opened again

#### Current goals - Goals for this version <a id="tech-current"></a>
When unchecked, it means that this feature is in progress.

 - [x] Change the sys system to replicate the state of the sys system.

#### Short-term goals <a id="tech-short"></a>
When checked, it means that this feature is going to be a "Current goal" in the next version.

 - [ ] Use hipopo.

 - [x] Change the z-index of windows when clicked on.

#### Long-term goals <a id="tech-long"></a>
When checked, it means that this feature is going to be a "Short-term goal" in the next version.

 - [ ] Be able to create, edit and delete files, with a command prompt. Have files on your desktop. Create a file browser system using commands and later a GUI. Be able to create, edit, and delete files using a GUI.

 - [ ] Create a browser inside Orion OS.

### Bugs - GUI, behaviour, and technical <a id="bugs"></a>
Checked boxes are bugs being solved right now. When no checkbox, bug was fixed.

 - Little problem when coloring appcons.
 - [ ] If I open a new app and there is already one open, the already open's maximize function wont work(probably something wrong with `window basics`). Unexpected behavior.
 <!-- - [ ] appcon for reader is not being created -->

## End

To reference data inside the plan, use the following notation `###.####.####.paragraph_number/list_number`.

example: bugs.1, technical.past.sys_window_basics.7.3
