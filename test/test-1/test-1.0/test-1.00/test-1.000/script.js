$(function() {
	var window = ".window";
	var head = ".head";
	var close = ".close";
	var maximize = ".maximize";
	var minimize = ".minimize";
	var app = ".app";
	var w1 = $(".w1").clone();
	var w2 = $(".w2").clone();

	app_basics();

	$(".app1").click(function() {
		if($(".w1").length) {
			if($(".w1").css("display") == "none") {
				$(".w1").css({
					"display": "block"
				});
				$(".app1").css({
					"background-color": "#ffffff"
				});
			} else {
				alert("app already exists");
			}
		} else {
			alert("making new app");
			$("main").append(w1);
			app_basics();
		}
	});

	$(".app2").click(function() {
		if($(".w2").length) {
			if($(".w2").css("display") == "none") {
				$(".w2").css({
					"display": "block"
				});
				$(".app2").css({
					"background-color": "#ffffff"
				});
			} else {
				alert("app already exists");
			}
		} else {
			alert("making new app");
			$("main").append(w2);
			app_basics();
		}
	});

	function app_basics() {
		$(window).draggable({
			scroll: false,
			snap: true,
			snapMode: "outer",
			snapTolerance: 7,
			// zIndex: 1,
			handle: head
		}).resizable();

		$(close).click(function() {
			$(this).parents(".window").remove();
			alert("Closing window");
		});

		$(maximize).click(function() {
			if($(this).parents(".window").css("width") == "500px" && $(this).parents(".window").css("height") == "200px") {
				$(this).parents(".window").css({
					"top": "0",
					"left": "0",
					"width": "100vw",
					"height": "100vh"
				});
			} else {
				$(this).parents(".window").css({
					"top": "0",
					"left": "0",
					"width": "500px",
					"height": "200px"
				});
			}
		});

		$(minimize).click(function() {
			var element = $(this).parents(".window")[0];

			$(this).parents(".window").css({
				"display": "none"
			});
			if(element == $(".w1")[0]) {
				$(".app1").css({
					"background-color": "#aaaaaa"
				});
			} else if(element == $(".w2")[0]) {
				$(".app2").css({
					"background-color": "#aaaaaa"
				});
			}
		});
	}
});