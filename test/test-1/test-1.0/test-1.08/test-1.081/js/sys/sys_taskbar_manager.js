var i;
var $body = $("body");

var app1 = ".app1";
var app2 = ".app2";

var appcon1 = ".appcon1";
var appcon2 = ".appcon2";

var al = ["app1", "app2"]; // app list
var acl = [appcon1, appcon2] // appcon list

var appreader = ".appreader";
var appconreader = ".appconreader";
al.push("appreader");
acl.push(appconreader);

var sys_tbm = { // taskbar manager
	unmnmzd_clr: "#ffffff", // unminimized appcon color
	mnmzd_clr: "#aaaaaa", // minimized appcon color

	unmnmzd_dsply: "block", // unminimized display
	mnmzd_dsply: "none", // minimized display

	exists: function(app_name) { // app open?
		return $(app_name).length;
	},

	acm: function(appcon_name, app_name, app_content_name) { // appcon manager
		$(appcon_name).click(function() {
			if(sys_tbm.exists(app_name)) {
				if($(app_name).hasClass("minimized")) {
					$(app_name).css({"display":sys_tbm.unmnmzd_dsply});
					$(appcon_name).css({"background-color":sys_tbm.unmnmzd_clr});
					$(app_name).removeClass("minimized");
				} else {
					alert("app already exists");
				}
			} else {
				$body.append(app_content_name);
				window_basics();
			}
		});
	},

	amm: function(element_this) { // app minimizing manager
		for(i = 0; i < al.length; i++) {
			if($(element_this).parents(app).hasClass(al[i])) {
				$(acl[i]).css({
					"background-color": sys_tbm.mnmzd_clr
				});
			}
		}
	}

};

function acm_start() { // appcon manager starter
	sys_tbm.acm(appcon1, app1, app_1);
	sys_tbm.acm(appcon2, app2, app_2);

	sys_tbm.acm(appconreader, appreader, app_reader);
}
