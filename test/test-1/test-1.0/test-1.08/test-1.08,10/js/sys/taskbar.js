// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano
$(function() {
	var $body = $("body");

	var aactl = ["<div class=\"appcon app_usr_appconclass_reader\">R</div>"] // add appcon to taskbar list

	var taskbar_ = "<div id=\"taskbar\"><div id=\"start_menu\"></div><div id=\"menu\"><div class=\"appcon appcon1\">W1</div><div class=\"appcon appcon2\">W2</div>" + aactl[0] + "</div></div>";

	$body.append(taskbar_);
});
