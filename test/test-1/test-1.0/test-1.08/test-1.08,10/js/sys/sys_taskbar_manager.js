// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano
var i;
var $body = $("body");

var app1=".app1",app2=".app2";
var appcon1=".appcon1",appcon2=".appcon2";

var al=["app1","app2"]; // app list
var acl=[appcon1,appcon2]; // appcon list

var systbm = { // taskbar manager
	unmnmzd_clr: "#ffffff", // unminimized appcon color
	mnmzd_clr: "#aaaaaa", // minimized appcon color

	unmnmzd_dsply: "block", // unminimized display
	mnmzd_dsply: "none", // minimized display

	exists: function(app_name) { // app open?
		return $(app_name).length;
	},

	acm: function(appcon_name, app_name, app_content_name) { // appcon manager
		$(appcon_name).click(function() {
			if(systbm.exists(app_name)) {
				if($(app_name).hasClass("minimized")) {
					$(app_name).css({"display":systbm.unmnmzd_dsply});
					$(appcon_name).css({"background-color":systbm.unmnmzd_clr});
					$(app_name).removeClass("minimized");
				} else {
					alert("app already exists");
				}
			} else {
				$body.append(app_content_name);
				sys_window_basics();
			}
		});
	},

	amm: function(element_this) { // app minimizing manager
		for(i = 0; i < al.length; i++) {
			if($(element_this).parents(app).hasClass(al[i])) {
				$(acl[i]).css({
					"background-color": systbm.mnmzd_clr
				});
			}
		}
	},

	sysas: function() {
		systbm.acm(appcon1, app1, app_1);
		systbm.acm(appcon2, app2, app_2);
	}
};
