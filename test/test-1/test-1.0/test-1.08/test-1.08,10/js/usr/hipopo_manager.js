var hpp = { // hipopo
	hipopo: function(location) {

	},

	exists: function(obj, n1, n2, n3, n4, n5) { // function capable of checking the existance of a nested key
  	var args = Array.prototype.slice.call(arguments, 1);
  	for(var i = 0; i < args.length; i++) {
    	if(!obj || !obj.hasOwnProperty(args[i])) {
      	return false;
    	}
    	obj = obj[args[i]];
  	}
  	return true;
	},

	action: function(appcluster, bodytypeprop, name, apptitle, appcontitle, content) {
		usr_hipopo = JSON.parse(usr_hipopo); // to object, since it is a string

		usr_hipopo.usr.content.apps.content[appcluster] = {};
		usr_hipopo.usr.content.apps.content[appcluster].properties = {}; // <= ppt
		usr_hipopo.usr.content.apps.content[appcluster].content = {}; // <= ctt

		usr_hipopo.usr.content.apps.content[appcluster].properties.typeprop = "cluster";

		usr_hipopo.usr.content.apps.content[appcluster].content.head = {};
		usr_hipopo.usr.content.apps.content[appcluster].content.body = {};

		usr_hipopo.usr.content.apps.content[appcluster].content.head.properties = {};
		usr_hipopo.usr.content.apps.content[appcluster].content.head.properties.typeprop = "app-head&nebula";

		usr_hipopo.usr.content.apps.content[appcluster].content.body.properties = {};
		usr_hipopo.usr.content.apps.content[appcluster].content.body.properties.typeprop = bodytypeprop;

		usr_hipopo.usr.content.apps.content[appcluster].content.head.content = {};
		usr_hipopo.usr.content.apps.content[appcluster].content.head.content.name = name;
		usr_hipopo.usr.content.apps.content[appcluster].content.head.content.apptitle = apptitle;
		usr_hipopo.usr.content.apps.content[appcluster].content.head.content.appcontitle = appcontitle;

		usr_hipopo.usr.content.apps.content[appcluster].content.body.content = content;
	},

	determine: function(appcluster, bodytypeprop, name, apptitle, appcontitle, content) { // determine weather app should be created
		var appcluster = appcluster, bodytypeprop = bodytypeprop, name = name, apptitle = apptitle, appcontitle = appcontitle, content = content;

		var existance = hpp.exists("usr_hipopo", "usr", "content", "apps", "content", appcluster);

		if(existance) {
			hpp.action(appcluster, bodytypeprop, name, apptitle, appcontitle, content);
		} else {
			console.log("this app already exists");

			var a = confirm("If you press \"OK\", this program will be rewritten. Otherwise it'll stay as it is.");

			if (a === true) {
				hpp.action(appcluster, bodytypeprop, name, apptitle, appcontitle, content);
			} else {
				console.log("This app will stay as it is");
			}
		}
	},

	ba: function(appcluster, bodytypeprop, name, apptitle, appcontitle, content) { // build a new app
		hpp.determine(appcluster, bodytypeprop, name, apptitle, appcontitle, content);
		var saa = hpp.saa(ua.appcluster);
		return saa;
	},

	saa: function(appname) { // start app, same as hipopo() but for apps only
		// reader
		// var location = hpp.browseapp(appname);
		// console.log(usr_hipopo);
		// console.log(usr_hipopo.usr);
		// console.log(typeof usr_hipopo.usr);
		// console.log(usr_hipopo.usr.content);
		// console.log(usr_hipopo.usr.content.apps.content[appname].content.head.content.name);
		var name = usr_hipopo.usr.content.apps.content[appname].content.head.content.name;
		var apptitle = usr_hipopo.usr.content.apps.content[appname].content.head.content.apptitle;
		var appcontitle = usr_hipopo.usr.content.apps.content[appname].content.head.content.appcontitle;
		var content = usr_hipopo.usr.content.apps.content[appname].content.body.content;
		console.log(content);
		return {
			name: name,
			apptitle: apptitle,
			appcontitle: appcontitle,
			content: content
		};
	},
	browseapp: function() { // change name to app location

	}
};
