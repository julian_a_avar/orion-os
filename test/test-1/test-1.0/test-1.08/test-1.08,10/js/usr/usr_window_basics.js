// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano
var app = ".app";

var usraa = { // app anatomy
	left_coord: "0", // coordinates from left
	top_coord: "0", // coordinates from left

	app_width: "100vw", // app height
	app_height: "100vh", // app height

	head: ".head",
	close: ".close",
	maximize: ".maximize",
	minimize: ".minimize"
};

var usrapp_dragprop = {
	scroll: false,
	snap: true,
	snapMode: "outer",
	snapTolerance: 7,
	handle: ".head"
};

function usr_window_basics() {
	$(app).draggable({
		scroll: usrapp_dragprop.scroll,
		snap: usrapp_dragprop.snap,
		snapMode: usrapp_dragprop.snapMode,
		snapTolerance: usrapp_dragprop.snapTolerance,
		handle: usrapp_dragprop.handle
	}).resizable();

	$(usraa.close).click(function() {
		$(this).parents(app).remove();
	});

	$(usraa.maximize).click(function() {
		switch($(this).parents(app).hasClass("maximized")) {
			case true:
				$(this).parents(app).css({
					"top": usraa.top_coord,
					"left": usraa.left_coord,
					"width": usraa.app_width,
					"height": usraa.app_height
				});
				usraa.left_coord = $(this).parents(app).css("left");
				usraa.top_coord = $(this).parents(app).css("top");
				usraa.app_height = $(this).parents(app).css("height");
				usraa.app_width = $(this).parents(app).css("width");
				$(this).parents(app).removeClass("maximized");
				break;
			case false:
				usraa.left_coord = $(this).parents(app).css("left");
				usraa.top_coord = $(this).parents(app).css("top");
				usraa.app_height = $(this).parents(app).css("height");
				usraa.app_width = $(this).parents(app).css("width");
				$(this).parents(app).css({
					"top": "0",
					"left": "0",
					"width": "100vw",
					"height": "100vh"
				});
				$(this).parents(app).addClass("maximized");
				break;
		}
	});

	$(usraa.minimize).click(function() {
		if(!$(this).parents(app).hasClass("minimized")) {
			$(this).parents(app).css({
				"display": usrtbm.mnmzd_dsply
			});

			usrtbm.amm(this);

			$(this).parents(app).addClass("minimized");
		}
	});
}
