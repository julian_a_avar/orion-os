var usram = { // usr app manager
	/*
		Variable model:
			app_usr_apptitle_appname => appname in string => example: app_usr_apptitle_reader = "Reader"
	  	app_usr_fullapp_appname => content of app => example: app_usr_fullapp_reader = "blablabla"
			app_usr_fullappcon_appname => content of appcon => example: app_usr_fullappcon_reader = "blablabla"
			app_usr_appclass_appclass => app class => example: app_usr_appclass_reader = ".app_usr_appclass_reader"
			app_usr_appclass_appconclass => appcon class => example: app_usr_appclass_reader = ".app_usr_appclass_reader"
	*/
	wc: function(appclass, appname, fullapp) { // window creator
		return "<div class=\"app " + appclass + "\"><div class=\"head\"><div class=\"title\">" + appname + "</div><div class=\"buttons\"><div class=\"close\"></div><div class=\"maximize\"></div><div class=\"minimize\"></div></div></div><div class=\"content\">" + fullapp + "</div></div>"
	},

	vm: function(obj) { // variable manager
		var apptitle = obj.title;

		var appname = "app_usr_apptitle_" + obj.name;
		var appclass = ".app_usr_appclass_" + obj.name;
		var appconclass = ".app_usr_appconclass_" + obj.name;

		var dotlessappclass = appclass.substring(1);

		var fullapp = obj.content;
		fullapp = usram.wc(dotlessappclass, apptitle, fullapp); // name

		usram.sk(appname, appclass, appconclass, fullapp);
		usram.saa(dotlessappclass, appconclass);

		// for(key in object) {
		// 	var apptitle = tte;
		//
		// 	var appname = "app_usr_apptitle_" + key;
		// 	var appclass = ".app_usr_appclass_" + key;
		// 	var appconclass = ".app_usr_appconclass_" + key;
		//
		// 	var dotlessappclass = appclass.substring(1);
		//
		// 	var fullapp = object[key];
		// 	fullapp = usram.wc(dotlessappclass, apptitle, fullapp); // name
		//
		// 	usram.sk(appname, appclass, appconclass, fullapp);
		// 	usram.saa(dotlessappclass, appconclass);
		// }
	},

	vl: {}, // Variable List
	sk: function(appname, appclass, appconclass, fullapp) { // set keys
		usram.vl[appname] = {};
		usram.vl[appname].appclass = appclass;
		usram.vl[appname].appconclass = appconclass;
		usram.vl[appname].fullapp = fullapp;
	},

	usr_al: [], // app list
	usr_acl: [], // appcon list

	saa: function(al, acl) { // Set App list and Appcon list
		usram.usr_al.push(al);
		usram.usr_acl.push(acl);
	},

	/*
	acn = appconclass
	acs = appclass
	fap = fullapp
	*/

	usras: function() { // USR Appcon-manager Start
		var acn, acs, fap;
		for(key in usram.vl) {
				acn = usram.vl[key].appconclass;
				acs = usram.vl[key].appclass;
				fap = usram.vl[key].fullapp;

				usrtbm.acm(acn, acs, fap);
		}
	}
};
