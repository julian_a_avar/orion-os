/*
I'll try to represent usr_hipopo with this object which is expanded for readibility.
var usr_hipopo_model = {
	oos: {
		properties: {
			typeprop: "cluster"
			share: "usr-only"
		},
		content: {
			apps: {
				properties: {
					typeprop: "cluster"
				},
				content: {
					Reader: {
						properties: {
							typeprop: "app-main"
						},
						content: {
							head: {
								properties: {
									typeprop: "app-head&nebula"
								},
								content: {
									name: "reader",
									apptitle: "Reader",
									appcontitle: "R"
								}
							},
							body: {
								properties: {
									typeprop: "app-body&star"
								},
								content: "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>"
							}
						}
					}
				}
			}
		}
	}
};
*/

var usr_hipopo = "{\"usr\":{\"properties\":{\"typeprop\":\"cluster\"},\"content\":{\"apps\":{\"properties\":{\"typeprop\":\"cluster\"},\"content\":{\"reader\":{\"properties\":{\"typeprop\":\"app-main\"},\"content\":{\"head\":{\"properties\":{\"typeprop\":\"app-head&nebula\"},\"content\":{\"name\":\"reader\",\"apptitle\":\"Reader\",\"appcontitle\":\"R\"}},\"body\":{\"properties\":{\"typeprop\":\"app-body&star\"},\"content\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>\"}}}}}}}}";

// console.log(usr_hipopo);
// var usr_hipopo = JSON.parse(usr_hipopo); // to object
// console.log(usr_hipopo);
// var usr_hipopo = JSON.stringify(usr_hipopo); // to string
// console.log(usr_hipopo);
