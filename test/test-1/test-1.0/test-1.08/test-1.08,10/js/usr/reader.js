// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano.

var ua = { // <- All usr apps need, user apps
	appcluster: "reader",
	bodytypeprop: "app-body&star",
	name: "reader",
	apptitle: "Reader",
	appcontitle: "R",
	content: "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>"
};

var h = hpp.ba(ua.appcluster, ua.bodytypeprop, ua.name, ua.apptitle, ua.appcontitle, ua.content);
console.log(hpp.saa().content);
console.log(h);

/*
ua is a list containing the content of the application. To add other programs, you need to select ua.af (additional features).
*/

// var location = "orion\\usr\\apps\\Reader\\body\\content";
// var identifier = ua.appcluster;

// ua.reader = start_app.content;
ua.name = hpp.saa().name;
ua.apptitle = hpp.saa().apptitle;
ua.appcontitle = hpp.saa().appcontitle;
ua.content = hpp.saa().content;
ua.reader = ua.content;
usr_hipopo = JSON.stringify(usr_hipopo); // after hipopo is done being used, string it again

// All usr apps need:
// var tte = "Reader";
usram.vm(ua);
