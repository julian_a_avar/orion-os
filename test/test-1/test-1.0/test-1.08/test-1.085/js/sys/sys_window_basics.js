// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano
var app = ".app";

var sysaa = { // app anatomy
	left_coord: "0", // coordinates from left
	top_coord: "0", // coordinates from left

	app_width: "100vw", // app height
	app_height: "100vh", // app height

	head: ".head",
	close: ".close",
	maximize: ".maximize",
	minimize: ".minimize"
};

var sysapp_dragprop = {
	scroll: false,
	snap: true,
	snapMode: "outer",
	snapTolerance: 7,
	handle: ".head"
};

function sys_window_basics() {
	$(app).draggable({
		scroll: sysapp_dragprop.scroll,
		snap: sysapp_dragprop.snap,
		snapMode: sysapp_dragprop.snapMode,
		snapTolerance: sysapp_dragprop.snapTolerance,
		handle: sysapp_dragprop.handle
	}).resizable();

	$(sysaa.close).click(function() {
		$(this).parents(app).remove();
	});

	$(sysaa.maximize).click(function() {
		switch($(this).parents(app).hasClass("maximized")) {
			case true:
				$(this).parents(app).css({
					"top": sysaa.top_coord,
					"left": sysaa.left_coord,
					"width": sysaa.app_width,
					"height": sysaa.app_height
				});
				sysaa.left_coord = $(this).parents(app).css("left");
				sysaa.top_coord = $(this).parents(app).css("top");
				sysaa.app_height = $(this).parents(app).css("height");
				sysaa.app_width = $(this).parents(app).css("width");
				$(this).parents(app).removeClass("maximized");
				break;
			case false:
				sysaa.left_coord = $(this).parents(app).css("left");
				sysaa.top_coord = $(this).parents(app).css("top");
				sysaa.app_height = $(this).parents(app).css("height");
				sysaa.app_width = $(this).parents(app).css("width");
				$(this).parents(app).css({
					"top": "0",
					"left": "0",
					"width": "100vw",
					"height": "100vh"
				});
				$(this).parents(app).addClass("maximized");
				break;
		}
	});

	$(sysaa.minimize).click(function() {
		if(!$(this).parents(app).hasClass("minimized")) {
			$(this).parents(app).css({
				"display": systbm.mnmzd_dsply
			});

			systbm.amm(this);

			$(this).parents(app).addClass("minimized");
		}
	});
}
