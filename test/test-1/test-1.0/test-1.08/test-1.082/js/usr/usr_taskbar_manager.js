var i;
var $body = $("body");

var appreader = ".appreader";
var appconreader = ".appconreader";
al.push("appreader");
acl.push(appconreader);

var usr_tbm = { // taskbar manager
	unmnmzd_clr: "#ffffff", // unminimized appcon color
	mnmzd_clr: "#aaaaaa", // minimized appcon color

	unmnmzd_dsply: "block", // unminimized display
	mnmzd_dsply: "none", // minimized display

	exists: function(app_name) { // app open?
		return $(app_name).length;
	},

	acm: function(appcon_name, app_name, app_content_name) { // appcon manager
		$(appcon_name).click(function() {
			if(usr_tbm.exists(app_name)) {
				if($(app_name).hasClass("minimized")) {
					$(app_name).css({"display":usr_tbm.unmnmzd_dsply});
					$(appcon_name).css({"background-color":usr_tbm.unmnmzd_clr});
					$(app_name).removeClass("minimized");
				} else {
					alert("app already exists");
				}
			} else {
				$body.append(app_content_name);
				window_basics();
			}
		});
	},

	amm: function(element_this) { // app minimizing manager
		for(i = 0; i < al.length; i++) {
			if($(element_this).parents(app).hasClass(al[i])) {
				$(acl[i]).css({
					"background-color": usr_tbm.mnmzd_clr
				});
			}
		}
	}

};

function usras() { // usr appcon manager starter
	usr_tbm.acm(appconreader, appreader, app_reader);
}
