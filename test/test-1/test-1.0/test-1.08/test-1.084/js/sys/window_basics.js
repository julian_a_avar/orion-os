// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano
var app = ".app";

var aa = { // app anatomy
	left_coord: "0", // coordinates from left
	top_coord: "0", // coordinates from left

	app_width: "100vw", // app height
	app_height: "100vh", // app height

	head: ".head",
	close: ".close",
	maximize: ".maximize",
	minimize: ".minimize"
};

var app_dragprop = {
	scroll: false,
	snap: true,
	snapMode: "outer",
	snapTolerance: 7,
	handle: ".head"
};

function window_basics() {
	$(app).draggable({
		scroll: app_dragprop.scroll,
		snap: app_dragprop.snap,
		snapMode: app_dragprop.snapMode,
		snapTolerance: app_dragprop.snapTolerance,
		handle: app_dragprop.handle
	}).resizable();

	$(aa.close).click(function() {
		$(this).parents(app).remove();
	});

	$(aa.maximize).click(function() {
		switch($(this).parents(app).hasClass("maximized")) {
			case true:
				$(this).parents(app).css({
					"top": aa.top_coord,
					"left": aa.left_coord,
					"width": aa.app_width,
					"height": aa.app_height
				});
				aa.left_coord = $(this).parents(app).css("left");
				aa.top_coord = $(this).parents(app).css("top");
				aa.app_height = $(this).parents(app).css("height");
				aa.app_width = $(this).parents(app).css("width");
				$(this).parents(app).removeClass("maximized");
				break;
			case false:
				aa.left_coord = $(this).parents(app).css("left");
				aa.top_coord = $(this).parents(app).css("top");
				aa.app_height = $(this).parents(app).css("height");
				aa.app_width = $(this).parents(app).css("width");
				$(this).parents(app).css({
					"top": "0",
					"left": "0",
					"width": "100vw",
					"height": "100vh"
				});
				$(this).parents(app).addClass("maximized");
				break;
		}
	});

	$(aa.minimize).click(function() {
		if(!$(this).parents(app).hasClass("minimized")) {
			$(this).parents(app).css({
				"display": systbm.mnmzd_dsply
			});

			systbm.amm(this);

			$(this).parents(app).addClass("minimized");
		}
	});
}
