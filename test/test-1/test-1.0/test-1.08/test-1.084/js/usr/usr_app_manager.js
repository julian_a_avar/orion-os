var usram = { // usr app manager
	/*
		auacac = app_usr_appclass_appclass
		auatan = app_usr_apptitle_appname
		aufaan = app_usr_fullapp_appname
	*/
	/*
		Variable model:
			appcon = c

			auatan = app_usr_apptitle_appname => appname in string => example: app_usr_apptitle_reader = "Reader"

	  	aufaan = app_usr_fullapp_appname => content of app => example: app_usr_fullapp_reader = "blablabla"
			aufcan = app_usr_fullappcon_appname => content of appcon => example: app_usr_fullappcon_reader = "blablabla"

			auacac = app_usr_appclass_appclass => app class => example: app_usr_appclass_reader = ".app_usr_appclass_reader"
			auaccc = app_usr_appclass_appconclass => appcon class => example: app_usr_appclass_reader = ".app_usr_appclass_reader"
	*/
	/*wc: function(aufac, aufat, aufan) { // window creator
		var wcr = "<div class=\"app " + aufac + "\><div class=\"head\"><div class=\"title\">" + aufat + "</div><div class=\"buttons\"><div class=\"close\"></div><div class=\"maximize\"></div><div class=\"minimize\"></div></div></div><div class=\"content\">" + aufan + "</div></div>"
	},*/

	vm: function(object) { // variable manager
		for(key in object) {
			var appname = tte;

			var appname = "app_usr_apptitle_" + key;
			var appclass = ".app_usr_appclass_" + key;
			var appconclass = ".app_usr_appconclass_" + key;
			var fullapp = object[key];

			var dotlessappclass = appclass.substring(1);

			usram.sk(appname, appclass, appconclass, fullapp);
			usram.saa(dotlessappclass, appconclass);
			// key = usram.wc(aufac, aufat, aufan); // name
		}
	},

	vl: {}, // Variable List
	sk: function(appname, appclass, appconclass, fullapp) { // set keys
		usram.vl[appname] = {};
		usram.vl[appname].appclass = appclass;
		usram.vl[appname].appconclass = appconclass;
		usram.vl[appname].fullapp = fullapp;
		// console.log(usram.vl);
		// console.log(usram.vl[appname]);
		// console.log(usram.vl[appname].appclass);
		// console.log(usram.vl[appname].appconclass);
		// console.log(usram.vl[appname].fullapp);
	},

	usr_al: [], // app list
	usr_acl: [], // appcon list

	saa: function(al, acl) { // Set App list and Appcon list
		usram.usr_al.push(al);
		usram.usr_acl.push(acl);
	},

	/*
	acn = appconclass
	acs = appclass
	fap = fullapp
	*/

	usras: function() { // USR Appcon-manager Start
		var ane, acs, fap;
		console.log("initializing usras");
		for(key in usram.vl) {
				acn = usram.vl[key]["appconclass"];
				acs = usram.vl[key]["appclass"];
				fap = usram.vl[key]["fullapp"];
				console.log(acn);
				console.log(acs);
				console.log(fap);
				console.log("made it through the loop");
				usrtbm.acm(acn, acs, fap);
				console.log("made it all the way through the loop");
		}
		console.log("finished usras");
	}
};
