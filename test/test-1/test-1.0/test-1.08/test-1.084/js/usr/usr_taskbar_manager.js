// The GNU General Public License v2.0, Copyright (c) 2015 Copyright Julian Antonio Avar Campopiano
var i;
var $body = $("body");

var usrtbm = { // taskbar manager
	unmnmzd_clr: "#ffffff", // unminimized appcon color
	mnmzd_clr: "#aaaaaa", // minimized appcon color

	unmnmzd_dsply: "block", // unminimized display
	mnmzd_dsply: "none", // minimized display

	exists: function(fullapp) { // app open?
		return $(fullapp).length;
	},

	acm: function(appconclass, appclass, fullapp) { // appcon manager
		console.log("begined acm");
		// console.log(appconclass);
		// console.log(appclass);
		// console.log(fullapp);
		console.log(appconclass);
		$(appconclass).click(function() {
			console.log("you clicked me");
			// alert("something");
			// console.log("made it");
			if(usrtbm.exists(appclass)) {
				if($(appclass).hasClass("minimized")) {
					$(appclass).css({"display": usrtbm.unmnmzd_dsply});
					$(appclass).css({"background-color": usrtbm.unmnmzd_clr});
					$(appclass).removeClass("minimized");
				} else {
					alert("app already exists");
				}
			} else {
				$body.append(fullapp);
				window_basics();
			}
		});
		console.log("you finished acm");
	},

	amm: function(t) { // app minimizing manager
		for(i = 0; i < usram.usr_al.length; i++) {
			if((this).parents(app).hasClass(usram.usr_al[i])) {
				$(usram.usr_acl[i]).css({
					"background-color": usrtbm.mnmzd_clr
				});
			}
		}
	},

	usras: function() {
		console.log("starting usras");
		// usrtbm.acm(appconreader, appreader, app_reader);
		usram.usras();
		console.log("made it all the way");
	}
};
