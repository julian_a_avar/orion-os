// Setting my variables

// The following set of vars is required for $(maximize).click() event
var Left;
var Top;
var Height;
var Width;

// setting the commoned used classes & apps
var app = ".app";
var app1 = ".app1";
var app2 = ".app2";

// setting the different parts of an app
var head = ".head";
var close = ".close";
var maximize = ".maximize";
var minimize = ".minimize";

// Defining appcons
var appcon1 = ".appcon1";
var appcon2 = ".appcon2";


function app_basics() {
	$(app).draggable({
		scroll: false,
		snap: true,
		snapMode: "outer",
		snapTolerance: 7,
		handle: head
	}).resizable();

	$(close).click(function() {
		$(this).parents(app).remove();
	});

	$(maximize).click(function() {
		if($(this).parents(app).hasClass("maximized") == false) {
			if($(this).parents(app).hasClass("maximizedOnce") == false) {
				$(this).parents(app).addClass("maximizedOnce");

				Left = $(this).parents(app).css("left");
				Top = $(this).parents(app).css("top");
				Height = $(this).parents(app).css("height");
				Width = $(this).parents(app).css("width");

				$(this).parents(app).css({
					"top": "0",
					"left": "0",
					"width": "100vw",
					"height": "100vh"
				});
			} else {
				Left = $(this).parents(app).css("left");
				Top = $(this).parents(app).css("top");
				Height = $(this).parents(app).css("height");
				Width = $(this).parents(app).css("width");

				$(this).parents(app).css({
					"top": "0",
					"left": "0",
					"width": "100vw",
					"height": "100vh"
				});
			}
			$(this).parents(app).addClass("maximized");
		} else {
			$(this).parents(app).css({
				"top": Top,
				"left": Left,
				"width": Width,
				"height": Height
			});

			Left = $(this).parents(app).css("left");
			Top = $(this).parents(app).css("top");
			Height = $(this).parents(app).css("height");
			Width = $(this).parents(app).css("width");
			$(this).parents(app).removeClass("maximized");
		}
	});

	$(minimize).click(function() {
		if($(this).parents(app).hasClass("minimized") == false) {
			$this.parents(app).css({
				"display": "none"
			});

			if($(this).parents(app).hasClass("app1")) {
				$(appcon1).css({
					"background-color": apcmgr.opened_clr
				});
			} else if($(this).parents(app).hasClass("app2")) {
				$(appcon2).css({
					"background-color": apcmgr.opened_clr
				});
			}

			$(this).parents(app).addClass("minimized");
		}
	});
}
