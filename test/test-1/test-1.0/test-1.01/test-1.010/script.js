$(function() {
	var app = ".app";
	var app1 = ".app1";
	var app2 = ".app2";
	var head = ".head";
	var close = ".close";
	var maximize = ".maximize";
	var minimize = ".minimize";
	var appcon = ".appcon";
	var appcon1 = ".appcon1";
	var appcon2 = ".appcon2";
	var app_1 = $(".app1").clone();
	var app_2 = $(".app2").clone();

	app_basics();

	$(appcon1).click(function() {
		if($(app1).length) {
			if($(app1).css("display") == "none") {
				$(app1).css({
					"display": "block"
				});
				$(appcon1).css({
					"background-color": "#ffffff"
				});
			} else {
				alert("appcon already exists");
			}
		} else {
			alert("making new app");
			$("body").append(app_1);
			app_basics();
		}
	});

	$(appcon2).click(function() {
		if($(app2).length) {
			if($(app2).css("display") == "none") {
				$(app2).css({
					"display": "block"
				});
				$(appcon2).css({
					"background-color": "#ffffff"
				});
			} else {
				alert("appcon already exists");
			}
		} else {
			alert("making new app");
			$("body").append(app_2);
			app_basics();
		}
	});

	function app_basics() {
		$(app).draggable({
			scroll: false,
			snap: true,
			snapMode: "outer",
			snapTolerance: 7,
			// zIndex: 1,
			handle: head
		}).resizable();

		$(close).click(function() {
			$(this).parents(app).remove();
			alert("Closing app");
		});

		$(maximize).click(function() {
			if($(this).parents(app).css("width") == "500px" && $(this).parents(app).css("height") == "200px") {
				$(this).parents(app).css({
					"top": "0",
					"left": "0",
					"width": "100vw",
					"height": "100vh"
				});
			} else {
				$(this).parents(app).css({
					"top": "0",
					"left": "0",
					"width": "500px",
					"height": "200px"
				});
			}
		});

		$(minimize).click(function() {
			var element = $(this).parents(app)[0];

			$(this).parents(app).css({
				"display": "none"
			});
			if(element == $(app1)[0]) {
				$(appcon1).css({
					"background-color": "#aaaaaa"
				});
			} else if(element == $(app2)[0]) {
				$(appcon2).css({
					"background-color": "#aaaaaa"
				});
			}
		});
	}
});