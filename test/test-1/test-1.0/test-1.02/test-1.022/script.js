$(function() {
	var Left;
	var Top;
	var Height;
	var Width;

	var app = ".app";
	var app1 = ".app1";
	var app2 = ".app2";
	var head = ".head";
	var close = ".close";
	var maximize = ".maximize";
	var minimize = ".minimize";
	var appcon = ".appcon";
	var appcon1 = ".appcon1";
	var appcon2 = ".appcon2";
	var app_1 = "<div class=\"app app1\"><div class=\"head\"><div class=\"title\">App 1</div><div class=\"buttons\"><div class=\"close\"></div><div class=\"maximize\"></div><div class=\"minimize\"></div></div></div><div class=\"content\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p></div></div>";
	var app_2 = "<div class=\"app app2\"><div class=\"head\"><div class=\"title\">App 2</div><div class=\"buttons\"><div class=\"close\"></div><div class=\"maximize\"></div><div class=\"minimize\"></div></div></div><div class=\"content\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p></div></div>";
	$("body").append(app_1);
	$("body").append(app_2);

	app_basics();

	$(appcon1).click(function() {
		if($(app1).length) {
			if($(app1).css("display") == "none") {
				$(app1).css({
					"display": "block"
				});
				$(appcon1).css({
					"background-color": "#ffffff"
				});
			} else {
				alert("appcon already exists");
			}
		} else {
			// alert("making new app");
			$("body").append(app_1);
			app_basics();
		}
	});

	$(appcon2).click(function() {
		if($(app2).length) {
			if($(app2).css("display") == "none") {
				$(app2).css({
					"display": "block"
				});
				$(appcon2).css({
					"background-color": "#ffffff"
				});
			} else {
				alert("appcon already exists");
			}
		} else {
			// alert("making new app");
			$("body").append(app_2);
			app_basics();
		}
	});

	function app_basics() {

		$(app).draggable({
			scroll: false,
			snap: true,
			snapMode: "outer",
			snapTolerance: 7,
			// zIndex: 1,
			handle: head
		}).resizable();

		$(close).click(function() {
			$(this).parents(app).remove();
			// alert("Closing app");
		});

		$(maximize).click(function() {
			if($(this).parents(app).hasClass("maximized") == false) {
				if($(this).parents(app).hasClass("maximizedOnce") == false) {
					$(this).parents(app).addClass("maximizedOnce");

					Left = $(this).parents(app).css("left");
					Top = $(this).parents(app).css("top");
					Height = $(this).parents(app).css("height");
					Width = $(this).parents(app).css("width");

					$(this).parents(app).css({
						"top": "0",
						"left": "0",
						"width": "100vw",
						"height": "100vh"
					});
				} else {
					Left = $(this).parents(app).css("left");
					Top = $(this).parents(app).css("top");
					Height = $(this).parents(app).css("height");
					Width = $(this).parents(app).css("width");

					$(this).parents(app).css({
						"top": "0",
						"left": "0",
						"width": "100vw",
						"height": "100vh"
					});
				}
				$(this).parents(app).addClass("maximized");
			} else {
				$(this).parents(app).css({
					"top": Top,
					"left": Left,
					"width": Width,
					"height": Height
				});

				Left = $(this).parents(app).css("left");
				Top = $(this).parents(app).css("top");
				Height = $(this).parents(app).css("height");
				Width = $(this).parents(app).css("width");
				$(this).parents(app).removeClass("maximized");
			}
		});

		$(minimize).click(function() {
			var element = $(this).parents(app)[0];

			$(this).parents(app).css({
				"display": "none"
			});
			if(element == $(app1)[0]) {
				$(appcon1).css({
					"background-color": "#aaaaaa"
				});
			} else if(element == $(app2)[0]) {
				$(appcon2).css({
					"background-color": "#aaaaaa"
				});
			}
		});
	}
});