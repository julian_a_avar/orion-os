var app_antmy = {
	left_coord: "0", // coordinates from left
	top_coord: "0", // coordinates from left

	app_width: "100vw", // app height
	app_height: "100vh", // app height

	head: ".head",
	close: ".close",
	maximize: ".maximize",
	minimize: ".minimize"
}

var app_dragprop = {
	scroll: false,
	snap: true,
	snapMode: "outer",
	snapTolerance: 7,
	handle: ".head"
}

var app = ".app";
var app1 = ".app1";
var app2 = ".app2";
var appcon1 = ".appcon1";
var appcon2 = ".appcon2";

function window_basics() {
	$(app).draggable({scroll:app_dragprop.scroll,snap:app_dragprop.snap,snapMode:app_dragprop.snapMode,snapTolerance:app_dragprop.snapTolerance,handle:app_dragprop.handle}).resizable();

	$(app_antmy.close).click(function() {
		$(this).parents(app).remove();
	});

	$(app_antmy.maximize).click(function() {
		if($(this).parents(app).hasClass("maximized") == false) {
			app_antmy.left_coord = $(this).parents(app).css("left");
			app_antmy.top_coord = $(this).parents(app).css("top");
			app_antmy.app_height = $(this).parents(app).css("height");
			app_antmy.app_width = $(this).parents(app).css("width");
			$(this).parents(app).css({
				"top": "0",
				"left": "0",
				"width": "100vw",
				"height": "100vh"
			});
			$(this).parents(app).addClass("maximized");
		} else {
			$(this).parents(app).css({
				"top": app_antmy.top_coord,
				"left": app_antmy.left_coord,
				"width": app_antmy.app_width,
				"height": app_antmy.app_height
			});
			app_antmy.left_coord = $(this).parents(app).css("left");
			app_antmy.top_coord = $(this).parents(app).css("top");
			app_antmy.app_height = $(this).parents(app).css("height");
			app_antmy.app_width = $(this).parents(app).css("width");
			$(this).parents(app).removeClass("maximized");
		}
	});

	$(app_antmy.minimize).click(function() {
		if($(this).parents(app).hasClass("minimized") == false) {
			$(this).parents(app).css({
				"display": tkrmar.mnmzd_dsply
			});

			if($(this).parents(app).hasClass("app1")) {
				$(appcon1).css({
					"background-color": tkrmar.unmnmzd_clr
				});
			} else if($(this).parents(app).hasClass("app2")) {
				$(appcon2).css({
					"background-color": tkrmar.unmnmzd_clr
				});
			}

			$(this).parents(app).addClass("minimized");
		}
	});
}
