var $body = $("body");
var app1 = ".app1";
var app2 = ".app2";
var appcon1 = ".appcon1";
var appcon2 = ".appcon2";

var tkrmar = { // task manager
	unmnmzd_clr: "#ffffff", // unminimized appcon color
	mnmzd_clr: "#aaaaaa", // minimized appcon color

	unmnmzd_dsply: "block", // unminimized display
	mnmzd_dsply: "none", // minimized display

	exists: function(app_name) { // app open?
		return $(app_name).length;
	}
};

function taskbar_manager() {
	$(appcon1).click(function() {
		if(tkrmar.exists(app1)) {
			if($(app1).css("display") == tkrmar.mnmzd_dsply) {
				$(app1).css({
					"display": tkrmar.unmnmzd_dsply
				});
				$(appcon1).css({
					"background-color": tkrmar.unmnmzd_clr
				});
			} else {
				alert("app already exists");
			}
		} else {
			$body.append(app_1);
			window_basics();
		}
	});

	$(appcon2).click(function() {
		if(tkrmar.exists(app2)) {
			if($(app2).css("display") == tkrmar.mnmzd_dsply) {
				$(app2).css({
					"display": tkrmar.unmnmzd_dsply
				});
				$(appcon2).css({
					"background-color": tkrmar.unmnmzd_clr
				});
			} else {
				alert("app already exists");
			}
		} else {
			$body.append(app_2);
			window_basics();
		}
	});
}
