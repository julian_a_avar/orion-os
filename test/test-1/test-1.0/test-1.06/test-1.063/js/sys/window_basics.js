var app_aty = {
	left_coord: "0", // coordinates from left
	top_coord: "0", // coordinates from left

	app_width: "100vw", // app height
	app_height: "100vh", // app height

	head: ".head",
	close: ".close",
	maximize: ".maximize",
	minimize: ".minimize"
}

var app_dragprop = {
	scroll: false,
	snap: true,
	snapMode: "outer",
	snapTolerance: 7,
	handle: ".head"
}

var app = ".app";
var app1 = ".app1";
var app2 = ".app2";
var appcon1 = ".appcon1";
var appcon2 = ".appcon2";

function window_basics() {
	$(app).draggable({scroll:app_dragprop.scroll,snap:app_dragprop.snap,snapMode:app_dragprop.snapMode,snapTolerance:app_dragprop.snapTolerance,handle:app_dragprop.handle}).resizable();

	$(app_aty.close).click(function() {
		$(this).parents(app).remove();
	});

	$(app_aty.maximize).click(function() {
		if($(this).parents(app).hasClass("maximized") == false) {
			app_aty.left_coord = $(this).parents(app).css("left");
			app_aty.top_coord = $(this).parents(app).css("top");
			app_aty.app_height = $(this).parents(app).css("height");
			app_aty.app_width = $(this).parents(app).css("width");
			$(this).parents(app).css({
				"top": "0",
				"left": "0",
				"width": "100vw",
				"height": "100vh"
			});
			$(this).parents(app).addClass("maximized");
		} else {
			$(this).parents(app).css({
				"top": app_aty.top_coord,
				"left": app_aty.left_coord,
				"width": app_aty.app_width,
				"height": app_aty.app_height
			});
			app_aty.left_coord = $(this).parents(app).css("left");
			app_aty.top_coord = $(this).parents(app).css("top");
			app_aty.app_height = $(this).parents(app).css("height");
			app_aty.app_width = $(this).parents(app).css("width");
			$(this).parents(app).removeClass("maximized");
		}
	});

	$(app_aty.minimize).click(function() {
		if($(this).parents(app).hasClass("minimized") == false) {
			$(this).parents(app).css({
				"display": tbm.mnmzd_dsply
			});

			tbm.amm(this);

			$(this).parents(app).addClass("minimized");
		}
	});
}
