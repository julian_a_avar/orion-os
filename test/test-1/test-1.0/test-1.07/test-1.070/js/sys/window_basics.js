var app = ".app";

var aa = { // app anatomy
	left_coord: "0", // coordinates from left
	top_coord: "0", // coordinates from left

	app_width: "100vw", // app height
	app_height: "100vh", // app height

	head: ".head", // window head
	close: ".close", // window close button
	maximize: ".maximize", // window maximize button
	minimize: ".minimize", // window minimize button

	ad_prop: {
		scroll: false,
		snap: true,
		snapMode: "outer",
		snapTolerance: 7,
		handle: ".head"
	}
}

function window_basics() {
	$(app).draggable({
		scroll: aa.ad_prop.scroll,
		snap: aa.ad_prop.snap,
		snapMode: aa.ad_prop.snapMode,
		snapTolerance: aa.ad_prop.snapTolerance,
		handle: aa.ad_prop.handle
	}).resizable();

	$(aa.close).click(function() {
		$(this).parents(app).remove();
	});

	$(aa.maximize).click(function() {
		if($(this).parents(app).hasClass("maximized") == false) {
			aa.left_coord = $(this).parents(app).css("left");
			aa.top_coord = $(this).parents(app).css("top");
			aa.app_height = $(this).parents(app).css("height");
			aa.app_width = $(this).parents(app).css("width");
			$(this).parents(app).css({
				"top": "0",
				"left": "0",
				"width": "100vw",
				"height": "100vh"
			});
			$(this).parents(app).addClass("maximized");
		} else {
			$(this).parents(app).css({
				"top": aa.top_coord,
				"left": aa.left_coord,
				"width": aa.app_width,
				"height": aa.app_height
			});
			aa.left_coord = $(this).parents(app).css("left");
			aa.top_coord = $(this).parents(app).css("top");
			aa.app_height = $(this).parents(app).css("height");
			aa.app_width = $(this).parents(app).css("width");
			$(this).parents(app).removeClass("maximized");
		}
	});

	$(aa.minimize).click(function() {
		if($(this).parents(app).hasClass("minimized") == false) {
			$(this).parents(app).css({
				"display": sys_tbm.mnmzd_dsply
			});

			sys_tbm.amm(this);

			$(this).parents(app).addClass("minimized");
		}
	});
}
