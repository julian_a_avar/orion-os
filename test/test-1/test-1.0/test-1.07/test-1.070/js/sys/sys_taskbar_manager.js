var i;
var $body = $("body");

/* start */
var app1 = ".app1";
var app2 = ".app2";

var appcon1 = ".appcon1";
var appcon2 = ".appcon2";

var sys_al = ["app1", "app2"]; // app list
var sys_acl = [appcon1, appcon2]; // appcon lis
/* end */

var sys_tbm = { // taskbar manager
	unmnmzd_clr: "#ffffff", // unminimized appcon color
	mnmzd_clr: "#aaaaaa", // minimized appcon color

	unmnmzd_dsply: "block", // unminimized display
	mnmzd_dsply: "none", // minimized display

	exists: function(app_name) { // app open?
		return $(app_name).length;
	},

	acm: function(appcon_name, app_name, app_content_name) { // appcon manager
		$(appcon_name).click(function() {
			if(sys_tbm.exists(app_name)) {
				if($(app_name).css("display") == sys_tbm.mnmzd_dsply) {
					$(app_name).removeClass("minimized");
					$(app_name).css({
						"display": sys_tbm.unmnmzd_dsply
					});
					$(appcon_name).css({
						"background-color": sys_tbm.unmnmzd_clr
					});
				} else {
					alert("app already exists");
				}
			} else {
				$body.append(app_content_name);
				window_basics();
			}
		});
	},

	/*
		Always use $this as the parameter!
	*/
	amm: function(element_this) { // app minimizing manager
		for(i = 0; i < sys_al.length; i++) {
			if($(element_this).parents(app).hasClass(sys_al[i])) {
				$(sys_acl[i]).css({
					"background-color": sys_tbm.mnmzd_clr
				});
			}
		}
	}

};

/* start */
function sys_acm_start() { // appcon manager starter
	sys_tbm.acm(appcon1, app1, app_1);
	sys_tbm.acm(appcon2, app2, app_2);
	sys_tbm.acm(appconreader, appreader, app_reader);
}
/* end */
