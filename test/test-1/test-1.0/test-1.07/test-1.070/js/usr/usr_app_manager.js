var usram = { // usr app manager
	/*
		aufac = app_usr_fullapp_appclass
		aufat = app_usr_fullapp_apptitle
		aufan = app_usr_fullapp_appname (it's content)
	*/
	wc: function(aufac, aufat, aufan) { // window creator
		var wcr = "<div class=\"app " + aufac + "\><div class=\"head\"><div class=\"title\">" + aufat + "</div><div class=\"buttons\"><div class=\"close\"></div><div class=\"maximize\"></div><div class=\"minimize\"></div></div></div><div class=\"content\">" + aufan + "</div></div>"
	},

	vm: function(object) { // variable manager
		for(key in object) {
			var aufac = key + "class";
			var aufat = t;
			var aufan = object.key;

			var appclass = aufac;
			var appclassvalue = "." + key;

			var appcon = key + "appcon";
			var appconvalue = "." + key + "appcon";

			usram.sk(key, appclass, appclassvalue, appcon, appconvalue);
			usram.saa(key, appcon);
			key = usram.wc(aufac, aufat, aufan); // name
		}
	},

	vl: {}, // variable list
	sk: function(appname, appclass, appclassvalue, appcon, appconvalue) { // set key
		usram.vl[appname] = {};
		usram.vl[appname][appclass] = appclassvalue;
		usram.vl[appname][appcon] = appconvalue;
	},

	usr_al: [], // app list
	usr_acl: [] // appcon list

	saa: function(al, acl) { // set app list and appcon list
		usram.usr_al.push(al);
		usram.usr_acl.push(acl);
	}

	/*
	acn = appcon
	acs = appclass
	ane = appname
	*/
	usras: function(acn, acs, ane) { // usr appcon manager start
		for(key in vl) {
			for(vl[key] in app_prop {
				acn = vl[key][app_prop][0];
				acs = vl[key][app_prop][1];
			}
			ane = vl[key];
		}
		usrtbm.acm(acn, acs, ane);
	}
};
