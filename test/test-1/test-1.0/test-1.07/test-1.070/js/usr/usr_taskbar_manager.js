var i;
var $body = $("body");

var usrtbm = { // taskbar manager
	unmnmzd_clr: "#ffffff", // unminimized appcon color
	mnmzd_clr: "#aaaaaa", // minimized appcon color

	unmnmzd_dsply: "block", // unminimized display
	mnmzd_dsply: "none", // minimized display

	exists: function(app_name) { // app open?
		return $(app_name).length;
	},

	acm: function(appcon_name, app_name, app_content_name) { // appcon manager
		$(appcon_name).click(function() {
			if(usrtbm.exists(app_name)) {
				if($(app_name).css("display") == usrtbm.mnmzd_dsply) {
					$(app_name).removeClass("minimized");
					$(app_name).css({
						"display": usrtbm.unmnmzd_dsply
					});
					$(appcon_name).css({
						"background-color": usrtbm.unmnmzd_clr
					});
				} else {
					alert("app already exists");
				}
			} else {
				$body.append(app_content_name);
				window_basics();
			}
		});
	},

	/*
		Always use $this as the parameter of usrtbm.amm(this) <- just like that!
	*/
	amm: function(element_this) { // app minimizing manager
		for(i = 0; i < usram.usr_al.length; i++) {
			if($(element_this).parents(app).hasClass(usram.usr_al[i])) {
				$(sys_acl[i]).css({
					"background-color": usrtbm.mnmzd_clr
				});
			}
		}
	}

};
