// Variables defined outside:
/*
global app_1
global app_2
*/
$(function() {
	var $body = $("body");
	var $this = $(this);
	var parentApp = $this.parents(app);
	var Left;
	var Top;
	var Height;
	var Width;
	var app = ".app";
	var app1 = ".app1";
	var app2 = ".app2";
	var head = ".head";
	var close = ".close";
	var maximize = ".maximize";
	var minimize = ".minimize";
	var appcon = ".appcon";
	var appcon1 = ".appcon1";
	var appcon2 = ".appcon2";

	app_basics();

	$(appcon1).click(function() {
		if($(app1).length) {
			if($(app1).css("display") == "none") {
				$(app1).css({
					"display": "block"
				});
				$(appcon1).css({
					"background-color": "#ffffff"
				});
			} else {
				alert("appcon already exists");
			}
		} else {
			// alert("making new app");
			$body.append(app_1);
			app_basics();
		}
	});

	$(appcon2).click(function() {
		if($(app2).length) {
			if($(app2).css("display") == "none") {
				$(app2).css({
					"display": "block"
				});
				$(appcon2).css({
					"background-color": "#ffffff"
				});
			} else {
				alert("appcon already exists");
			}
		} else {
			// alert("making new app");
			$body.append(app_2);
			app_basics();
		}
	});

	function app_basics() {

		$(app).draggable({
			scroll: false,
			snap: true,
			snapMode: "outer",
			snapTolerance: 7,
			// zIndex: 1,
			handle: head
		}).resizable();

		$(close).click(function() {
			parentApp.remove();
			// alert("Closing app");
		});

		$(maximize).click(function() {
			if(parentApp.hasClass("maximized") == false) {
				if(parentApp.hasClass("maximizedOnce") == false) {
					parentApp.addClass("maximizedOnce");

					Left = parentApp.css("left");
					Top = parentApp.css("top");
					Height = parentApp.css("height");
					Width = parentApp.css("width");

					parentApp.css({
						"top": "0",
						"left": "0",
						"width": "100vw",
						"height": "100vh"
					});
				} else {
					Left = parentApp.css("left");
					Top = parentApp.css("top");
					Height = parentApp.css("height");
					Width = parentApp.css("width");

					parentApp.css({
						"top": "0",
						"left": "0",
						"width": "100vw",
						"height": "100vh"
					});
				}
				parentApp.addClass("maximized");
			} else {
				parentApp.css({
					"top": Top,
					"left": Left,
					"width": Width,
					"height": Height
				});

				Left = parentApp.css("left");
				Top = parentApp.css("top");
				Height = parentApp.css("height");
				Width = parentApp.css("width");
				parentApp.removeClass("maximized");
			}
		});

		$(minimize).click(function() {
			var element = parentApp[0];

			parentApp.css({
				"display": "none"
			});
			if(element == $(app1)[0]) {
				$(appcon1).css({
					"background-color": "#aaaaaa"
				});
			} else if(element == $(app2)[0]) {
				$(appcon2).css({
					"background-color": "#aaaaaa"
				});
			}
		});
	}
});