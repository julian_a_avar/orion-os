/*
global app_1
global app_2
global app_basics
*/
$(function() {
	var $body = $("body");
	var app1 = ".app1";
	var app2 = ".app2";
	var appcon1 = ".appcon1";
	var appcon2 = ".appcon2";

	app_basics();

	$(appcon1).click(function() {
		if($(app1).length) {
			if($(app1).css("display") == "none") {
				$(app1).css({
					"display": "block"
				});
				$(appcon1).css({
					"background-color": "#ffffff"
				});
			} else {
				alert("app already exists");
			}
		} else {
			$body.append(app_1);
			app_basics();
		}
	});

	$(appcon2).click(function() {
		if($(app2).length) {
			if($(app2).css("display") == "none") {
				$(app2).css({
					"display": "block"
				});
				$(appcon2).css({
					"background-color": "#ffffff"
				});
			} else {
				alert("app already exists");
			}
		} else {
			$body.append(app_2);
			app_basics();
		}
	});
});