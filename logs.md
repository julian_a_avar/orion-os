# Logs

> The version logs of Orion OS will be recorded here.  
> A quick explanation of new features resolved bugs and unresolved bugs, as well as a description for every version will be recorded here. These are called De's.
> There will be two different development descriptions(Dev - des). First we have the SDevde's, is the explanation before(objective summary), and then we have the EDevde's which are the result, what was accomplished, and future objectives(which might change as the development goes on).

`note: Sadly, `[`logs.md`](logs.md)` started just when finishing test 1.050, meaning that there is no logs before that.`  
`note: `[`logs.md`](logs.md)` is highly involved with `[`notes.md`](notes)`, since it's purpose is to record thinking activity`

<br>



# Test - `12/6/15 4:56pm`

**`SDevde - 12/6/15 4:56pm`**

I just started creating Orion OS!

In Test, I hope to accomplish to successfully create `Hipopo`, `Octopi`, `Grasshopper`, and `Lemur`. Test will probably be the  most crucial part of the development, since it is the time when I have to start from scratch. Sadly I did not think of creating a [`logs.md`](logs.md) file until now, which of course will not include the files before. I am therefore starting this file from version `test 1.050`.

<br>

**`EDevde - in progress`**

<br>



## Test 1 - `12/6/15 4:37pm`

**`SDevde - 12/6/15 4:37pm`**

Test 1 will most likely contain every single version subversion and so on of everything inside the Test Orion. I expect having absolutely everything planed to move on to Alpha before I finish Test 1.

<br>

**`EDevde - in progress`**

<br>



### Test 1.1 - `2/22/16 8:32pm`

**`SDevde - 2/22/16 8:32pm`**

After all I have been through, I think its safe to say that I have a long way to go. File manipulation should not get started until I finish hpp, so I will just work on my browser, and other applications!

<br>

**`EDevde - in progress`**

<br>



#### Test 1.10 - `2/22/16 9:02pm`


**`SDevde - 2/22/16 9:02pm`**

In 1.10 I will produce a quantity of applications and create a full API for application genereation, I will also create various hipopo research exams to see how that is going

<br>

**`EDevde - in progress`**

<br>



##### Test 1.104 - `6/30/16 11:31pm`

**`SDevde - 5/19/16 7:23pm`**

<br>

**`EDevde - in progress`**

<br>



##### Test 1.103 - `6/30/16 11:31pm`

**`SDevde - 5/19/16 7:23pm`**

The side panel is momentarily being ignored.

1.103 will be more about power of my platform to create apps. I think I also want to add a terminal, which shuldnt be too hard, but not on 1.103.

So mainly get things done so it's easier to make apps, and to have a more powerful platform.

<br>

**`EDevde - 6/30/16 11:31pm`**

 - 6/25/16

I have done so many darn things in this version, including my very first library `"include.js"`, which I will continue to improve later. For now I'm satisfied with not having a messy `index.html`. I will make `include.js` a little more helpful than it is right now, since it needs it. Now to see the included files go to `@include.js`.

I'm almost finished with nested objects so I'm super happy. Well not so much since I keep failing somewhere.

I got rid of the padding, which is a BIG improvement.

 - 6/29/16

Great discovery!

I have been wondering why it is that `js/consts.js` in `uasoi()`, the entire tree seems to deform once it reaches any more than one secondary object. Furthermore, even if I dont have a secondary object, the tree doesn't seem to load properly to the DOM.

These are two major bugs, and they are so big that they had to be mentioned here.

 - 6/30/16

The solution was unexpected.

Although unexpected it was to be expected. Reiterated variable definitions, which were used earlier in building the object, were later used to build the DOM. Other way of saying this, if you didn't get this yet, building complex modular apps composed of multiple objects are now possible!

Therefore now, while in my summer vacations in Argentina, I'm finishing up version `test 1.103`.

Looking forward to my next release!

<br>



##### Test 1.102 - `5/19/16 7:18pm`

**`SDevde - 3/6/16 10:53pm`**

My goal on 1.101 was way too optimistic, I was dealing with way too much new knowledge. I mean, I am an amateur, and so did not know of the existance of the `.find()` JQuery function.

That asides, I will be now working on fixing the sidepanel, since its so bad looking, and I'll probably take 1.103 to add some nice animations!

<br>

**`EDevde - 5/19/16 7:18pm`**

I completely ignored my original goal mainly because it was too sophisticated. I'll deal with it later.

In the meanwhile, I added a more responsive way to add apps, and now the contain objects, which is awesome.

<br>



##### Test 1.101 - `3/6/16 10:50pm`

**`SDevde - 2/24/16 8:59pm`**

Now I am dealing with some small bugs, that could potentially make applications fully independent from oos engine.

<br>

**`EDevde - 3/6/16 10:50pm`**

For now, it was hard enough only to start up a side panel. on 1.102 I'll be fixing my side panel.

<br>



##### Test 1.100 - `2/24/16 8:57pm`

**`SDevde - 2/22/16 9:02pm`**

I will start test 1.100 by creating an online browser!!!

What for? So that I can check some things out!

<br>

**`EDevde - 2/24/16 8:57pm`**

Well, I kinda defeated my goal. I had no wifi, so  it was hard to search up some things that I needed for the browser, so instead of a browser, I made "notepad", My first interactive application.

<br>



### Test 1.0 - `2/22/16 8:22pm`

**`SDevde - 12/6/15 4:43pm`**

I will most likely have finished Hipopo by the end of Test 1.0

I do not know if I will be able to finish anything else through.

<br>

**`EDevde - 2/22/16 8:22pm`**

I am a little sad that my goal was not accomplished, but many other things have though. I have decreased the complexity of the application, and increased the readibility. I have made this app a lot more functional. Therefore, I think I have accomplished something pretty cool!

Although this version is not functional, it is not failed because its goal was research :) :3

<br>



#### Test 1.09 - `12/25/15 3:21pm`

**`SDevde - 12/25/15 3:21pm`**

I have realized that my organization skills aren't the best, I need to improved organizing the code, that is why I'll organize my code with extra comments in 1.09!

I will begin 1.09 by organizing my previous mistakes is 1.085, and finish it by making Hipopo.

<br>

**`EDevde - 2/22/16 8:22pm`**

Although 1.09 has helped me better understand the mechanics of Hipopo, I have not sucedeed in my original goal to fully make it. I have, too, made huge progress in the features that were very bugy.

The truth, is that in every version coming now, I will first start adding new features, and towards the end, I will research more on hipopo.

<br>



##### Test 1.098 - `1/10/16 7:12pm`

**`SDevde - 1/10/16 7:12pm`**

Well, first month of the year and I am yet again trying solving the Hipopo integration. I will be doing mostly research.

But never mind that.

I'll probably try using the `hipopo.js` file and try starting all over again with the `hipopo_manager.js` all over again but taking concepts from there.

Also, I would like to discuss why I add every version to Github. My reasoning behind this is because I am tired of having problems with my files being lost somewhere, so I add everyfile from every version I have. If you dont want to download them all, there is no need, just go to the Github website, browse you favorite version and download that specific folder.

<br>

**`EDevde - 2/22/16 8:19`**

Changed `History.md` to `logs.md`.

A combination of irritation because of working in this project for too long, and of having personal matters to attend to, has led me to have taken this two extra months to advance in this newest version, and perhaps the highest achievement in Orion OS until now.

Test 1.098 has been a sucess mainly because there has been many new things that I learned from looking at my contemporary development. I was highly surprized at many problems I found in the research of hipopo. I have added structures and development guides. And I dont think I'll be ready to add hipopo until test 1.2. Until then I will be adding new features that will better help this application.

<br>


##### Test 1.093 - `1/10/16 7:07pm`

**`SDevde - 1/6/16 8:55pm`**

I have pretty much cleared two thirds of my obstacles from 1.087, organization improved greatly, and documentation did too. The number of files went down to six, not including libraries.

<br>

**`EDevde - 1/10/16 7:07pm`**

The goal of this version was to integrate a feature, to pop up the window that was clicked, this is now resolved finishing this version and making it stable.

This seemed to be a lot easier than expected, being about 10 lines at maximum.

Next up I will try to integrate Hipopo yet for the third time.

<br>



##### Test 1.092 - `1/6/16 8:46pm`

**`SDevde - 1/6/16 7:31pm`**

Although I have reduced the file complexity, and I am quite happy about that, there is now another problem. The methods inside `lemur.js` are almost impossible to find.

Therefore, my solution is to get rid of the `sys` object and replace it with the `usr` object which is a ton more flexible, and certainly less steps to succeed at adding a new app.

I'm also repairing a couple of bugs here and there in my way. And don't let me get started on the documentation, which is mostly deleting unnecessary methods.

<br>

**`EDevde - 1/6/16 8:46pm`**

That sure longer than expected, but it is now done!!!

All apps are now in one file `sys_apps`. And I only need one variable `ua`. This really did simplify everything!!!

I'll tell you what I have on mind on my next SDevde.

<br>



##### Test 1.091 - `1/6/16 7:26pm`

**`SDevde - 1/2/16 1:07am`**

The second day of the year and I already am pretty exited. In 1.091 I will attempt to things:

 1. Create a new file for every constant.
 2. Combine all variables and methods as well as functions into a single object.

They both are from short term goals from 1.090. They seem doable, and believe me, they will! :)

<br>

**`EDevde - 1/6/16 7:26pm`**

Doing great, I did what I wanted to do :)

I have succeeded at my goals.

Next up, I'll be fixing some bugs. The documentation decresed in size tremendusly after this update.

<br>



##### Test 1.090 - `1/2/16 1:04am`

**`SDevde - 12/25/15 3:25pm`**

There are many serious mistakes I have made, and am responsible for in the early development of Orion. I have just suffered another encounter and have failed 1.08,10, which of course has made me move into the new version 1.09.

1.09 will be certainly different, I will first organize ABOSLUTELLY EVERYTHING and later rearrange it in order to create a successful development of Hipopo.

<br>

**`EDevde - 1/2/16 1:04am`**

`Registry.md` has been replaced by the `plan.md` file that every version contains.

`plan.md` was the main reason for this version. Therefore I have succeeded in 1.090. Plus, Today it is the second day of the year, pretty exiting!

<br>



#### Test 1.08 - `12/25/15 3:09pm`

**`SDevde - 12/9/15 7:10pm`**

Based upon the "study session" I had in `test-f-1.0(10)`, I can much easily recover from test-s-1.063. Then I guess I will just have to build of off that.

This time I'll try to be more careful and write and post more often in order to organize myself. :)

<br>

**`EDevde - 12/25/15 3:09pm`**

It is now over, I have failed in 1.08 yet again, lost complete track of what I was doing in 1.08,10.

But lets also look at the positive side of this. I have created a pretty well organized user app manager. There is still some problems with the appcon manager, since I have to do so many things!

I organized myself and recreated my mind as it was during 1.08,10, which helped me replicated a bunch of features. I will first make sure to get rid of any problems I could have had in 1.085, and then I'll try to create Hipopo one more time.

<br>



##### Test 1.08,10 - `12/25/15 2:53pm`

**`SDevde - 12/24/15 6:12pm`**

This is the last version of 1.08!

I will now finish by creating the first Hipopo, capable of reading files off of a text file!!!

Yes! I will now utilize my knowledge of the framework of wc and such to create my first Hipopo!

<br>

**`EDevde - 12/25/15 2:53pm`**

I have failed. I was so enthusiastic about Hipopo, that I lost track of what I was doing. The big problem is that I couldn't initialize the first Hipopo by the end of `1.08`, since this is a failed version.

<br>



##### Test 1.085 - `12/23/15 9:50am`

**`SDevde - 12/23/15 9:50am`**

I have finally finished the `app manager`, the one that allows me to stop repeating the same code over and over, which of course is AWESOME!!!

1.085 will be dedicated to clearing up some space, getting rid of stupid comments, and so forth.

In my last update I declared that I wanted 1.08,10 to be my new implant for Hipopo, I'm afraid that "that" will not take priority over the `wc` function in the app manager. `wc` will be resolved in 1.085!

<br>

**`EDevde - 12/23/15 1:20pm`**

Due to several bugs in my code, it was rather hard to identify what the problems were.

I have successfully applied `wc` into the code. Furthermore, now there only need to be one file change in order to add a new app!!!

But, I cannot add new appcons alone, making the user have to edit aactl, inside `js/sys/sys_taskbar_manager.js`. So some improvement needs to be done. I will add this to plan.md.

<br>



##### Test 1.084 - `12/23/15 9:44am`

**`SDevde - 12/11/15 11:25pm`**

This version should include the 1.07 "`app manager`". Once that is in, I need to start debugging it.

I'll finish this version whenever I finishing connecting the app manager.

<br>

**`EDevde - 12/23/15 9:44am`**

I am happy that I have finally finished the `app manager` a great idea from version 7. But now that is done, there are still some problems with the minimizing function of my new app `reader`.

That will be my next version meaning test 1.085. Then I'll finally add Hipopo in test 1.08,10, so yes I am skipping one, why? Because it will be a BIG update!

I am certainly happy with my new editor ATOM. Reason being, atom supports node.js functions, I mean the highlighting part, while sublime doesn't, and it makes it a lot easier to do things.

<br>



##### Test 1.083 - `12/11/15 11:22pm`

**`SDevde - 12/10/15 12:04pm`**

I now have to try to repeat the logic I started in 1.07!

This is gonna be specially hard since I really don't know if the logic worked at all. I'm first gonna do a quick clean up of my code so then I can start to code without worrying about weird stuff.

<br>

**`EDevde - 12/11/15 11:22pm`**

I now have downloaded Jquery/UI in order to have no necessity of being online. I have also added a concept for `taskbar.js`.

I need to change the name of variables, and start creating the "`app manager`"!

First version that is fully operational offline!

<br>



##### Test 1.082 - `12/10/15 12:01pm`

**`SDevde - 12/9/15 11:04pm`**

I will now create a new `reader.js` which will include ONLY my reader app.

<br>

**`EDevde - 12/10/15 12:01pm`**

I successfully created the necessary files to start the `usr` "app manager" from 1.07.

<br>



##### Test 1.081 - `12/9/15 11:02pm`

**`SDevde - 12/9/15 8:49pm`**

I should back up and start by adding a new app in a non-flexible fashion.

<br>

**`EDevde - 12/9/15 11:02pm`**

Succeeded at the generation of a non-flexible new app(reader).

<br>



##### Test 1.080 - `12/9/15 7:29pm`

**`SDevde - 12/9/15 7:11pm`**

In this first version, I will first try to reconstruct the baby steps that I took during 1.07. Then I'll figure what to do.

<br>

**`EDevde - 12/9/15 7:29pm`**

I started by modifying some file names, and then commenting the lines of code which need to be more flexible.

I'm probably gonna add usr filenames in order to have two different loads:

 1. system load - loading basic application and framework to start the application
 2. user load - almost everything else will be in this category. This is the part of orion which has a default but allows users to add new features.

So almost repeating my example in test 1.07

<br>



#### Test 1.07 - `12/9/15 7:06pm`

**`SDevde - 12/7/15 8:21pm`**

In 1.07 I will try to add some new features!

More importantly, I will try to create a new app called "reader", which will be my first "real" application.

I will also try to start creating Hipopo(If confuzed, please refer to note 3 under the notes folder).

<br>

**`EDevde - 12/9/15 7:06pm`**

Failed! I advanced too fast to realize that I was doing things wrong. I completely forgot my objective.

Since I experimented so much with the code, I now have a better understanding of what I'm doing, although it kind of sucks that this is the second time I do this.

<br>



##### Test 1.070 - `12/9/15 7:06pm`

**`SDevde - 12/7/15 5:08pm`**

test 1.070's goal was to create(even if unstable), the first application, which will be called reader!

<br>

**`EDevde - 12/9/15 7:06pm`**

I failed, it truly did help my research and helped clear things that were a big confusiling. Now it'll be easier to start the new version(although I'm not happy to skip a version because it was a fail).

<br>



#### Test 1.06 - `12/7/15 8:16pm`

**`SDevde - 12/6/15 5:29pm`**

I hope that I can recreate an organization similar than the one in `test-f-1.05` but that works. That's my plan!

**`EDevde - 12/7/15 8:16pm`**

Finished and I feel quite accomplished with my results. Minimizing has been improved and the overall logic!

<br>



##### Test 1.063 - `12/7/15 8:10pm`

**`SDevde - 12/7/15 5:08pm`**

`taskbar_manager()` has a problem, for every new window I want, I need to repeat a TON of code. I need to create an object constructor for that!

Attempt to fix a small bug, appcons are not colored when minimizing.

<br>

**`EDevde - 12/7/15 8:10pm`**

I have successfully created a versatile way minimizing apps. All bugs are done with :)

I will now work with new features!

<br>



##### Test 1.062 - `12/7/15 12:27am`

**`SDevde - 12/6/15 8:32pm`**

I need to finish creating a taskbar manager!

<br>

**`EDevde - 12/7/15 12:27am`**

Finished my favorite taskbar manger. There is a small bug though, appcons are not colored when minimized.

<br>



##### Test 1.061 - `12/6/15 8:22pm`

**`SDevde - 12/6/15 8:22pm`**

Quoting myself from my previous log:

> I am still loading system data first though.

I have to do that, that might fill up `test 1.061`, but I still might have some time for more.

Also fixing the minimizing bug.

<br>

**`EDevde - 12/6/15 8:29pm`**

Finished fixing the minimizing bug. Also, FINALLY created taskbar manager!

<br>



##### Test 1.060 - `12/6/15 5:57pm`

**`SDevde - 12/6/15 6:02pm`**

I hope that I can recreate an organization similar than the one in `test-f-1.050` but that works. That's my plan!

<br>

**`EDevde - 12/6/15 8:19`**

To keep organized, I have decided to stop now with `test 6.060`. It is now stable!

I added two folders, [`sys`](test/test-1.0/test-1.0/test-1.06/1.061/js/sys) and [`usr`](test/test-1.0/test-1.0/test-1.06/1.061/js/usr).

I have decided that the apps that I have created from the beginning will now be user defined, in other words, the user defines the two apps. So I'm trying to decide whether I want to first load the user data and then the system data, or the other way around. Although it would make sense to first load the system data, it seems a lot easier to code the user data first. There is only one problem with loading user data first, users might be able to manipulate system data since they can load their stuff first. I am still loading system data first though.

There is a big bug that is really annoying me; I can't minimize windows.

<br>



#### Test 1.05 - `12/6/15 5:01pm`

**`SDevde - 12/6/15 5:01pm`**

I have finally accomplished the organization of files in `test 1.045`, but sadly, there is still need for further organization and development.

The idea was to create functions that were more versatile, to create an early model of `Lemur`(seen in [`note3.md`](notes/note3.md) for reference).

<br>

**`EDevde - 12/6/15 5:21pm`**

As `test-f-1.050` failed, there is no choice but to restart with `test 1.060`!

<br>



##### Test 1.050 - `12/6/15 5:03pm`

**`SDevde - 12/6/15 5:03pm`**

Due to the poor recording data from before, I'm afraid that the initial goal was lost.

<br>

**`EDevde - 12/6/15 5:04pm`**

Failure. Test 1.050 was a disaster!
`test-u-1.050` is now unstable. `test-u-1.050` was so big as a project, that it would be a good idea to rebuild from `test 1.045`. Too many steps were taken at the same time, as so it was difficult to do everything at once.

<br>
