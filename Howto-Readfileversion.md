# Versions Info

## Orion OS will be registered as the following

test: indicates versions before alpha(not presentable)

 - 1.0 first
 - 1.1 first set
   - Added 10 features
 - 1.01 adding feature
   - Maximum 100 features
 - 1.001 fixing bugs
   - after fixing 10 bugs you can consider it a feature
   - every Major bug fix(MBF) is considered 5 bug fixes
 - 2.0 enough difference to call it a different application

alpha: first indication of it being presentable

 - 1.0 first
 - 1.1 first set
   - Added 10 features
 - 1.01 adding feature
   - Maximum 100 features
 - 1.001 fixing bugs
   - after fixing 10 bugs you can consider it a feature
   - every Major bug fix(MBF) is considered 5 bug fixes
 - 2.0 enough difference to call it a different application

beta: last version of the program before the being functional

 - 1.0 first
 - 1.1 first set
   - Added 10 features
 - 1.01 adding feature
   - Maximum 100 features
 - 1.001 fixing bugs
   - after fixing 10 bugs you can consider it a feature
   - every Major bug fix(MBF) is considered 5 bug fixes
 - 2.0 enough difference to call it a different application

xi: fully functional applications

 - 1.0 first
 - 1.1 first set
   - Added 16 features
 - 1.01 adding feature
   - Maximum 256 features
 - 1.00a fixing bugs
   - Fixing up to 26 bugs
   - after fixing 26 bugs you can consider it a feature
   - every Major bug fix(MBF) is considered 13 bug fixes
 - 2.0 enough difference to call it a different application

Examples:  
test 3.237

 - test app version 3
 - 23 added features from the original 3.0
 - 7 bugs fixed

alpha 7.024

 - alpha version 7
 - 2 added features from the original 7.0
 - 4 bugs fixed

beta 25.234

 - beta version 25
 - 23 added features
 - 4 bugs fixed

xi8.a4k or 8.a4k

 - xi version/version 8
 - 164 added features
 - 11 bugs fixed

***

The stage is:

 - test
 - alpha
 - beta
 - xi

The version is:

 - the first number before the dot

The division/branch is:

 - the two numbers after the dot
 - a set is the first number after the dot

The sub-division/bug-number is:

 - the third number after the dot


On certain cases, you would have beta 4.167 and you release a MBF then the result would be beta 4.16(12) or beta 4.167,5 to indicate that there was more bugs than the version number, instead of writing beta 4.172 which assumes that you already finished branch 16 & 17, which is incorrect, what actually does happened is that branch 16 wasn`t completed, so to better register that branch 16 wasn't completed but rather extended, we add the "()" or the ",". In case that you need to restart the development of a certain version, you would indicate that the previous version was failed with -f- and would create the next division/branch, like alpha 7.789 fails, you would restart and now call it alpha-f-7.789 and create a new division called alpha 7.790.

Stable versions have the option to have a -s- after the stage, while unstable ones would have a -u-, example: beta-s-25.234. In case that you have a fail, that fail would not be an unstable, you would call it a fail, but not unstable, only fail.
