# Hipopo string file system

As in stated in `note3` Hipopo should be the file management system, which will be one big string that will contain all the information. Now, the problem is, how are we gonna do that?

It would quite simple to use node.js, and get rid of all this nonsense. But part of Orion OS is exactly that, which could potentially decrease its size. One way is to use an object, like:

```js
var hipopo = {
	// stuff...
};
```

but as stated before, it is even better if we use a string. So how do we do that? How about we use JSON to help us out, how? Like this:

```js
var hipopo = "{stuff...}";
```

What we could do is set some properties, like:

```js
`
general:
	name: \"The_name.txt\",

	type: \"Text Document (.txt)\",
	opens_with: \"text_editor\",
	location: \"C:/usr/user/Desktop\",
	size: \"84 bytes\",

	created: \"Sunday, November 8, 2015, 7:35:43 PM\",
	modified: \"Sunday, November 8, 2015, 7:35:43 PM\",
	accessed: \"Sunday, November 8, 2015, 7:35:43 PM\",

	attributes: {
		read_only: false,
		hidden: false
	}
}`
// or condensed:
`general:{name:\"The name.txt\",type:\"Text Document(.txt)\",opens_with:\"text_editor\",location:\"C:/usr/user/Desktop\",size:\"84bytes\",created:\"Sunday,November 8,2015,7:35:43 PM\",modified:\"Sunday,November 8,2015,7:35:43 PM\",accessed:\"Sunday,November 8,2015,7:35:43 PM\",attributes:{read_only:false,hidden:false}}`
```

True, this setup would be perfect for files, but what about folders?

```js
var hipopo = `{
	orion_os: {
		properties: {
			name: \"Orion OS\",
			type: \"folder\",
			location: \"root\",
			size: \"n-bytes\",
			contains: [\"1 folder\", \"2 files\"],
			created: \"Sunday, September 27, 2015, 10:47:48 PM\",
			attributes: {
				read_only: false,
				hidden: false
			}
		},
		children: [

		]
	}
}`;
```

So let's build a tree that actually holds files and folders:

```js
var hipopo = `{
	orion_os: {
		properties: {
			name: \"Orion OS\",
			type: \"folder\",
			location: \"\\\",
			size: \"n-bytes\",
			contains: [true, [\"1 folder\", \"2 files\"]],
			created: \"Sunday, September 27, 2015, 10:47:48 PM\",
			attributes: {
				read_only: false,
				hidden: false
			}
		},
		children: [
			myfolder: {
				properties: {
					name: \"myfolder\",
					type: \"folder\",
					location: \"\\myfolder\",
					size: \"n-bytes\",
					contains: [\"0 folder\", \"0 files\"],
					created: \"Sunday, September 27, 2015, 10:47:48 PM\",
					attributes: {
						read_only: false,
						hidden: false
					}
				},
				children: [false, [\"0 folder\", \"0 files\"]]
			},
			file_one: {
				properties: {
					name: \"file_one.txt\",
					type: \"Text Document (.txt)\",
					opens_with: \"text_editor\",
					location: \"\\file_one.txt\",
					size: \"n-bytes\",
					created: \"Sunday, November 8, 2015, 7:35:43 PM\",
					modified: \"Sunday, November 8, 2015, 7:35:43 PM\",
					accessed: \"Sunday, November 8, 2015, 7:35:43 PM\",
					attributes: {
						read_only: false,
						hidden: false
					}
				},
				content: \"Hi you!!!\"
			},
			file_two: {
				properties: {
					name: \"file_two.jpg\",
					type: \"JPG Document (.jpg)\",
					opens_with: \"photo_viewer\",
					location: \"\\file_two.txt\",
					size: \"n-bytes\",
					created: \"Sunday, November 8, 2015, 7:35:43 PM\",
					modified: \"Sunday, November 8, 2015, 7:35:43 PM\",
					accessed: \"Sunday, November 8, 2015, 7:35:43 PM\",
					attributes: {
						read_only: false,
						hidden: false
					}
				},
				content: \"image file content\"
			},
		]
	}
}`;
```

which might look big at first, but that's only because it is unfolded, in reality, because this is pure text, this same thing condensed would be:

```js
var hipopo = `{orion_os:{properties:{name:\"Orion OS\",type:\"folder\",location:\"\\\",size:\"n-bytes\",contains:[\"1 folder\",\"2 files\"],created:\"Sunday,September 27,2015,10:47:48 PM\",attributes:{read_only:false,hidden:false}},children:[myfolder:{properties:{name:\"myfolder\",type:\"folder\",location:\"\\myfolder\",size:\"n-bytes\",contains:[\"0 folder\",\"0 files\"],created:\"Sunday,September 27,2015,10:47:48 PM\",attributes:{read_only:false,hidden:false}},children:[false]},file_one:{properties:{name:\"file_one.txt\",type:\"Text Document (.txt)\",opens_with:\"text_editor\",location:\"\\file_one.txt\",size:\"n-bytes\",created:\"Sunday,November 8,2015,7:35:43 PM\",modified:\"Sunday,November 8,2015,7:35:43 PM\",accessed:\"Sunday,November 8,2015,7:35:43 PM\",attributes:{read_only:false,hidden:false}},content:\"Hi you!!!\"},file_two:{properties:{name:\"file_two.jpg\",type:\"JPG Document(.jpg)\",opens_with:\"photo_viewer\",location:\"\\file_two.txt\",size:\"n-bytes\",created:\"Sunday,November 8,2015,7:35:43 PM\",modified:\"Sunday,November 8,2015,7:35:43 PM\",accessed:\"Sunday,November 8,2015,7:35:43 PM\",attributes:{read_only:false,hidden:false}},content:\"image file content\"},]}}`;
```

For a total of 1186 bytes. The size of this could be reduced even further just by translating this into number notation.
