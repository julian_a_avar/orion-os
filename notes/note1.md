# Note 1

Let's say that be have two variables

```js
var variable = "paypay";
var value = "this is the value of paypay!";
```

With the info above, is there a way to make paypay = "this is the value of paypay!"?

You can put it in a dictionary:

```js
var obj = {};
obj[variable] = value;
```

Then obj.paypay is a variable which is now "this is the value of payday!"


This would allow to add app variables without manually having to add the variables.
