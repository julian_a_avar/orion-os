# Note 2
###### test-1.022

In apps.js

naming apps' variables

There are two types of file

`app` = This will indicate that this is not a system file  
or  
`sys` = This will indicate that this is a system file


There are two types of apps

`ext` = This will indicate that this is an external app  
or  
`int` = This will indicate that this is an internal app

Location

for now, the location tree will be:  
	orion(also known as partition O, or `_O_`


Name

name can be any set of characters, but, characters other than letters and numbers will be listed below:

```
` = /01/ or /_sym_backtick/
! = /02/ or /_sym_exclamation/
@ = /03/ or /_sym_at/
# = /04/ or /_sym_hash/
$ = /05/ or /_sym_dollar/
% = /06/ or /_sym_percent/
^ = /07/ or /_sym_exp/
& = /08/ or /_sym_amp/
* = /09/ or /_sym_star/
( = /10/ or /_sym_circlestart/
) = /11/ or /_sym_circleend/
- = /12/ or /_sym_dash/
= = /13/ or /_sym_equal/
[ = /14/ or /_sym_squarestart/
] = /15/ or /_sym_squareend/
\ = /16/ or /_sym_backslash/
; = /17/ or /_sym_dotncomma/
' = /18/ or /_sym_snglquote/
, = /19/ or /_sym_comma/
. = /20/ or /_sym_dot/
/ = /21/ or /_sym_frontslash/
~ = /22/ or /_sym_squigly/
_ = /23/ or /_sym_underdash/
+ = /24/ or /_sym_plus/
{ = /25/ or /_sym_squiglystart/
} = /26/ or /_sym_squiglyend/
| = /27/ or /_sym_pipe/
: = /28/ or /_sym_dblpoint/
" = /29/ or /_sym_dblquote/
< = /20/ or /_sym_arrowstart/
> = /31/ or /_sym_arrowend/
? = /32/ or /_sym_question/
0 = /33/ or /_sym_zro/
1 = /34/ or /_sym_one/
2 = /35/ or /_sym_two/
3 = /36/ or /_sym_tre/
4 = /37/ or /_sym_for/
5 = /38/ or /_sym_fiv/
6 = /39/ or /_sym_six/
7 = /40/ or /_sym_svn/
8 = /41/ or /_sym_eit/
9 = /42/ or /_sym_nin/
(space) = /43/ or /_sym_space/
```

This will allow to properly name variables

All user defined apps will start with app_usr_fullapp_(appname)
