# How to deal with Hipopo

I now realize that I can use Hipopo with JSON to easily.

Before If I had a string like:

```js
`
general:
	name: \"The_name.txt\",

	type: \"Text Document (.txt)\",
	opens_with: \"text_editor\",
	location: \"C:/usr/user/Desktop\",
	size: \"84 bytes\",

	created: \"Sunday, November 8, 2015, 7:35:43 PM\",
	modified: \"Sunday, November 8, 2015, 7:35:43 PM\",
	accessed: \"Sunday, November 8, 2015, 7:35:43 PM\",

	attributes: {
		read_only: false,
		hidden: false
	}
}`
```

I would need to use RegEx to retrieve the info. Now since I have JSON, I can just say:

```js
var example = general.attributes.read_only;
```

Which makes things a lot easier.
