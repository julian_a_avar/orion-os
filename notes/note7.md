# File tree system(FTS) for Orion OS


Linux, windows, dos, they all have a file tree system, mainly because it makes things really easy to organize.

Therefore, I have decided to take the same path, of course, utilizing Hipopo, now on 1.098!

I have a global in `oos-engine` called `oos`. `oos` has inside hipopo, which has `tmy`; tummy, as in the stomach of the hippo.

hipopo needs a root directory, not `tmy`, `tmy` signifies the content inside hipopo, not the root directory.

```js
/*
oos =>
	hpp =>
		tmy =>
			o = >
				boot => // orion os data
				lib => // external libraries to orion os should be included here
				tmp => // temporal data should be stored here
				ou => // orion user, the top admin, but it is not oos. Only oos can manage the structure of certain trees, and oos is not a user, oos is the lemur, or the central decision taker, with which you can do whatever you need to. However, lemur has to be modified by hand, that is why if those permissions are desirable, please do add a plug-in.
					apps => // application that can only be used by ou
				usr => // other users are in here
					antonio => // user
					julian => // another user
						apps => application that can only be used by julian, unless otherwise instructed
					mary =>
						desktop => // this folder cannot be deleted by any user, not even ou, only by lemur.
							app1.evngln =>
								"ua.app1={};ua.app1.content=\"1<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>\";ua.app1.at=\"App 1\";ua.app1.act=\"A1\";lmr.sys.am.vm();"
							app2 =>
								"ua.app2={};ua.app2.content=\"2<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>\";ua.app2.at=\"App 2\";ua.app1.act=\"A2\";lmr.sys.am.vm();"
						downloads => // this folder cannot be deleted by any user, not even ou, only by lemur.
						documents => // premade folder to serve as organizer
						music => // premade folder to serve as organizer
						pictures => // premade folder to serve as organizer
						videos => // premade folder to serve as organizer
						apps => // this folder cannot be deleted by any user, not even ou, only by lemur.
				apps => // applications shared by everyone
					reader => // applications can be generated by creating a `.oosac`(orion os app creator, much like a window wizard using `.exe`) out of a normal evangeline(`.evngln`) application. Only `ou` has the rights to create a app in this format inside a apps folder without using a `.oosac`.
						head =>
							name : reader
							apptitle : Reader
							appcontitle : R
						body =>
							reader<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>
*/
```

`o` will be the top root directory.
