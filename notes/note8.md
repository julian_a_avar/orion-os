# More about the hpp file system

In Orion OS, files and folders and partitions are all the same. Therefore being called "cells".

Every cell inside `tmy` of `hpp`, needs to have two basic objects, `prp` & `ctt`.

`prp` is an object containing properties(metadata), while `ctt` is an object containing content information.

There are three basic properties which need to be included in every `prp`; `type`, `type-parent`, and `share`. These are necessary in order for the filesystem to understand itself.

Let us start my talking about the `type-parent`. This is a property which defines the type of cell that the parent(the cell containing this `prp`) is. There is a total of seven multiple kinds of predefined `type-parent`s.

1. universe
2. galaxy
3. cluster
4. star
5. nebula
6. quasar
7. parent

Number 6, the quasar is the most powerful `type-parent`, it is the one used to define other things, including all of the other types. Quasar can be modified however you need to in order to create a better file system.

But let us start from the easy things first(The orion os options that are predefined).

1. universe
  - `unvrse`
  - This sets the universe file system, and the default in Orion OS, there is only one.
  - Can hold a predetermined number of things
  - More on this later
2. galaxy
  - `glx`
  - These are root folders which cannot be modified by anyone but `root` aka `o`'s children.
	- Can hold a predetermined number of things
3. cluster
  - `clstr`
  - These are basic folders
  - They can hold anything, depending on name
4. star
  - `str`
  - These are normal files, and binaries
  - These refers to the contents of the folder, and means that there will be zero or one files inside the folder
5. nebula
  - `nbl`
  - These are normal files, and binaries, just like stars
  - These refers to the contents of the folder, and means that there will be two or more files inside the folder
6. quasar(non oos specific)
  - `qsr`
  - These can be anything
  - Contents need to be specified
7. parent(non oos specific)
  - `prt`
  - These can be anything
  - They will be able to hold anything that the parent could

## Going to the specifics

### Using quasars

#### What is a quasar

A quasar us the name we've given for data structures. Instead of working with objects or arrays, we just kind of make up different kinds of objects. All objects are quasar, but you can manipulate quasar to give you different kinds of structural data represented by objects.

Quasars are found inside the hipopo application, although they could eventually be used outside of hipopo, since hipopo is a file manager that was created specidically for oos. Therefore, althought hipopo might not be used outside of the oos scope, quasars, or `tmy` could potentially be used for other external applications.

#### What is `parent`

Althought it `parent` is non-oos specific, `parent` is after all, a quasar. `parent` defines the parent of the current quasar, but when talking about `type` and `type-parent`, it refers to the quasar data structure of the parent, therefore making life easier. `parent` is the default metadata property value for all current properties, except for `o`, the top root.

#### How to make quasars

Quasars are perhaps one of the most powerful features in oos right now, mainly because you can control the entire phisical space of the application with it.

It is currently not supported.

### Universe quasar

The `universe` quasar is the default filesystem used by orion os.

The universe quasar is a defult value that holds every single object in oos. `o` must be universe when working in the oos file system.

This is an example of how `unvrs` could be utilized:

```js
o ->
	root: o
	prp ->
		type-parent: "::unvrs>>"
		share: "::o>>"
	ctt ->
		boot ->
			prp ->
				typeprop: "::clstr>>"
				share: "::o>>"
			ctt ->
		lib ->
			prp ->
				typeprop: "::clstr>>"
				share: "::o>>"
			ctt ->
		tmp ->
			prp ->
				typeprop: "::clstr>>"
				share: "::o>>"
			ctt ->
		ou ->
			root: root -> ctt -> ou
			prp ->
				typeprop: "::glx>>"
				share: "::ou>>"
			ctt ->
				desktop ->
					prp ->
						typeprop: "::clstr>>"
						share: "::ou>>"
					ctt ->
				apps ->
					prp ->
						typeprop: "::clstr>>"
						share: "::ou>>"
					ctt ->
		nausr ->
			prp ->
				typeprop: "::glx>>"
				share: "::all>>"
			ctt ->
				julian ->
					prp: ->
						typeprop: "::clstr>>"
						share: "::nausr>:julian>>"
					ctt ->
						desktop
							prp ->
								typeprop: "::clstr>>"
								share: "::nausr>:julian>>"
							ctt ->
								readerapp ->
									prp ->
										typeprop: "::str>>"
										share: "::nausr>:julian>>"
									ctt ->
										main: "ua.reader={};ua.reader.content=\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>\";ua.reader.at=\"Reader\";ua.reader.act=\"R\";lmr.sys.am.vm();"
						apps ->
							prp ->
								typeprop: "::clstr>:app>>"
								share: "::nausr>:julian>>"
							ctt ->
								app1 ->
									typeprop: "::clstr>:app>:main>>"
									share: "::nausr>:julian>>"
								ctt ->
									head ->
										prp ->
											typeprop: "::clstr>:app>:head>:nbl>>"
											share: "::nausr>:julian>>"
										ctt ->
											name: "app1"
											apptitle: "App 1"
											appcontitle: "A1"
									body ->
										prp ->
											typeprop: "::clstr>:app>:head>:str>>"
											share: "::nausr>:julian>>"
										ctt ->
											main: "<p>1</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>"
		apps -> // global apps
			prp ->
				typeprop: "::clstr>:app>>"
				share: "::all>>"
			ctt ->
				app2 ->
					prp ->
						typeprop: "app"
						share: "::all>>"
					ctt ->
						head ->
							prp ->
								typeprop: "::clstr>:app>:head>:nbl>>"
								share: "::all>>"
							ctt ->
								name: "app2"
								apptitle: "App 2"
								appcontitle: "A2"
						body ->
							prp ->
								typeprop: "::clstr>:app>:body>:str>>"
								share: "::all>>"
							ctt ->
								main: "<p>2</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>"
```
### Galaxy quasar

As seen in the example from the universe quasar, galaxy quasars may only be direct children of the universe quasar. These are just predefined containers that hold basic info for the system to operate.

### Cluster quasar

These is prehaps one of the most basic units in oos. Clusters are no more than objects that can hold ANY kind fo file type, exept of course for universe and galaxy.

They however may not directly have binary data in them.

### Star quasar

Star quasars may only have one string which is binary data, later it should be transformed to numbers instead of strings. These may not contain absolutelly anything else.

### Nebula quasar

These is no more than a combination of stars all together. For example, all `prp` objects are nebulas, unless specified. They are like clusters because they can hold more than one, but more like stars in that they may only hold strings.
