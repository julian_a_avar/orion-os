# Note 3
###### test-1.051

Orion OS is made out of different interrelated parts.
These include:
```
 - Lemur -lmr- (like the Master in KungFU Panda)
 - Hipopo -hpp- (Holds stuff with its big tummy)
 - Fly -fly- (Very light but can go around taking stuff from others)
 - Octopi -octpi- (It can do 8 things at the time)
 - Grasshoper -grshpr- (As soon as something is done it jumps around doing what whatever it has to)
 - Axolot -axlt- (Very complex, it is the center of attention, and very pretty)
 - Humingbird -hmngbrd- (It goes around telling tales)
 - Evangeline -evngln- (History made up, it is the poem that made something new out of the past -plus Ben told me to call it that, so, whatever...-)
```

### Lemur:
Lemur is the decision taker. All ifs and else's are set in Lemur. But Lemur is a sys program, so you can't exactly use it in any app. Lemur can, thought, be used in an app, but first you would need to edit it to respond to a certain app. Lemur's purpose is to decrease the complexity of the common app.

### Hipopo:
Hipopo is the language that can store the information of the whole application. This may as well include all files and folders. For the protection of users, this file will later be encrypted.

### Fly:
Fly is the most simple part of Orion OS. Since this app is suppose to be running independently, Fly is suppose to replace JS, and be like the scrap. Octopi code is turned to Fly, Fly is then transferred to Axolot where it is converted to JS by Fly(the same app) and then run as Three.js, to WebGL. Because Fly is used only as a "JS simplifier", meaning that its job is to drop the size of the software by quite a bit(since user applications can become very heavy, to fasten the processes, although it might ask more RAM, I transfer it and package it into a string), it can only be used as a "helper" for Octopi, so it cannot stand alone, it needs to be used BY Octopi.

### Octopi:
Octopi's purpose is to create. This is a language that can be later be processed into JS, but it is suppose to be able to create any app. And technically talking, it could not only create itself(repeat it's own library), but also create a whole independent app(like Orion OS 2). Octopi is supposed to be only used with non-sys files, although the opposite is possible.

### Grasshoper:
Grasshoper is the app that processes events. It is much like a RAM.

### Axolot:
Axolot is the graphic interphase of Orion OS. All -events- happen in Axolot, although not all processes can be visualized.

### Humingbird:
Humingbird is the messenger of the whole application. Humingbird will be called by the various parts to send information to one another.

### Evangeline:
Evangeline is a language similar to Octopi. The major difference between the two, is that Evangeline has less powerful methods. The reason behind this is to help users not brake Orion OS. Octopi's reason of existence is the fact that you can add more "sys" files easily instead of building them up by hand.


### How does Orion OS turn ON?
 1. Orion OS turns ON
 2. Grasshoper delivers the ON event, and asks Humingbird to send it to Hipopo
 3. Hipopo then is ready to send the information to Lemur, via Humingbird
 4. Lemur then decides what information should be sent to Axolot
 5. Lemur then sends info to Humingbird to deliver to Axolot
 6. Axolot processes the information and transfers it to Three.js, to then change it to WebGL :)

```
event ON
-grshpr-
	-hmngbrd-
		-hpp-
-hpp-
	-hmngbrd-
		-lmr-
-lmr-
	-hmngbrd-
		-axlt-
-axlt-
	exec
```

### How do users build apps?
 1. Users will build the app using Evangeline
 2. Evangeline is just an API so they can build it anywhere

### How does Orion OS run an application?
 1. App is run(app is in Evangeline)
 2. Grasshoper delivers the "App is run" event, and asks Humingbird to send it to Hipopo
 3. Hipopo then finds the file, and asks Humingbird to send it to Octopi
 4. Octopi then runs Fly and asks Humingbird to send it to Axolot
 5. Axolot runs Fly
 6. Axolot is transformed into Three.js, and then to WebGL

```
event "App is run"
-grshpr-
	-hmngbrd-
		-hpp-
-hpp-
	-hmngbrd-
	-octpi-
-octpi-
	-fly-
	-hmngbrd-
		-axlt-
-axlt-
	-fly-
		exec
```

This is the system which will be used when building the full app. For now the app doesn't need all of this, but later it will.
