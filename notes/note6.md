# Creating and understanding the hipopo file system.


Due that I cannot nest object without defining them first, adding an app becomes quite a headache.

Adding an app:

```js
var usr_hipopo = JSON.parse(usr_hipopo); // to object, since it is a string

usr_hipopo.usr.content.apps.content["MyAppName"] = {};
usr_hipopo.usr.content.apps.content["MyAppName"].properties = {}; <= ppt
usr_hipopo.usr.content.apps.content["MyAppName"].content = {}; <= ctt

usr_hipopo.usr.content.apps.content["MyAppName"].properties.typeprop = "cluster";

usr_hipopo.usr.content.apps.content["MyAppName"].content.head = {};
usr_hipopo.usr.content.apps.content["MyAppName"].content.body = {};

usr_hipopo.usr.content.apps.content["MyAppName"].content.head.properties = {};
usr_hipopo.usr.content.apps.content["MyAppName"].content.head.properties.typeprop = "app-head&nebula";

usr_hipopo.usr.content.apps.content["MyAppName"].content.body.properties = {};
usr_hipopo.usr.content.apps.content["MyAppName"].content.body.properties.typeprop = "app-body&star";

usr_hipopo.usr.content.apps.content["MyAppName"].content.head.content = {};
usr_hipopo.usr.content.apps.content["MyAppName"].content.head.content.name = "reader2";
usr_hipopo.usr.content.apps.content["MyAppName"].content.head.content.apptitle = "Reader 2";
usr_hipopo.usr.content.apps.content["MyAppName"].content.head.content.appcontitle = "R";

usr_hipopo.usr.content.apps.content["MyAppName"].content.body.content = "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.</p>";

```

Note that the `body typeprop` does not have be a star, that can be changed depending on your app.



This is a simpler hierarchy model of the stated above:

usr =>
	apps =>
		Reader =>
			head =>
				name : reader
				apptitle : Reader
				appcontitle : R
			body =>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique massa ipsum, eu posuere tellus interdum vel. Nullam quis lectus feugiat, malesuada enim ac, malesuada sapien. Cras elit tellus, consequat id justo non, convallis interdum nisi. Sed pulvinar, ipsum in imperdiet malesuada, lorem turpis gravida urna, consectetur auctor leo lectus et est. Nunc in pulvinar nulla, in pharetra dolor. Nunc accumsan felis interdum commodo pretium. Praesent rutrum neque et orci congue imperdiet. Fusce et gravida dui. Donec gravida in mauris vitae aliquam. Quisque semper est ut orci lacinia facilisis.


file types(like folder, file, and other), can be differently named, and positioned. How that works is stated below:

galaxy =>
	cluster =>
		star
	stars =>
		star
		star
		star

app-clstr
	app-main =>
		app-head =>
			nebula
		app-body =>
			clusters || star || nebula

examples of writing properties:

::clstr>:app>>
::clstr>:app>:main>>
::clstr>:app>:head>:nbl>>
::clstr>:app>:body>:clstr>>
::clstr>:app>:body>:star>>
::clstr>:app>:body>:>nebula>

::nausr>:julian,adam>>

::clstr>>

There is a difference between a typeprop:cluster, typeprop:star and a typeprop nebula. Since some can be files, and other folders, and most people have a clear difference of them mentally, in Hipopo they are the same, and will be called cells.

A cluster is much like a folder, it holds only ONE object and nothing else, this object can contain anything inside.
A star is much like a file, it holds only ONE type of data, which might not be an object(it can be an array though).
A nebula is a bit more complex. It is a collection of stars, just like a cluster, but no objects are allowed.
Quasars are perhaps the most powerful typeprop. They can hold anything. The problem is that they need to have a basic specification inside whatever they are.

The properties object is much like a nebula, since it hold multiple types of data, none of which is an object. But properties objects may only have certain kind of stars, with already specified keys. That is why the properties object is considered a quasar. As stated above, all quasars need specified rules. The rules for the properties quasar is specified in the system, so there is no need to specify, although you may change it if you want to.

Properties inside the properties object:

type => type of this properties cell > default quasar
	cluster  - clstr
	star     - str
	nebula   - nbl
	quasar   - qsr
	parent   - prt
typeprop => type of the parent of this properties cell > no default ; specification needed
	universe - unvrs <- can only be used by `o`
	galaxy   - glx
	cluster  - clstr
	star     - str
	nebula   - nbl
	quasar   - qsr
	parent   - prt
		for parent, it uses the same sharing settings as the parent
share => who can access this cell ; usr(specify) or sys > default all
	all
		everyone can touch these files
	o
		only root can touch these files
	ou
		root and admin can rouch these files
	nausr:nameOfThisUser:AndThisOtherOne:
		root, admin, nameOfThisUser, and AndThisOtherOne can touch these files
	prt
		for parent, it uses the same sharing settings as the parent, default
```
