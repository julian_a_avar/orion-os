# Components

## Description

Orion OS is now beginning to be a complex application, due to the fact that now it is not only about `lemur`, but `hipopo`. These make it hard to define exactly what I am doing. That is why this file exists, to document the different versions of Orion OS' components.

## Components`(2 - total)`

### `lemur(1 - total)`

[![](https://img.shields.io/badge/lemur-alpha--1.000-red.svg?style=flat-square)]()

### `hipopo(1 - total)`

[![](https://img.shields.io/badge/hipopo-alpha--1.000-red.svg?style=flat-square)]()
